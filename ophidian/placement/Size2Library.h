#ifndef SIZE2LIBRARY_H
#define SIZE2LIBRARY_H

#include <ophidian/placement/Library.h>
#include <ophidian/parser/Size.h>
#include <ophidian/circuit/Netlist.h>
#include <ophidian/circuit/LibraryMapping.h>
#include <ophidian/floorplan/Floorplan.h>

namespace ophidian{
namespace placement{
void size2Library(const parser::Size & size, Library & library, const circuit::Netlist & netlist, const circuit::LibraryMapping & libraryMapping, const floorplan::Floorplan & floorplan);
}
}

#endif // SIZE2LIBRARY_H
