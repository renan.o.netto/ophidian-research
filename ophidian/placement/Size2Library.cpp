#include "Size2Library.h"

namespace ophidian
{
namespace placement
{

void size2Library(const parser::Size &size, Library &library, const circuit::Netlist &netlist, const circuit::LibraryMapping &libraryMapping, const floorplan::Floorplan &floorplan)
{
    auto site = *floorplan.sitesRange().begin();
    auto siteWidth = floorplan.siteUpperRightCorner(site).toPoint().x();
    auto rowHeight = floorplan.siteUpperRightCorner(site).toPoint().y();

    for (auto cellIt = netlist.begin(circuit::Cell()); cellIt != netlist.end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = netlist.name(cell);

        if (size.contains(cellName)) {
            auto cellSize = size.cellSize(cellName);
            cellSize.x(cellSize.x() * siteWidth);
            cellSize.y(cellSize.y() * rowHeight);
            geometry::Box cellBox(geometry::Point(0, 0), cellSize);

            auto cellStdCell = libraryMapping.cellStdCell(cell);
            library.geometry(cellStdCell, util::MultiBox({cellBox}));
        }
    }
}

}
}
