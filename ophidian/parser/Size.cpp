#include "Size.h"

namespace ophidian {
namespace parser {
Size::Size()
{

}

const geometry::Point &Size::cellSize(std::string cellName) const
{
    return mSizes.at(cellName);
}

bool Size::contains(std::string cellName) const {
    return mSizes.find(cellName) != mSizes.end();
}

SizeParser::SizeParser()
{

}

void SizeParser::readFile(const std::__cxx11::string &filename, std::unique_ptr<Size> &size)
{
    std::ifstream fileStream;
    fileStream.open(filename, std::ios::in);

    if(!fileStream.good())
    {
        return;
    }

    while (!fileStream.eof()) {
        std::string cellName;
        int cellWidth, cellHeight;
        fileStream >> cellName;
        fileStream >> cellWidth;
        fileStream >> cellHeight;

        size->mSizes[cellName] = ophidian::geometry::Point(cellWidth, cellHeight);
    }

    fileStream.close();
}

}
}
