#ifndef SIZE_H
#define SIZE_H

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <iostream>
#include <fstream>

#include <ophidian/geometry/Models.h>

namespace ophidian {
namespace parser {
class Size
{
public:
    Size();

    const geometry::Point & cellSize(std::string cellName) const;

    bool contains(std::string cellName) const;

private:
    std::unordered_map<std::string, geometry::Point> mSizes;

    friend class SizeParser;
};

class SizeParser
{
public:
    SizeParser();

    void readFile(const std::string & filename, std::unique_ptr<Size> &size);
};
}
}

#endif // SIZE_H
