/*
 * Copyright 2017 Ophidian
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

#ifndef OPHIDIAN_UTIL_KDTREEPARTITIONING_H
#define OPHIDIAN_UTIL_KDTREEPARTITIONING_H

#include <vector>
#include <ophidian/geometry/Models.h>
#include <algorithm>
#include <memory>

namespace ophidian {
namespace util {

template <typename Data>
class KDTreeBuilder;

template <typename Data>
class KDTreePartitioning{
public:
    using Range = ophidian::geometry::Box;
    using Point = ophidian::geometry::Point;

    struct Node{
        std::unique_ptr<Node> left, right;
        Node * parent;
        const Point point;
        const Range range;
        std::vector<Data> partition;
        bool legalized;
        Node(const Point p, const Range r): point(p), range(r){}
        ~Node(){
            left.reset();
            right.reset();
        }
    };

    KDTreePartitioning(KDTreeBuilder<Data> & builder, const unsigned int maxDepth = std::numeric_limits<unsigned int>::max()){
        builder.mMaxDepth = maxDepth;
        Range boudingBox = builder.boundingBox();
        mRoot.reset(builder.buildTree(builder.mElements, boudingBox, 0));
        mRoot->parent = nullptr;
        builder.clear();
    }

    KDTreePartitioning(KDTreeBuilder<Data> & builder, Range area, const unsigned int maxDepth = std::numeric_limits<unsigned int>::max()){
        builder.mMaxDepth = maxDepth;
        mRoot.reset(builder.buildTree(builder.mElements, area, 0));
        mRoot->parent = nullptr;
        builder.clear();
    }

    const std::vector<Node*> leafNodes() const{
        std::vector<Node*> partitions;
        leafNodes(partitions, mRoot);
        return partitions;
    }

    const std::vector<Node*> nonLeafNodes() const{
        std::vector<Node*> elements;
        nonLeafNodes(elements, mRoot);
        return elements;
    }

    const std::vector<Node*> mergeNodes(std::vector<Node*> nodes) const{
        std::vector<Node*> parentNodes = unduplicatedParents(nodes);
        for(auto parent : parentNodes){
            std::vector<Node*> sucessors;
            getAllSucessors(sucessors, parent->left.get());
            getAllSucessors(sucessors, parent->right.get());
            for(auto sucessor : sucessors)
                std::copy(sucessor->partition.begin(), sucessor->partition.end(), std::back_inserter(parent->partition));
            parent->left.reset();
            parent->right.reset();
        }
        return parentNodes;
    }

protected:
    std::unique_ptr<Node> mRoot;

    void leafNodes(std::vector<Node*> & result, const std::unique_ptr<Node> & node) const{
        if(node->left.get())
            leafNodes(result, node->left);
        if(node->right.get())
            leafNodes(result, node->right);
        if(!node->left.get() && !node->right.get())
            result.push_back(node.get());
    }

    void nonLeafNodes(std::vector<Node*> & result, const std::unique_ptr<Node> & node) const{
        if(node->left.get())
            nonLeafNodes(result, node->left);
        if(node->right.get())
            nonLeafNodes(result, node->right);
        if(node->left.get() || node->right.get())
            result.push_back(node.get());
    }

    const inline std::vector<Node*> unduplicatedParents(const std::vector<Node*> & nodes) const {
        std::vector<Node*> parentNodes;
        std::transform(nodes.begin(), nodes.end(), std::back_inserter(parentNodes), [](Node * node){return node->parent;});
        std::sort(parentNodes.begin(), parentNodes.end());
        parentNodes.erase(std::unique(parentNodes.begin(), parentNodes.end()), parentNodes.end());
        return parentNodes;
    }

    void getAllSucessors(std::vector<Node*> & result, Node * node) const{
        result.push_back(node);
        if(node->left.get())
            getAllSucessors(result, node->left.get());
        if(node->right.get())
            getAllSucessors(result, node->right.get());
    }

};

template <typename Data>
struct KDTreeBuilder{
    using Point = typename KDTreePartitioning<Data>::Point;
    using Range = typename KDTreePartitioning<Data>::Range;
    using Node = typename KDTreePartitioning<Data>::Node;
    using Element = std::pair<Point, Data>;
    using Elements = std::vector<Element>;

    Elements mElements;
    unsigned int mMaxDepth;

    void add(const Point p, const Data d){
        mElements.push_back(std::make_pair(p,d));
    }

    void reserve(const std::size_t size){
        mElements.reserve(size);
    }

    void clear() {
        mElements.clear();
    }

    const Range boundingBox() const{
        double minX = std::numeric_limits<double>::max();
        double minY = std::numeric_limits<double>::max();
        double maxX = std::numeric_limits<double>::min();
        double maxY = std::numeric_limits<double>::min();
        for(auto p : mElements){
            minX = std::min(minX, p.first.x());
            minY = std::min(minY, p.first.y());
            maxX = std::max(maxX, p.first.x());
            maxY = std::max(maxY, p.first.y());
        }
        return Range(Point(minX,minY), Point(maxX,maxY));
    }

    Node * buildTree(Elements & elements, Range range, const unsigned int depth){
        if (elements.empty())
            return nullptr;

        int axis = depth % boost::geometry::dimension<Point>();
        size_t medianIndex = elements.size() / 2;

        Range leftRange, rightRange;
        if(axis == 0){
            std::nth_element(elements.begin(), elements.begin() + medianIndex, elements.end(), [](Element n1, Element n2){return n1.first.x() < n2.first.x();});
            leftRange = Range(range.min_corner(), Point(elements.at(medianIndex).first.x(), range.max_corner().y()));
            rightRange = Range(Point(elements.at(medianIndex).first.x(), range.min_corner().y()), range.max_corner());
        }else{
            std::nth_element(elements.begin(), elements.begin() + medianIndex, elements.end(), [](Element n1, Element n2){return n1.first.y() < n2.first.y();});
            leftRange = Range(range.min_corner(), Point(range.max_corner().x(), elements.at(medianIndex).first.y()));
            rightRange = Range(Point(range.min_corner().x(), elements.at(medianIndex).first.y()), range.max_corner());
        }

        Node * node = new Node(elements.at(medianIndex).first, range);
        //max depth reached or is leaf.
        if(mMaxDepth == depth || elements.size() == 1){
            for(auto e : elements)
                node->partition.push_back(e.second);
            return node;
        }

        //non leaf node
        node->partition.push_back(elements.at(medianIndex).second);

        Elements left(elements.begin(), elements.begin() + medianIndex);
        Elements right(elements.begin() + medianIndex + 1, elements.end());
        node->left.reset(buildTree(left, leftRange, depth + 1));
        if(node->left)
            node->left->parent = node;
        node->right.reset(buildTree(right, rightRange, depth + 1));
        if(node->right)
            node->right->parent = node;
        return node;
    }
};

} //namespace util

} //namespace ophidian

#endif // OPHIDIAN_UTIL_KDTREEPARTITIONING_H
