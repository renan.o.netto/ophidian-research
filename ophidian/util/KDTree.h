/*
 * Copyright 2017 Ophidian
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at
  http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

#ifndef OPHIDIAN_UTIL_KDTREE_H
#define OPHIDIAN_UTIL_KDTREE_H

#include <vector>
#include <ophidian/geometry/Models.h>
#include <algorithm>
#include <memory>

namespace ophidian {
namespace util {

template <typename Data>
class KDTree{
public:
    using Point = ophidian::geometry::Point;
    struct Node {
        std::shared_ptr<Node> left, right, parent;
        const Point point;
        const Data data;
        Node(const Point p, const Data & d) : point(p), data(d){}
    };

    void reserve(const std::size_t size){
        mNodes.reserve(size);
    }

    void add(const Point point, const Data & data) {
        mNodes.push_back(std::make_shared<Node>(point, data));
    }

    void build(){
        mRoot = build(mNodes);
    }

    void clear() {
        mRoot.reset();
        mNodes.clear();
    }

protected:
    std::vector<std::shared_ptr<Node>> mNodes;
    std::shared_ptr<Node> mRoot;

    std::shared_ptr<Node> build(std::vector<std::shared_ptr<Node>> & nodes, unsigned int depth = 0) {
        if (nodes.empty())
            return NULL;
        int axis = depth % boost::geometry::dimension<Point>();
        size_t median = nodes.size() / 2;
        if(axis == 0)
            std::nth_element(nodes.begin(), nodes.begin() + median, nodes.end(), [](std::shared_ptr<Node> n1, std::shared_ptr<Node> n2){return n1.get()->point.x() < n2.get()->point.x();});
        else
            std::nth_element(nodes.begin(), nodes.begin() + median, nodes.end(), [](std::shared_ptr<Node> n1, std::shared_ptr<Node> n2){return n1.get()->point.y() < n2.get()->point.y();});

        std::shared_ptr<Node> node = nodes.at(median);

        std::vector<std::shared_ptr<Node>> left(nodes.begin(), nodes.begin() + median);
        std::vector<std::shared_ptr<Node>> right(nodes.begin() + median + 1, nodes.end());
        node->left = build(left, depth + 1);
        if(node->left.get() != NULL)
            node->left->parent = node;
        node->right = build(right, depth + 1);
        if(node->right.get() != NULL)
            node->right->parent = node;
        return node;
    }
};

} //namespace util

} //namespace ophidian

#endif // OPHIDIAN_UTIL_KDTREE_H
