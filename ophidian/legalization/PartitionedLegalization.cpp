#include "PartitionedLegalization.h"

namespace ophidian {
namespace legalization {

PartitionedLegalization::PartitionedLegalization(design::Design &design, Box placeableArea, std::string modelFile)
    : mDesign(design), mCellsInitialLocations(design.netlist().makeProperty<util::Location>(Cell())), mInitialFixed(design.netlist().makeProperty<bool>(Cell())), mPlaceableArea(placeableArea),
    mLegalizationPrediction(design, modelFile)
{
    for(auto cellIt = mDesign.netlist().begin(Cell()); cellIt != mDesign.netlist().end(Cell()); ++cellIt) {
        mCellsInitialLocations[*cellIt] = mDesign.placement().cellLocation(*cellIt);
        mInitialFixed[*cellIt] = mDesign.placement().isFixed(*cellIt);
    }
}

struct placementConstraints {
    int maximumMovement;
    int maximumUtilization;
};

inline double cellDisplacement(util::Location l1, util::Location l2){
    return std::abs(l1.toPoint().y() - l2.toPoint().y()) + std::abs(l1.toPoint().x() - l2.toPoint().x());
}

void PartitionedLegalization::legalize(unsigned int i, unsigned int maximumDisplacement){
    unsigned int mergeCount = 0;
    unsigned int mergesByMaxDispl = 0;
    unsigned legalizationCalls = 0;
    removeMacroblocksOverlaps();//move cells outside macroblocks
    allignCellsToNearestSite();//TODO: make sure to insert all cells inside chip boundaries

    std::vector<ophidian::circuit::Cell> chipCells;
    chipCells.reserve(mDesign.netlist().size(ophidian::circuit::Cell()));
    for(auto cellIt = mDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != mDesign.netlist().end(ophidian::circuit::Cell()); ++cellIt)
    {
        if(!mDesign.placement().cellHasFence(*cellIt))
        {
            chipCells.push_back(*cellIt);
        }
    }
    chipCells.shrink_to_fit();

    //create kdtree
    util::KDTreeBuilder<Cell> kdtreeBuilder;
    for(auto cell : chipCells) {
        kdtreeBuilder.add(mDesign.placement().cellLocation(cell).toPoint(), cell);
    }
    util::KDTreePartitioning<Cell> kdtree(kdtreeBuilder, mPlaceableArea, i);

    //reset cells positions
    for(auto cell : chipCells)
            mDesign.placement().placeCell(cell, mCellsInitialLocations[cell]);

    //get ancients and fixed cells
    std::vector<Cell> fixedCells;
    std::vector<Cell> ancientsAndFixedCells;
    for (auto cell : chipCells)
        if (mDesign.placement().isFixed(cell))
            fixedCells.push_back(cell);
    for(auto node : kdtree.nonLeafNodes())//Missing assert size == 1 for all non leaf nodes
        std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(ancientsAndFixedCells));
    std::copy(fixedCells.begin(), fixedCells.end(), std::back_inserter(ancientsAndFixedCells));

    //legalize ancients and fix then
    MultirowAbacus multirowAbacus1(mDesign.netlist(), mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping());
    multirowAbacus1.legalizePlacement(ancientsAndFixedCells, util::MultiBox({mPlaceableArea}));
    for(auto cell : ancientsAndFixedCells)
        mDesign.placement().fixLocation(cell, true);

    //get partitions to legalize
    auto nodesToLegalize = kdtree.leafNodes();
    std::vector<bool> legalized_partitions;
    legalized_partitions.resize(nodesToLegalize.size());

    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();

    //legalize partitions
    unsigned count = 0;
    bool legalized = false;
    while(legalized == false)
    {
        //update ancients and add fixed cells
        ancientsAndFixedCells.clear();
        for(auto node : kdtree.nonLeafNodes()) {
            std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(ancientsAndFixedCells));
            node->legalized = true;
        }
        std::copy(fixedCells.begin(), fixedCells.end(), std::back_inserter(ancientsAndFixedCells));

//        #pragma omp parallel for
        for(unsigned partitionIndex = 0; partitionIndex < nodesToLegalize.size(); partitionIndex++)
        {
//            std::cout << "count " << count++ << std::endl;
//            std::cout << "nodes to legalize " << nodesToLegalize.size() << std::endl;

            // get local vector of cells to be legalized
            auto node = nodesToLegalize.at(partitionIndex);

            if (node->parent && !node->parent->legalized) {
                continue;
            }

            std::vector<Cell> cellsToLegalizeWithoutFixed(node->partition.begin(), node->partition.end());

            std::vector<Cell> cellsToLegalize;
            std::copy(ancientsAndFixedCells.begin(), ancientsAndFixedCells.end(), std::back_inserter(cellsToLegalize));
            std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(cellsToLegalize));

            //legalize partition
            MultirowAbacus multirowAbacus(mDesign.netlist(), mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping());
            auto multibox = util::MultiBox({node->range});
            legalizationCalls++;
            auto result = multirowAbacus.legalizePlacement(cellsToLegalize, multibox);
            bool legalized = result != util::micrometer_t(std::numeric_limits<double>::max());
            legalized_partitions[partitionIndex] = legalized;
            if (node->parent) {
                node->parent->legalized = legalized;
            }

            //check if partition violates the threshold and then flag it as ilegal
            if(legalized == true)
                for(auto cell : node->partition){
                    if(mDesign.placement().cellHasFence(cell))
                        continue;
                    if(cellDisplacement(mDesign.placement().cellLocation(cell), mCellsInitialLocations[cell]) > maximumDisplacement * rowHeight) {
                        legalized_partitions[partitionIndex] = false;
                        node->parent->legalized = false;
                        mergesByMaxDispl++;
                        break;
                    }
                }
        }

        //get unlegalized partitions
        legalized = true;
        std::vector<Node*> unlegalizedNodes;
        //try to change to lambda
        for(unsigned partitionIndex = 0; partitionIndex < nodesToLegalize.size(); partitionIndex++)
            if(legalized_partitions[partitionIndex] == false){
                legalized = false;
                unlegalizedNodes.push_back(nodesToLegalize.at(partitionIndex));
                mergeCount++;
            }

        //merge unlegalized partitions
        if(legalized == false)
        {
            //check if node is root of KDtree.
            if(!unlegalizedNodes.at(0)->parent)
            {
                legalized = true;
//                std::cout<<"WARNING: Circuit is ILEGAL, the legalization could not be possible even for the whole circuit partition."<<std::endl;
            }else{
                nodesToLegalize.clear();
                nodesToLegalize = kdtree.mergeNodes(unlegalizedNodes);

                //reset cell positions and unfix merged ancients
                for(auto ilegalNode : nodesToLegalize)
                    for(auto cell : ilegalNode->partition){
                        mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
                        mDesign.placement().placeCell(cell, mCellsInitialLocations[cell]);
                    }
            }
        }
    }

    for(auto cell : chipCells) {
        mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
    }
    std::cout<< legalizationCalls << ", " << mergeCount<<", "<<mergesByMaxDispl<<", ";
}

void PartitionedLegalization::legalizeWithPrediction(unsigned int i, unsigned int maximumDisplacement)
{
    unsigned int mergeCount = 0;
    unsigned int mergesByMaxDispl = 0;
    unsigned legalizationCalls = 0;
    removeMacroblocksOverlaps();//move cells outside macroblocks
    allignCellsToNearestSite();//TODO: make sure to insert all cells inside chip boundaries

    std::vector<ophidian::circuit::Cell> chipCells;
    chipCells.reserve(mDesign.netlist().size(ophidian::circuit::Cell()));
    for(auto cellIt = mDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != mDesign.netlist().end(ophidian::circuit::Cell()); ++cellIt)
    {
        if(!mDesign.placement().cellHasFence(*cellIt))
        {
            chipCells.push_back(*cellIt);
        }
    }
    chipCells.shrink_to_fit();

    //create kdtree
    util::KDTreeBuilder<Cell> kdtreeBuilder;
    for(auto cell : chipCells) {
        kdtreeBuilder.add(mDesign.placement().cellLocation(cell).toPoint(), cell);
    }
    util::KDTreePartitioning<Cell> kdtree(kdtreeBuilder, mPlaceableArea, i);

    //reset cells positions
    for(auto cell : chipCells)
            mDesign.placement().placeCell(cell, mCellsInitialLocations[cell]);

    //get ancients and fixed cells
    std::vector<Cell> fixedCells;
    std::vector<Cell> ancientsAndFixedCells;
    for (auto cell : chipCells)
        if (mDesign.placement().isFixed(cell))
            fixedCells.push_back(cell);
    for(auto node : kdtree.nonLeafNodes())//Missing assert size == 1 for all non leaf nodes
        std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(ancientsAndFixedCells));
    std::copy(fixedCells.begin(), fixedCells.end(), std::back_inserter(ancientsAndFixedCells));

    //legalize ancients and fix then
    MultirowAbacus multirowAbacus1(mDesign.netlist(), mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping());
    multirowAbacus1.legalizePlacement(ancientsAndFixedCells, util::MultiBox({mPlaceableArea}));
    for(auto cell : ancientsAndFixedCells)
        mDesign.placement().fixLocation(cell, true);

    //get partitions to legalize
    auto nodesToLegalize = kdtree.leafNodes();
    std::vector<bool> legalized_partitions;
    legalized_partitions.resize(nodesToLegalize.size());

    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();

    //legalize partitions
    unsigned count = 0;
    bool legalized = false;
    while(legalized == false)
    {
        //update ancients and add fixed cells
        ancientsAndFixedCells.clear();
        for(auto node : kdtree.nonLeafNodes()) {
            std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(ancientsAndFixedCells));
            node->legalized = true;
        }
        std::copy(fixedCells.begin(), fixedCells.end(), std::back_inserter(ancientsAndFixedCells));

//        #pragma omp parallel for
        for(unsigned partitionIndex = 0; partitionIndex < nodesToLegalize.size(); partitionIndex++)
        {
//            std::cout << "count " << count++ << std::endl;
//            std::cout << "nodes to legalize " << nodesToLegalize.size() << std::endl;

            // get local vector of cells to be legalized
            auto node = nodesToLegalize.at(partitionIndex);

            if (node->parent && !node->parent->legalized) {
                continue;
            }

            std::vector<Cell> cellsToLegalizeWithoutFixed(node->partition.begin(), node->partition.end());

            auto prediction = mLegalizationPrediction.predict(node->range, cellsToLegalizeWithoutFixed);
            if (prediction && node->parent) {
                legalized_partitions[partitionIndex] = false;
                node->parent->legalized = false;
                mergesByMaxDispl++;
                continue;
            }

            std::vector<Cell> cellsToLegalize;
            std::copy(ancientsAndFixedCells.begin(), ancientsAndFixedCells.end(), std::back_inserter(cellsToLegalize));
            std::copy(node->partition.begin(), node->partition.end(), std::back_inserter(cellsToLegalize));

            //legalize partition
            MultirowAbacus multirowAbacus(mDesign.netlist(), mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping());
            auto multibox = util::MultiBox({node->range});
            legalizationCalls++;
            auto result = multirowAbacus.legalizePlacement(cellsToLegalize, multibox);
            bool legalized = result != util::micrometer_t(std::numeric_limits<double>::max());
            legalized_partitions[partitionIndex] = legalized;
            if (node->parent) {
                node->parent->legalized = legalized;
            }
        }

        //get unlegalized partitions
        legalized = true;
        std::vector<Node*> unlegalizedNodes;
        //try to change to lambda
        for(unsigned partitionIndex = 0; partitionIndex < nodesToLegalize.size(); partitionIndex++)
            if(legalized_partitions[partitionIndex] == false){
                legalized = false;
                unlegalizedNodes.push_back(nodesToLegalize.at(partitionIndex));
                mergeCount++;
            }

        //merge unlegalized partitions
        if(legalized == false)
        {
            //check if node is root of KDtree.
            if(!unlegalizedNodes.at(0)->parent)
            {
                legalized = true;
//                std::cout<<"WARNING: Circuit is ILEGAL, the legalization could not be possible even for the whole circuit partition."<<std::endl;
            }else{
                nodesToLegalize.clear();
                nodesToLegalize = kdtree.mergeNodes(unlegalizedNodes);

                //reset cell positions and unfix merged ancients
                for(auto ilegalNode : nodesToLegalize)
                    for(auto cell : ilegalNode->partition){
                        mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
                        mDesign.placement().placeCell(cell, mCellsInitialLocations[cell]);
                    }
            }
        }
    }

    for(auto cell : chipCells) {
        mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
    }
    std::cout<< legalizationCalls << ", " << mergeCount<<", "<<mergesByMaxDispl<<", ";
}

void PartitionedLegalization::allignCellsToNearestSite(){
    for(auto cellIt = mDesign.netlist().begin(Cell()); cellIt != mDesign.netlist().end(Cell()); cellIt++)
    {
        if(!mDesign.placement().isFixed(*cellIt))
        {
            auto initialCellLocation = mDesign.placement().cellLocation(*cellIt);
            auto cellAlignment = mDesign.placementMapping().alignment(*cellIt);

            auto siteHeight = units::unit_cast<double>(mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).y());
            auto siteWidth = units::unit_cast<double>(mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).x());

            double nearSiteX = std::round(initialCellLocation.toPoint().x() / siteWidth) * siteWidth;
            double nearSiteY = std::round(initialCellLocation.toPoint().y() / siteHeight) * siteHeight;


            placement::RowAlignment rowAligment = (((int)(nearSiteY / siteHeight) % 2) == 0) ? placement::RowAlignment::EVEN : placement::RowAlignment::ODD;
            if(cellAlignment == rowAligment || cellAlignment == placement::RowAlignment::NA)
                mDesign.placement().placeCell(*cellIt, util::Location(nearSiteX, nearSiteY));
            else if(initialCellLocation.toPoint().y() >= nearSiteY+0.5*siteHeight)
                mDesign.placement().placeCell(*cellIt, util::Location(nearSiteX, nearSiteY+siteHeight));
            else
                mDesign.placement().placeCell(*cellIt, util::Location(nearSiteX, nearSiteY-siteHeight));
        }
    }
}

void PartitionedLegalization::removeMacroblocksOverlaps(){
    std::vector<circuit::Cell> cells(mDesign.netlist().begin(Cell()), mDesign.netlist().end(Cell()));

    ophidian::legalization::Subrows subrows(mDesign.netlist(), mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping());
    subrows.createSubrows(cells, util::MultiBox({mPlaceableArea}));
    RTree macroblocks_boxes_rtree;
    //create macroblocks rtree
    for (auto cell_it = mDesign.netlist().begin(Cell()); cell_it != mDesign.netlist().end(Cell()); ++cell_it)
    {
        if(mDesign.placement().isFixed(*cell_it))
        {
            auto cellGeometry = mDesign.placementMapping().geometry(*cell_it);
            for(auto cell_box : cellGeometry)
            {
                macroblocks_boxes_rtree.insert(std::make_pair(cell_box, *cell_it));
            }
        }
    }

    //check if cell overlap a macroblock
    for (auto cell_it = mDesign.netlist().begin(Cell()); cell_it != mDesign.netlist().end(Cell()); ++cell_it)
    {
        if(!mDesign.placement().isFixed(*cell_it))
        {
            auto cellGeometry = mDesign.placementMapping().geometry(*cell_it);

//            if (cellGeometry[0].min_corner().y() == 6840 || cellGeometry[0].min_corner().y() == 6840 + 3420) {
//                auto cellName = mDesign.netlist().name(*cell_it);
//                std::cout << "cell " << cellName << " location " << cellGeometry[0].min_corner().x() << ", " << cellGeometry[0].min_corner().y() << std::endl;
//            }

            for(auto cell_box : cellGeometry)
            {
                std::vector<RTreeNode> intersecting_nodes;
                macroblocks_boxes_rtree.query( boost::geometry::index::contains(cell_box), std::back_inserter(intersecting_nodes));
                macroblocks_boxes_rtree.query( boost::geometry::index::overlaps(cell_box), std::back_inserter(intersecting_nodes));

//                if (cellGeometry[0].min_corner().y() == 6840 || cellGeometry[0].min_corner().y() == 6840 + 3420) {
//                    std::cout << "number of intersections " << intersecting_nodes.size() << std::endl;
//                }

                if (!intersecting_nodes.empty())
                {
                    std::vector<ophidian::legalization::Subrow> closestSubrow;
                    auto cellLocation = mDesign.placement().cellLocation(*cell_it);
                    subrows.findClosestSubrows(1, cellLocation, closestSubrow);
                    auto origin = subrows.origin(closestSubrow.at(0));
                    auto upperRightCorner = subrows.upperCorner(closestSubrow.at(0));

//                    if (cellGeometry[0].min_corner().y() == 6840 || cellGeometry[0].min_corner().y() == 6840 + 3420) {
//                        std::cout << "closest subrow origin " << origin.toPoint().x() << ", " << origin.toPoint().y() << std::endl;
//                        std::cout << "closest subrow upper corner " << upperRightCorner.toPoint().x() << ", " << upperRightCorner.toPoint().y() << std::endl;
//                    }

                    if(cellLocation.toPoint().x() <= origin.toPoint().x())
                        mDesign.placement().placeCell(*cell_it, ophidian::util::Location(origin.toPoint().x(), origin.toPoint().y()));
                    else if(cell_box.max_corner().x() >= upperRightCorner.toPoint().x())
                        mDesign.placement().placeCell(*cell_it, ophidian::util::Location(upperRightCorner.toPoint().x() - (cell_box.max_corner().x() - cell_box.min_corner().x()), origin.toPoint().y()));
                    else
                        mDesign.placement().placeCell(*cell_it, ophidian::util::Location(cellLocation.toPoint().x(), origin.toPoint().y()));

//                    if (cellGeometry[0].min_corner().y() == 6840 || cellGeometry[0].min_corner().y() == 6840 + 3420) {
//                        auto cellLocation = mDesign.placement().cellLocation(*cell_it);
//                        std::cout << "new location " << cellLocation.toPoint().x() << ", " << cellLocation.toPoint().y() << std::endl;
//                    }
                }
            }
        }
    }
}

}
}
