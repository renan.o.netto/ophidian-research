#include "LegalizationPrediction.h"

namespace ophidian {
namespace legalization {
LegalizationPrediction::LegalizationPrediction(design::Design &design, std::string modelFile)
    : mDesign(design), mSubrows(design) {
//    std::string modelFile = "./histogram_full_model_10000.pb";
//    std::string modelFile = "./histogram_full_model_20000.pb";
//    std::string modelFile = "./histogram_full_model_30000.pb";
    tensorflow::SessionOptions options;
    mRuntime = 0;

    auto status = tensorflow::NewSession(options, &mSession);
    checkStatus(status);

    status = ReadBinaryProto(tensorflow::Env::Default(), modelFile, &mGraphDef);
    checkStatus(status);

    status = mSession->Create(mGraphDef);
    checkStatus(status);
}

bool LegalizationPrediction::predict(const geometry::Box &area, const std::vector<circuit::Cell> &cells)
{
    unsigned numberOfBins = 20;

    Rtree cellsRtree;

    for (auto cell : cells) {
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        cellsRtree.insert(RtreeNode(cellGeometry, cell));
    }

    std::vector<unsigned> histogramValues;
    histogramValues.resize(numberOfBins + 1, 0);

    tensorflow::Tensor inputs(tensorflow::DT_FLOAT, tensorflow::TensorShape({1, 47}));
//    tensorflow::Tensor inputs(tensorflow::DT_FLOAT, tensorflow::TensorShape({1, 46}));

    addDensityAndAreas(area, cells, inputs);
    addHistogramData(area, cells, inputs);

//    auto inputMap = inputs.tensor<float, 2>();
//    for (unsigned index = 0; index < 47; index++) {
//        std::cout << inputMap(0, index) << ",";
//    }
//    std::cout << std::endl;

    std::vector<std::pair<tensorflow::string, tensorflow::Tensor>> inputTensors = {
        {"dense_1_input", inputs},
    };

    std::vector<tensorflow::Tensor> outputTensors;

    auto status = mSession->Run(inputTensors, {"output_node0"}, {}, &outputTensors);
    checkStatus(status);

    float result = outputTensors[0].scalar<float>()();

//    std::cout << "result " << result << std::endl;

    return result > 0.5;
}

bool LegalizationPrediction:: predict(std::string imageFileName)
{
    //    std::vector<tensorflow::Tensor> inputs;
    //    auto status = readTensorFromImageFile(imageFileName, 150, 150, 0, 255, &inputs);
    //    checkStatus(status);

    //    std::vector<tensorflow::Tensor> outputs;
    //    std::string inputLayer = "conv2d_13_input";
    //    std::string outputLayer = "output_node0";
    //    status = mSession->Run({{inputLayer, inputs[0]}}, {outputLayer}, {}, &outputs);
    //    checkStatus(status);

    //    float result = outputs[0].scalar<float>()();

    //    return result > 0.5;
}

//tensorflow::Status LegalizationPrediction::readTensorFromImageFile(std::__cxx11::string file_name, const int input_height, const int input_width, const float input_mean, const float input_std, std::vector<tensorflow::Tensor> *out_tensors)
//{
//    auto root = tensorflow::Scope::NewRootScope();
//    using namespace ::tensorflow::ops;  // NOLINT(build/namespaces)

//    std::string input_name = "file_reader";
//    std::string output_name = "normalized";
//    auto file_reader = tensorflow::ops::ReadFile(root.WithOpName(input_name),
//                                                 file_name);
//    // Now try to figure out what kind of file it is and decode it.
//    const int wanted_channels = 3;
//    tensorflow::Output image_reader = DecodePng(root.WithOpName("png_reader"), file_reader,
//                                 DecodePng::Channels(wanted_channels));
//    // Now cast the image data to float so we can do normal math on it.
//    auto float_caster =
//            Cast(root.WithOpName("float_caster"), image_reader, tensorflow::DT_FLOAT);
//    // The convention for image ops in TensorFlow is that all images are expected
//    // to be in batches, so that they're four-dimensional arrays with indices of
//    // [batch, height, width, channel]. Because we only have a single image, we
//    // have to add a batch dimension of 1 to the start with ExpandDims().
//    auto dims_expander = ExpandDims(root, float_caster, 0);
//    // Bilinearly resize the image to fit the required dimensions.
//    auto resized = ResizeBilinear(
//                root, dims_expander,
//                Const(root.WithOpName("size"), {input_height, input_width}));
//    // Subtract the mean and divide by the scale.
//    Div(root.WithOpName(output_name), Sub(root, resized, {input_mean}),
//    {input_std});

//    // This runs the GraphDef network definition that we've just constructed, and
//    // returns the results in the output tensor.
//    tensorflow::GraphDef graph;
//    TF_RETURN_IF_ERROR(root.ToGraphDef(&graph));

//    std::unique_ptr<tensorflow::Session> session(
//                tensorflow::NewSession(tensorflow::SessionOptions()));
//    TF_RETURN_IF_ERROR(session->Create(graph));
//    TF_RETURN_IF_ERROR(session->Run({}, {output_name}, {}, out_tensors));
//    return tensorflow::Status::OK();
//}

void LegalizationPrediction::checkStatus(const tensorflow::Status &status)
{
    if (!status.ok()) {
        std::cout << status.ToString() << std::endl;
    }
}

void LegalizationPrediction::addDensityAndAreas(const geometry::Box &area, const std::vector<circuit::Cell> &cells, tensorflow::Tensor &inputs)
{
    auto site = *mDesign.floorplan().sitesRange().begin();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    auto densityMax = 104.5;
    std::vector<float> areasMax = {5.77, 0.35, 0.34, 0.32};

    auto partitionArea = boost::geometry::area(area);
    double areaOfFixedCells = 0;
    double areaOfMovableCells = 0;

    std::vector<double> areas;
    areas.resize(4, 0);

//    std::cout << "partition " << area.min_corner().x() << ", " << area.min_corner().y() << " -> "
//              << area.max_corner().x() << ", " << area.max_corner().y() << std::endl;

    for (auto & cell : cells) {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];

        if (!mDesign.placement().isFixed(cell)) {
            auto cellArea = boost::geometry::area(cellBox);
    //        auto cellWidth = cellBox.max_corner().x() - cellBox.min_corner().x();
            auto cellHeight = cellBox.max_corner().y() - cellBox.min_corner().y();
    //        auto cellArea = cellWidth * cellHeight;

            cellHeight = (cellBox.max_corner().y() - cellBox.min_corner().y()) / rowHeight;
            areas[cellHeight-1] += cellArea;
        }

        if (boost::geometry::intersects(cellBox, area)) {
            geometry::Box intersection;
            boost::geometry::intersection(cellBox, area, intersection);
            auto intersectionArea = boost::geometry::area(intersection);

            if (mDesign.placement().isFixed(cell)) {
//                auto cellName = mDesign.netlist().name(cell);
//                std::cout << "fixed cell name " << cellName << std::endl;
//                std::cout << "fixed cell " << cellBox.min_corner().x() << ", " << cellBox.min_corner().y() << " -> "
//                          << cellBox.max_corner().x() << ", " << cellBox.max_corner().y() << std::endl;
//                std::cout << "intersection " << intersection.min_corner().x() << ", " << intersection.min_corner().y() << " -> "
//                          << intersection.max_corner().x() << ", " << intersection.max_corner().y() << std::endl;
//                std::cout << "intersection area " << intersectionArea << std::endl;
                areaOfFixedCells += intersectionArea;
            } else {
                areaOfMovableCells += intersectionArea;
            }
        }
    }

//    std::cout << "partition area " << partitionArea << std::endl;
//    std::cout << "fixed area " << areaOfFixedCells << std::endl;
//    std::cout << "movable area " << areaOfMovableCells << std::endl;

    auto density = (partitionArea != areaOfFixedCells) ? areaOfMovableCells / (partitionArea - areaOfFixedCells) : densityMax;

    auto inputMap = inputs.tensor<float, 2>();
    inputMap(0, 0) = (float)density / (float)densityMax;
//    inputMap(0, 0) = density;

    unsigned beginIndex = 1;
//    unsigned beginIndex = 0;
    for (unsigned index = 0; index < areas.size(); index++) {
        float area = (float)areas.at(index) / (float)partitionArea;
//        inputMap(0, beginIndex+index) = area / (float)areasMax.at(index);
        inputMap(0, beginIndex+index) = (float)area / (float) areasMax.at(index);;
    }
}

void LegalizationPrediction::addHistogramData(const geometry::Box &area, const std::vector<circuit::Cell> &cells, tensorflow::Tensor &inputs)
{
    unsigned numberOfBins = 20;

    std::vector<float> binMax = {1, 1, 1, 1, 1, 1, 0.9, 1, 0.83, 1, 0.71, 1, 1, 1, 1, 0.81, 0.71, 0.91, 0.88, 1, 1, 1};
    std::vector<float> overlapMax = {1, 0.88, 0.77, 0.75, 0.75, 0.66, 1, 0.75, 0.64, 0.8, 0.75, 0.8, 0.75, 0.66, 0.8, 1, 0.75, 0.66, 0.66, 0.66, 1};

    Rtree cellsRtree;

    for (auto cell : cells) {
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        cellsRtree.insert(RtreeNode(cellGeometry, cell));
    }

    auto site = *mDesign.floorplan().sitesRange().begin();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    std::vector<unsigned> histogramValues;
    histogramValues.resize(numberOfBins + 1, 0);
    std::vector<unsigned> overlapValues;
    overlapValues.resize(numberOfBins + 1, 0);

    auto multibox = util::MultiBox({area});
    mSubrows.createSubrows(cells, multibox);

    std::set<std::string> checkedCells;

    unsigned numberOfSubrows = mSubrows.rowCount();

    for (auto subrow : mSubrows.range(Subrow())) {
        auto subrowOrigin = mSubrows.origin(subrow).toPoint();
        auto subrowUpperCorner = mSubrows.upperCorner(subrow).toPoint();

        auto subrowBox = geometry::Box(subrowOrigin, subrowUpperCorner);

        std::vector<RtreeNode> intersectingNodes;
        cellsRtree.query(boost::geometry::index::overlaps(subrowBox), std::back_inserter(intersectingNodes));
        cellsRtree.query(boost::geometry::index::within(subrowBox), std::back_inserter(intersectingNodes));
        cellsRtree.query(boost::geometry::index::contains(subrowBox), std::back_inserter(intersectingNodes));

        double overlapWidth = 0.0;
        double overlapWithOtherCells = 0.0;
        for (auto node : intersectingNodes) {
            auto cellWidth = node.first.max_corner().x() - node.first.min_corner().x();
            overlapWidth += cellWidth;

            auto cellBox = node.first;

            std::vector<RtreeNode> otherIntersections;
            cellsRtree.query(boost::geometry::index::overlaps(cellBox), std::back_inserter(otherIntersections));
            cellsRtree.query(boost::geometry::index::within(cellBox), std::back_inserter(otherIntersections));
            cellsRtree.query(boost::geometry::index::contains(cellBox), std::back_inserter(otherIntersections));

            for (auto otherNode : otherIntersections) {
                auto otherCell = otherNode.second;
                auto otherCellName = mDesign.netlist().name(otherCell);

                if (checkedCells.find(otherCellName) == checkedCells.end()) {
                    auto otherBox = otherNode.first;
                    geometry::Box intersectionBox;
                    boost::geometry::intersection(cellBox, otherBox, intersectionBox);
                    auto intersectionWidth = intersectionBox.max_corner().x() - intersectionBox.min_corner().x();
                    overlapWithOtherCells += intersectionWidth;
                }
            }

            auto cell = node.second;
            auto cellName = mDesign.netlist().name(cell);
            checkedCells.insert(cellName);
        }

        auto subrowWidth = subrowUpperCorner.x() - subrowOrigin.x();
        auto areaPercentage = (overlapWidth / subrowWidth) * 100;
        auto areaPercentageOfOverlaps = (overlapWithOtherCells / subrowWidth) * 100;

        unsigned histogramIndex = std::min(numberOfBins, (unsigned)(areaPercentage / (100 / numberOfBins)));
        histogramValues.at(histogramIndex) += 1;

        unsigned overlapIndex = std::min(numberOfBins, (unsigned)(areaPercentageOfOverlaps / (100 / numberOfBins)));
        overlapValues.at(overlapIndex) += 1;
    }

    unsigned beginIndex = 5;
    auto inputMap = inputs.tensor<float, 2>();
    for (unsigned index = 0; index < histogramValues.size(); index++) {
//        inputMap(0, beginIndex+index) = (float)histogramValues.at(index) / (float)binMax.at(index);
        inputMap(0, beginIndex+index) = ((float) histogramValues.at(index) / (float) numberOfSubrows) / (float) binMax.at(index);
    }
    beginIndex += histogramValues.size();
    for (unsigned index = 0; index < overlapValues.size(); index++) {
//        inputMap(0, beginIndex+index) = (float)overlapValues.at(index) / (float)overlapMax.at(index);
        inputMap(0, beginIndex+index) = ((float) overlapValues.at(index) / (float) numberOfSubrows) / (float) overlapMax.at(index);
    }
}

}
}
