#include "ILPLegalizationWithConstraintGraph.h"

namespace ophidian {
namespace legalization {
ILPLegalizationWithConstraintGraph::ILPLegalizationWithConstraintGraph(design::Design &design)
    : mDesign(design),
      mInitialLocations(design.netlist().makeProperty<util::Location>(circuit::Cell()))
{
    setInitialLocations(mDesign);
}

void ILPLegalizationWithConstraintGraph::setInitialLocations(design::Design &design)
{
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mInitialLocations[currentDesignCell] = design.placement().cellLocation(cell);
    }
}

util::micrometer_t ILPLegalizationWithConstraintGraph::legalize(const std::vector<circuit::Cell> &cells, const geometry::Box & legalizationArea, bool trial)
{
    ConstraintGraph<LeftComparator> horizontalConstraintGraph(mDesign);
    ConstraintGraph<BelowComparator> verticalConstraintGraph(mDesign);

    auto origin = legalizationArea.min_corner();
    auto upperRightCorner = legalizationArea.max_corner();

    horizontalConstraintGraph.buildConstraintGraph(cells, util::micrometer_t(origin.x()), util::micrometer_t(upperRightCorner.x()));
    verticalConstraintGraph.buildConstraintGraph(cells, util::micrometer_t(origin.y()), util::micrometer_t(upperRightCorner.y()));

//    horizontalConstraintGraph.removeTransitiveEdges();
//    verticalConstraintGraph.removeTransitiveEdges();

    legalize(horizontalConstraintGraph, verticalConstraintGraph, cells, legalizationArea, trial);
}

util::micrometer_t ILPLegalizationWithConstraintGraph::legalize(ConstraintGraph<LeftComparator> &horizontalConstraintGraph, ConstraintGraph<BelowComparator> &verticalConstraintGraph, const std::vector<circuit::Cell> &cells, const geometry::Box &legalizationArea, bool trial)
{
    struct timeval startTime;
    struct timeval endTime;

    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();

    auto origin = legalizationArea.min_corner();
    auto upperRightCorner = legalizationArea.max_corner();

    auto evenOrigin = ((int)(origin.y() / rowHeight) % 2) == 0 ? origin.y() : origin.y() + rowHeight;
    auto oddOrigin = ((int)(origin.y() / rowHeight) % 2) == 1 ? origin.y() : origin.y() + rowHeight;

    GRBEnv env = GRBEnv();

    GRBModel model = GRBModel(env);

    entity_system::Property<circuit::Cell, GRBVar> xVariables = mDesign.netlist().makeProperty<GRBVar>(circuit::Cell());
    entity_system::Property<circuit::Cell, GRBVar> yVariables = mDesign.netlist().makeProperty<GRBVar>(circuit::Cell());

    GRBQuadExpr objective = 0.0;

    auto numberOfRows = (upperRightCorner.y() - origin.y()) / rowHeight;

    gettimeofday(&startTime, NULL);
    for (auto cell : cells) {
        auto cellInitialLocation = mInitialLocations[cell];
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
        auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();

        auto cellName = mDesign.netlist().name(cell);

        GRBVar x;
        GRBVar y;

        if (mDesign.placement().isFixed(cell)) {
            x = model.addVar(cellGeometry.min_corner().x(), cellGeometry.min_corner().x(),
                                    cellGeometry.min_corner().x(), GRB_CONTINUOUS, cellName + "_x");
            y = model.addVar(cellGeometry.min_corner().y(), cellGeometry.min_corner().y(),
                                    cellGeometry.min_corner().y(), GRB_CONTINUOUS, cellName + "_y");
        } else {
            geometry::Point alignedOrigin(std::ceil(origin.x() / siteWidth) * siteWidth, std::ceil(origin.y() / rowHeight) * rowHeight);
            geometry::Point alignedTop(std::floor(upperRightCorner.x() / siteWidth) * siteWidth, std::floor(upperRightCorner.y() / rowHeight) * rowHeight);
            x = model.addVar(alignedOrigin.x(), alignedTop.x() - cellWidth,
                                    alignedOrigin.x(), GRB_CONTINUOUS, cellName + "_x");
            y = model.addVar(alignedOrigin.y(), alignedTop.y() - cellHeight,
                                    alignedOrigin.y(), GRB_CONTINUOUS, cellName + "_y");

            auto cellAlignment = mDesign.placementMapping().alignment(cell);
            if (cellAlignment == placement::RowAlignment::EVEN) {
                GRBVar alignment = model.addVar(0, numberOfRows, 0, GRB_INTEGER, cellName + "_alignment");
                model.addConstr(y == 2*alignment*rowHeight + evenOrigin, cellName + "_alignment_constraint");
            } else if (cellAlignment == placement::RowAlignment::ODD) {
                GRBVar alignment = model.addVar(0, numberOfRows, 0, GRB_INTEGER, cellName + "_alignment");
                model.addConstr(y == 2*alignment*rowHeight + oddOrigin, cellName + "_alignment_constraint");
            }

            double initialX = cellInitialLocation.toPoint().x();
            double initialY = cellInitialLocation.toPoint().y();
            objective += (initialX - x)*(initialX - x);
            objective += (initialY - y)*(initialY - y);
        }
        xVariables[cell] = x;
        yVariables[cell] = y;
    }

    gettimeofday(&startTime, NULL);
    for (auto cell1 : cells) {
        for (auto cell2 : cells) {
            if (cell1 != cell2) {
                auto cell1Geometry = mDesign.placementMapping().geometry(cell1)[0];
                auto cell1Name = mDesign.netlist().name(cell1);
                auto cell2Name = mDesign.netlist().name(cell2);

                if (horizontalConstraintGraph.hasEdge(cell1, cell2)) {
                    auto cell1Var = xVariables[cell1];
                    auto cell1Width = cell1Geometry.max_corner().x() - cell1Geometry.min_corner().x();
                    auto cell2Var = xVariables[cell2];
                    model.addConstr(cell1Var + cell1Width <= cell2Var, cell1Name + "_" + cell2Name + "_horizontal_overlap_constraint");
                }
                if (verticalConstraintGraph.hasEdge(cell1, cell2)) {
                    auto cell1Var = yVariables[cell1];
                    auto cell1Height = cell1Geometry.max_corner().y() - cell1Geometry.min_corner().y();
                    auto cell2Var = yVariables[cell2];
                    model.addConstr(cell1Var + cell1Height <= cell2Var, cell1Name + "_" + cell2Name + "_vertical_overlap_constraint");
                }
            }
        }
    }
    gettimeofday(&endTime, NULL);
    mTimeBuildingModel += endTime.tv_sec - startTime.tv_sec + (endTime.tv_usec - startTime.tv_usec)/1e6;

    gettimeofday(&startTime, NULL);

    model.setObjective(objective, GRB_MINIMIZE);

    model.set(GRB_IntParam_BarHomogeneous, GRB_BARHOMOGENEOUS_ON);

    model.set(GRB_IntParam_OutputFlag, 0);

//    model.write("ilp_legalization.lp");

    model.optimize();

    gettimeofday(&endTime, NULL);
    mTimeSolvingModel += endTime.tv_sec - startTime.tv_sec + (endTime.tv_usec - startTime.tv_usec)/1e6;

    if (model.get(GRB_IntAttr_Status) != GRB_OPTIMAL) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

    if (!trial) {
        for (auto cell : cells) {
            auto cellX = xVariables[cell];
            auto cellY = yVariables[cell];

            auto cellXLocation = std::round(cellX.get(GRB_DoubleAttr_X) / siteWidth) * siteWidth;
            auto cellYLocation = std::round(cellY.get(GRB_DoubleAttr_X) / rowHeight) * rowHeight;

            util::Location legalLocation(cellXLocation, cellYLocation);
            mDesign.placement().placeCell(cell, legalLocation);
        }
    }

    return util::micrometer_t(model.get(GRB_DoubleAttr_ObjVal));
}

}
}
