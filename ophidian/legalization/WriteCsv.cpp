#include "WriteCsv.h"

namespace ophidian
{
namespace legalization
{
WriteCsv::WriteCsv(design::Design &design, std::string fileName)
    : mDesign(design)
{
    //    mFileStream.open (fileName, std::fstream::in | std::fstream::out | std::fstream::app);
    mFileStream.open(fileName);
}

void WriteCsv::writeCsvForLegalizationArea(geometry::Box &area, const std::vector<circuit::Cell> &cells, circuit::Cell targetCell, util::micrometer_t result)
{
    auto site = *mDesign.floorplan().sitesRange().begin();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(site).toPoint().x();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    auto partitionArea = boost::geometry::area(area);
    double areaOfFixedCells = 0;
    double areaOfMovableCells = 0;

    std::unordered_map<unsigned, unsigned> numberOfMovableCellsPerHeight;
    numberOfMovableCellsPerHeight[1] = 0;
    numberOfMovableCellsPerHeight[2] = 0;
    numberOfMovableCellsPerHeight[3] = 0;
    numberOfMovableCellsPerHeight[4] = 0;

    std::unordered_map<unsigned, unsigned> numberOfFixedCellsPerHeight;
    numberOfFixedCellsPerHeight[1] = 0;
    numberOfFixedCellsPerHeight[2] = 0;
    numberOfFixedCellsPerHeight[3] = 0;
    numberOfFixedCellsPerHeight[4] = 0;

    std::unordered_map<unsigned, unsigned> numberOfMovableCellsPerWidth;
    numberOfMovableCellsPerWidth[2] = 0;
    numberOfMovableCellsPerWidth[4] = 0;
    numberOfMovableCellsPerWidth[6] = 0;
    numberOfMovableCellsPerWidth[8] = 0;
    numberOfMovableCellsPerWidth[12] = 0;
    numberOfMovableCellsPerWidth[14] = 0;
    numberOfMovableCellsPerWidth[16] = 0;
    numberOfMovableCellsPerWidth[18] = 0;
    numberOfMovableCellsPerWidth[256] = 0;

    std::unordered_map<unsigned, unsigned> numberOfFixedCellsPerWidth;
    numberOfFixedCellsPerWidth[2] = 0;
    numberOfFixedCellsPerWidth[4] = 0;
    numberOfFixedCellsPerWidth[6] = 0;
    numberOfFixedCellsPerWidth[8] = 0;
    numberOfFixedCellsPerWidth[12] = 0;
    numberOfFixedCellsPerWidth[14] = 0;
    numberOfFixedCellsPerWidth[16] = 0;
    numberOfFixedCellsPerWidth[18] = 0;
    numberOfFixedCellsPerWidth[256] = 0;

    unsigned numberOfMovableEvenCells = 0;
    unsigned numberOfMovableOddCells = 0;

    unsigned numberOfFixedEvenCells = 0;
    unsigned numberOfFixedOddCells = 0;

    auto targetCellBox = mDesign.placementMapping().geometry(targetCell)[0];
    double areaOfOverlappingCells = boost::geometry::area(targetCellBox);

    for (auto cell : cells)
    {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];

        if (boost::geometry::overlaps(cellBox, targetCellBox) || boost::geometry::within(cellBox, targetCellBox) || boost::geometry::within(targetCellBox, cellBox))
        {
            areaOfOverlappingCells += boost::geometry::area(cellBox);
        }

        if (boost::geometry::intersects(cellBox, area))
        {
            geometry::Box intersection;
            boost::geometry::intersection(cellBox, area, intersection);
            auto intersectionArea = boost::geometry::area(intersection);

            auto cellHeight = (cellBox.max_corner().y() - cellBox.min_corner().y()) / rowHeight;
            auto cellWidth = (cellBox.max_corner().x() - cellBox.min_corner().x()) / siteWidth;
            auto cellAlignment = mDesign.placementMapping().alignment(cell);

            if (mDesign.placement().isFixed(cell))
            {
                areaOfFixedCells += intersectionArea;

                numberOfFixedCellsPerHeight[cellHeight] += 1;
                numberOfFixedCellsPerWidth[cellWidth] += 1;

                if (cellAlignment == placement::RowAlignment::EVEN)
                {
                    numberOfFixedEvenCells++;
                }
                else if (cellAlignment == placement::RowAlignment::ODD)
                {
                    numberOfFixedOddCells++;
                }
            }
            else
            {
                areaOfMovableCells += intersectionArea;

                numberOfMovableCellsPerHeight[cellHeight] += 1;
                numberOfMovableCellsPerWidth[cellWidth] += 1;

                if (cellAlignment == placement::RowAlignment::EVEN)
                {
                    numberOfMovableEvenCells++;
                }
                else if (cellAlignment == placement::RowAlignment::ODD)
                {
                    numberOfMovableOddCells++;
                }
            }
        }
    }

    auto boolResult = (result != util::micrometer_t(std::numeric_limits<double>::max()));

    mFileStream << numberOfMovableCellsPerHeight[1] << ",";
    mFileStream << numberOfMovableCellsPerHeight[2] << ",";
    mFileStream << numberOfMovableCellsPerHeight[3] << ",";
    mFileStream << numberOfMovableCellsPerHeight[4] << ",";
    mFileStream << numberOfMovableCellsPerWidth[2] << ",";
    mFileStream << numberOfMovableCellsPerWidth[4] << ",";
    mFileStream << numberOfMovableCellsPerWidth[6] << ",";
    mFileStream << numberOfMovableCellsPerWidth[8] << ",";
    mFileStream << numberOfMovableCellsPerWidth[12] << ",";
    mFileStream << numberOfMovableCellsPerWidth[14] << ",";
    mFileStream << numberOfMovableCellsPerWidth[16] << ",";
    mFileStream << numberOfMovableCellsPerWidth[18] << ",";
    mFileStream << numberOfMovableCellsPerWidth[256] << ",";
    mFileStream << numberOfMovableEvenCells << ",";
    mFileStream << numberOfMovableOddCells << ",";
    mFileStream << numberOfFixedCellsPerHeight[1] << ",";
    mFileStream << numberOfFixedCellsPerHeight[2] << ",";
    mFileStream << numberOfFixedCellsPerHeight[3] << ",";
    mFileStream << numberOfFixedCellsPerHeight[4] << ",";
    mFileStream << numberOfFixedCellsPerWidth[2] << ",";
    mFileStream << numberOfFixedCellsPerWidth[4] << ",";
    mFileStream << numberOfFixedCellsPerWidth[6] << ",";
    mFileStream << numberOfFixedCellsPerWidth[8] << ",";
    mFileStream << numberOfFixedCellsPerWidth[12] << ",";
    mFileStream << numberOfFixedCellsPerWidth[14] << ",";
    mFileStream << numberOfFixedCellsPerWidth[16] << ",";
    mFileStream << numberOfFixedCellsPerWidth[18] << ",";
    mFileStream << numberOfFixedCellsPerWidth[256] << ",";
    mFileStream << numberOfFixedEvenCells << ",";
    mFileStream << numberOfFixedOddCells << ",";
    mFileStream << areaOfOverlappingCells << ",";
    mFileStream << partitionArea << ",";
    mFileStream << areaOfFixedCells << ",";
    mFileStream << areaOfMovableCells << ",";
    mFileStream << units::unit_cast<double>(result) << ",";
    mFileStream << boolResult << std::endl;
}

void WriteCsv::writeCsvForLegalizationArea(geometry::Box &area, const std::vector<circuit::Cell> &cells, util::micrometer_t result, bool boolResult)
{
    auto site = *mDesign.floorplan().sitesRange().begin();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(site).toPoint().x();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    auto partitionArea = boost::geometry::area(area);
    double areaOfFixedCells = 0;
    double areaOfMovableCells = 0;

    std::unordered_map<unsigned, unsigned> numberOfMovableCellsPerHeight;
    numberOfMovableCellsPerHeight[1] = 0;
    numberOfMovableCellsPerHeight[2] = 0;
    numberOfMovableCellsPerHeight[3] = 0;
    numberOfMovableCellsPerHeight[4] = 0;

    std::unordered_map<unsigned, unsigned> numberOfFixedCellsPerHeight;
    numberOfFixedCellsPerHeight[1] = 0;
    numberOfFixedCellsPerHeight[2] = 0;
    numberOfFixedCellsPerHeight[3] = 0;
    numberOfFixedCellsPerHeight[4] = 0;

    std::unordered_map<unsigned, unsigned> numberOfMovableCellsPerWidth;
    numberOfMovableCellsPerWidth[2] = 0;
    numberOfMovableCellsPerWidth[4] = 0;
    numberOfMovableCellsPerWidth[6] = 0;
    numberOfMovableCellsPerWidth[8] = 0;
    numberOfMovableCellsPerWidth[12] = 0;
    numberOfMovableCellsPerWidth[14] = 0;
    numberOfMovableCellsPerWidth[16] = 0;
    numberOfMovableCellsPerWidth[18] = 0;
    numberOfMovableCellsPerWidth[256] = 0;

    std::unordered_map<unsigned, unsigned> numberOfFixedCellsPerWidth;
    numberOfFixedCellsPerWidth[2] = 0;
    numberOfFixedCellsPerWidth[4] = 0;
    numberOfFixedCellsPerWidth[6] = 0;
    numberOfFixedCellsPerWidth[8] = 0;
    numberOfFixedCellsPerWidth[12] = 0;
    numberOfFixedCellsPerWidth[14] = 0;
    numberOfFixedCellsPerWidth[16] = 0;
    numberOfFixedCellsPerWidth[18] = 0;
    numberOfFixedCellsPerWidth[256] = 0;

    unsigned numberOfMovableEvenCells = 0;
    unsigned numberOfMovableOddCells = 0;

    unsigned numberOfFixedEvenCells = 0;
    unsigned numberOfFixedOddCells = 0;

    for (auto cell : cells)
    {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];

        if (boost::geometry::intersects(cellBox, area))
        {
            geometry::Box intersection;
            boost::geometry::intersection(cellBox, area, intersection);
            auto intersectionArea = boost::geometry::area(intersection);

            auto cellHeight = (cellBox.max_corner().y() - cellBox.min_corner().y()) / rowHeight;
            auto cellWidth = (cellBox.max_corner().x() - cellBox.min_corner().x()) / siteWidth;
            auto cellAlignment = mDesign.placementMapping().alignment(cell);

            if (mDesign.placement().isFixed(cell))
            {
                areaOfFixedCells += intersectionArea;

                numberOfFixedCellsPerHeight[cellHeight] += 1;
                numberOfFixedCellsPerWidth[cellWidth] += 1;

                if (cellAlignment == placement::RowAlignment::EVEN)
                {
                    numberOfFixedEvenCells++;
                }
                else if (cellAlignment == placement::RowAlignment::ODD)
                {
                    numberOfFixedOddCells++;
                }
            }
            else
            {
                areaOfMovableCells += intersectionArea;

                numberOfMovableCellsPerHeight[cellHeight] += 1;
                numberOfMovableCellsPerWidth[cellWidth] += 1;

                if (cellAlignment == placement::RowAlignment::EVEN)
                {
                    numberOfMovableEvenCells++;
                }
                else if (cellAlignment == placement::RowAlignment::ODD)
                {
                    numberOfMovableOddCells++;
                }
            }
        }
    }

    mFileStream << numberOfMovableCellsPerHeight[1] << ",";
    mFileStream << numberOfMovableCellsPerHeight[2] << ",";
    mFileStream << numberOfMovableCellsPerHeight[3] << ",";
    mFileStream << numberOfMovableCellsPerHeight[4] << ",";
    mFileStream << numberOfMovableCellsPerWidth[2] << ",";
    mFileStream << numberOfMovableCellsPerWidth[4] << ",";
    mFileStream << numberOfMovableCellsPerWidth[6] << ",";
    mFileStream << numberOfMovableCellsPerWidth[8] << ",";
    mFileStream << numberOfMovableCellsPerWidth[12] << ",";
    mFileStream << numberOfMovableCellsPerWidth[14] << ",";
    mFileStream << numberOfMovableCellsPerWidth[16] << ",";
    mFileStream << numberOfMovableCellsPerWidth[18] << ",";
    mFileStream << numberOfMovableCellsPerWidth[256] << ",";
    mFileStream << numberOfMovableEvenCells << ",";
    mFileStream << numberOfMovableOddCells << ",";
    mFileStream << numberOfFixedCellsPerHeight[1] << ",";
    mFileStream << numberOfFixedCellsPerHeight[2] << ",";
    mFileStream << numberOfFixedCellsPerHeight[3] << ",";
    mFileStream << numberOfFixedCellsPerHeight[4] << ",";
    mFileStream << numberOfFixedCellsPerWidth[2] << ",";
    mFileStream << numberOfFixedCellsPerWidth[4] << ",";
    mFileStream << numberOfFixedCellsPerWidth[6] << ",";
    mFileStream << numberOfFixedCellsPerWidth[8] << ",";
    mFileStream << numberOfFixedCellsPerWidth[12] << ",";
    mFileStream << numberOfFixedCellsPerWidth[14] << ",";
    mFileStream << numberOfFixedCellsPerWidth[16] << ",";
    mFileStream << numberOfFixedCellsPerWidth[18] << ",";
    mFileStream << numberOfFixedCellsPerWidth[256] << ",";
    mFileStream << numberOfFixedEvenCells << ",";
    mFileStream << numberOfFixedOddCells << ",";
    mFileStream << partitionArea << ",";
    mFileStream << areaOfFixedCells << ",";
    mFileStream << areaOfMovableCells << ",";
    //    mFileStream << units::unit_cast<double>(result) << ",";
    //    mFileStream << boolResult << std::endl;
}

void WriteCsv::writeCsvOfPixelsForLegalizationArea(geometry::Box &area, const std::vector<circuit::Cell> &cells, util::micrometer_t result)
{
    Rtree cellsRtree;

    for (auto cell : cells)
    {
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        cellsRtree.insert(RtreeNode(cellGeometry, cell));
    }

    auto site = *mDesign.floorplan().sitesRange().begin();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(site).toPoint().x();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    auto numberOfRows = (area.max_corner().y() - area.min_corner().y()) / rowHeight;
    auto numberOfSites = (area.max_corner().x() - area.min_corner().x()) / siteWidth;

    std::cout << "number of rows " << numberOfRows << std::endl;
    std::cout << "number of sites " << numberOfSites << std::endl;

    if (numberOfRows != 10 || numberOfSites != 100)
    {
        std::cout << "stop" << std::endl;
    }

    for (unsigned rowIndex = 0; rowIndex < numberOfRows; rowIndex++)
    {
        for (unsigned siteIndex = 0; siteIndex < numberOfSites; siteIndex++)
        {
            geometry::Point siteMinCorner(area.min_corner().x() + siteIndex * siteWidth,
                                          area.min_corner().y() + rowIndex * rowHeight);
            geometry::Point siteMaxCorner(siteMinCorner.x() + siteWidth, siteMinCorner.y() + rowHeight);
            geometry::Box siteBox(siteMinCorner, siteMaxCorner);

            std::vector<RtreeNode> intersectingNodes;
            cellsRtree.query(boost::geometry::index::overlaps(siteBox), std::back_inserter(intersectingNodes));
            cellsRtree.query(boost::geometry::index::within(siteBox), std::back_inserter(intersectingNodes));
            cellsRtree.query(boost::geometry::index::contains(siteBox), std::back_inserter(intersectingNodes));

            mFileStream << intersectingNodes.size() << ",";
        }
    }

    auto boolResult = (result != util::micrometer_t(std::numeric_limits<double>::max()));

    mFileStream << units::unit_cast<double>(result) << ",";
    mFileStream << boolResult << std::endl;
}

void WriteCsv::writeCsvOfHistogram(geometry::Box &area, const std::vector<circuit::Cell> &cells, util::micrometer_t result, bool boolResult, double runtime)
{
    //    BinDecomposition binDecomposition(mDesign);
    //    binDecomposition.decomposeCircuitInBins(area, cells, 4);

    unsigned numberOfBins = 20;

    Rtree cellsRtree;

    for (auto cell : cells)
    {
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        cellsRtree.insert(RtreeNode(cellGeometry, cell));
    }

    auto site = *mDesign.floorplan().sitesRange().begin();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    std::vector<unsigned> histogramValues;
    histogramValues.resize(numberOfBins + 1, 0);
    std::vector<unsigned> overlapValues;
    overlapValues.resize(numberOfBins + 1, 0);

    Subrows subrows(mDesign);
    util::MultiBox multibox({area});
    subrows.createSubrows(cells, multibox);
    //    subrows.splitSubrows(util::micrometer_t(1000));

    std::set<std::string> checkedCells;

    unsigned numberOfSubrows = subrows.rowCount();
    //    unsigned numberOfSubrows = binDecomposition.size(Bin());

    for (auto subrow : subrows.range(Subrow()))
    {
        //    for (auto bin : binDecomposition.range(Bin())) {
        auto subrowOrigin = subrows.origin(subrow).toPoint();
        auto subrowUpperCorner = subrows.upperCorner(subrow).toPoint();

        auto subrowBox = geometry::Box(subrowOrigin, subrowUpperCorner);

        //        auto subrowBox = binDecomposition.box(bin);

        std::vector<RtreeNode> intersectingNodes;
        cellsRtree.query(boost::geometry::index::overlaps(subrowBox), std::back_inserter(intersectingNodes));
        cellsRtree.query(boost::geometry::index::within(subrowBox), std::back_inserter(intersectingNodes));
        cellsRtree.query(boost::geometry::index::contains(subrowBox), std::back_inserter(intersectingNodes));

        double overlapWidth = 0.0;
        double overlapWithOtherCells = 0.0;
        for (auto node : intersectingNodes)
        {
            auto cellWidth = node.first.max_corner().x() - node.first.min_corner().x();
            overlapWidth += cellWidth;
            //            overlapWidth += boost::geometry::area(node.first);

            auto cellBox = node.first;

            std::vector<RtreeNode> otherIntersections;
            cellsRtree.query(boost::geometry::index::overlaps(cellBox), std::back_inserter(otherIntersections));
            cellsRtree.query(boost::geometry::index::within(cellBox), std::back_inserter(otherIntersections));
            cellsRtree.query(boost::geometry::index::contains(cellBox), std::back_inserter(otherIntersections));

            for (auto otherNode : otherIntersections)
            {
                auto otherCell = otherNode.second;
                auto otherCellName = mDesign.netlist().name(otherCell);

                if (checkedCells.find(otherCellName) == checkedCells.end())
                {
                    auto otherBox = otherNode.first;
                    geometry::Box intersectionBox;
                    boost::geometry::intersection(cellBox, otherBox, intersectionBox);
                    auto intersectionWidth = intersectionBox.max_corner().x() - intersectionBox.min_corner().x();
                    //                    auto intersectionWidth = boost::geometry::area(intersectionBox);
                    overlapWithOtherCells += intersectionWidth;
                }
            }

            auto cell = node.second;
            auto cellName = mDesign.netlist().name(cell);
            checkedCells.insert(cellName);
        }

        auto subrowWidth = subrowUpperCorner.x() - subrowOrigin.x();
        //        auto subrowWidth = boost::geometry::area(subrowBox);
        auto areaPercentage = (overlapWidth / subrowWidth) * 100;
        auto areaPercentageOfOverlaps = (overlapWithOtherCells / subrowWidth) * 100;

        unsigned histogramIndex = std::min(numberOfBins, (unsigned)(areaPercentage / (100 / numberOfBins)));
        histogramValues.at(histogramIndex) += 1;

        unsigned overlapIndex = std::min(numberOfBins, (unsigned)(areaPercentageOfOverlaps / (100 / numberOfBins)));
        overlapValues.at(overlapIndex) += 1;
        //        mFileStream << areaPercentage << ",";
    }

    for (auto value : histogramValues)
    {
        mFileStream << (double)value / (double)numberOfSubrows << ",";
    }

    for (auto value : overlapValues)
    {
        mFileStream << (double)value / (double)numberOfSubrows << ",";
    }

    //    auto boolResult = (result != util::micrometer_t(std::numeric_limits<double>::max()));

    //    mFileStream << units::unit_cast<double>(result) << ",";
    //    mFileStream << boolResult << ",";
    //    mFileStream << runtime << std::endl;
}

void WriteCsv::writeCsvOfHistogramWithAreaOfMultirowCells(geometry::Box area, const std::vector<circuit::Cell> &cells, util::micrometer_t result, bool boolResult, double runtime)
{
    auto site = *mDesign.floorplan().sitesRange().begin();
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(site).toPoint().y();

    auto totalArea = boost::geometry::area(area);

    std::vector<double> areas;
    areas.resize(4, 0);

    for (auto cell : cells)
    {
        if (!mDesign.placement().isFixed(cell))
        {
            auto cellBox = mDesign.placementMapping().geometry(cell)[0];
            auto cellArea = boost::geometry::area(cellBox);

            auto cellHeight = (cellBox.max_corner().y() - cellBox.min_corner().y()) / rowHeight;
            areas[cellHeight - 1] += cellArea;
        }
    }

    for (auto area : areas)
    {
        mFileStream << area / totalArea << ",";
    }

    writeCsvOfHistogram(area, cells, result, boolResult, runtime);
}

void WriteCsv::writeCsvWithEverything(ophidian::geometry::Box area, const std::vector<ophidian::circuit::Cell> &cells, ophidian::util::micrometer_t totalDisplacement, ophidian::util::micrometer_t avgDisplacement, ophidian::util::micrometer_t maxDisplacement, double hpwlReduction, bool legalizationResult)
{
    writeCsvForLegalizationArea(area, cells, totalDisplacement, legalizationResult);
    writeCsvOfHistogramWithAreaOfMultirowCells(area, cells, totalDisplacement, legalizationResult, 0);

    mFileStream << units::unit_cast<double>(totalDisplacement) << ",";
    mFileStream << units::unit_cast<double>(avgDisplacement) << ",";
    mFileStream << units::unit_cast<double>(maxDisplacement) << ",";
    mFileStream << hpwlReduction << ",";
    mFileStream << legalizationResult << std::endl;
}

void WriteCsv::writeCsvForSingleSubrow(geometry::Box &area, const std::vector<circuit::Cell> &cells, util::micrometer_t displacement)
{
    auto subrowWidth = area.max_corner().x() - area.min_corner().x();
    auto totalCellWidth = 0.0;
    auto overlapWidth = 0.0;

    for (auto cell : cells)
    {
        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
        totalCellWidth += cellWidth;

        for (auto otherCell : cells) {
            if (cell != otherCell) {
                auto otherCellGeometry = mDesign.placementMapping().geometry(otherCell)[0];

                if (boost::geometry::intersects(cellGeometry, otherCellGeometry))
                {
                    geometry::Box intersectionBox;
                    boost::geometry::intersection(cellGeometry, otherCellGeometry, intersectionBox);
                    auto intersectionWidth = intersectionBox.max_corner().x() - intersectionBox.min_corner().x();
                    overlapWidth += intersectionWidth;
                }
            }
        }

    }

    auto occupiedArea = totalCellWidth / subrowWidth;
    auto overlapArea = overlapWidth / subrowWidth;

    mFileStream << subrowWidth << ",";
    mFileStream << totalCellWidth << ",";
    mFileStream << occupiedArea << ",";
    mFileStream << overlapWidth << ",";
    mFileStream << overlapArea << ",";
    mFileStream << cells.size() << ",";
    mFileStream << units::unit_cast<double>(displacement) << std::endl;
}

} // namespace legalization
} // namespace ophidian
