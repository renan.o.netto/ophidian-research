#include "CandidateLocations.h"

namespace ophidian {
namespace legalization {
CandidateLocations::CandidateLocations(design::Design &design)
    : mDesign(design) {

}

void CandidateLocations::findCandidateLocations(circuit::Cell cell, util::Location targetLocation, unsigned numberOfCandidates, geometry::Box area, Subrows &subrows, std::vector<util::Location> &candidateLocations)
{
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
    auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
    auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();
    auto cellAlignment = mDesign.placementMapping().alignment(cell);
    std::vector<Subrow> closestSubrows;

    targetLocation.x(util::micrometer_t(std::round(targetLocation.toPoint().x() / siteWidth) * siteWidth));
    targetLocation.y(util::micrometer_t(std::round(targetLocation.toPoint().y() / rowHeight) * rowHeight));

    subrows.findClosestSubrows(numberOfCandidates, targetLocation, closestSubrows);

    for (auto subrow : closestSubrows) {
        auto subrowOrigin = subrows.origin(subrow);
        auto subrowUpperCorner = subrows.upperCorner(subrow);

        auto subrowCapacity = subrows.capacity(subrow);

        placement::RowAlignment locationAlignment = (((int)(subrowOrigin.toPoint().y() / rowHeight) % 2) == 0) ? placement::RowAlignment::EVEN : placement::RowAlignment::ODD;
        if (cellAlignment != placement::RowAlignment::NA && cellAlignment != locationAlignment) {
            continue;
        }

        util::Location location(targetLocation.x(), subrowOrigin.y());
        if (targetLocation.x() < subrowOrigin.x()) {
            location.x(subrowOrigin.x());
        } else if (targetLocation.x() + util::micrometer_t(cellWidth) > subrowUpperCorner.x()) {
            location.x(subrowUpperCorner.x() - util::micrometer_t(cellWidth));
        }

        if (location.toPoint().x() + cellWidth > area.max_corner().x() ||
                location.toPoint().y() + cellHeight > area.max_corner().y() ||
                location.toPoint().x() < area.min_corner().x() ||
                location.toPoint().y() < area.min_corner().y()) {
            continue;
        }

        candidateLocations.push_back(location);
    }
}

void CandidateLocations::findCandidateLocationsGrid(circuit::Cell cell, util::Location targetLocation, unsigned numberOfRows, unsigned numberOfSteps, geometry::Box area, Subrows &subrows, std::vector<util::Location> &candidateLocations)
{
    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
    auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
    auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();
    auto cellAlignment = mDesign.placementMapping().alignment(cell);
    std::vector<Subrow> closestSubrows;

    targetLocation.x(util::micrometer_t(std::round(targetLocation.toPoint().x() / siteWidth) * siteWidth));
    targetLocation.y(util::micrometer_t(std::round(targetLocation.toPoint().y() / rowHeight) * rowHeight));

    if(cellAlignment == placement::RowAlignment::EVEN)
        numberOfRows *= 2;

    subrows.findClosestSubrows(numberOfRows, targetLocation, closestSubrows);

    for (auto subrow : closestSubrows) {
        auto subrowOrigin = subrows.origin(subrow);
        auto subrowUpperCorner = subrows.upperCorner(subrow);

        placement::RowAlignment locationAlignment = (((int)(subrowOrigin.toPoint().y() / rowHeight) % 2) == 0) ? placement::RowAlignment::EVEN : placement::RowAlignment::ODD;
        if (cellAlignment != placement::RowAlignment::NA && cellAlignment != locationAlignment) {
            continue;
        }

        util::Location location(targetLocation.x(), subrowOrigin.y());
        if (targetLocation.x() < subrowOrigin.x()) {
            location.x(subrowOrigin.x());
        } else if (targetLocation.x() + util::micrometer_t(cellWidth) > subrowUpperCorner.x()) {
            location.x(subrowUpperCorner.x() - util::micrometer_t(cellWidth));
        }
        //check if location is outside of legalization area.
        if (location.toPoint().x() + cellWidth > area.max_corner().x() ||
                location.toPoint().y() + cellHeight > area.max_corner().y() ||
                location.toPoint().x() < area.min_corner().x() ||
                location.toPoint().y() < area.min_corner().y()) {
            continue;
        }

        candidateLocations.push_back(location);

        for(double right=1; location.toPoint().x() + cellWidth + right * siteWidth <= subrowUpperCorner.toPoint().x() && right <= numberOfSteps; right++)
            candidateLocations.push_back(util::Location(location.toPoint().x() + right * siteWidth, location.toPoint().y()));
        for(double left=1; location.toPoint().x() - left * siteWidth >= subrowOrigin.toPoint().x() && left <= numberOfSteps; left++)
            candidateLocations.push_back(util::Location(location.toPoint().x() - left * siteWidth, location.toPoint().y()));
    }
}

}
}
