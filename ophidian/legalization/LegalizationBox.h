#ifndef LEGALIZATIONBOX_H
#define LEGALIZATIONBOX_H

#include <ophidian/design/Design.h>

namespace ophidian {
namespace legalization {
class LegalizationBox
{
    using RtreeNode = std::pair<geometry::Box, circuit::Cell>;
    using Rtree = boost::geometry::index::rtree<RtreeNode, boost::geometry::index::rstar<16> >;

public:
    LegalizationBox(design::Design & design);

    geometry::Box createLegalizationBox(circuit::Cell cell, util::Location candidateLocation, geometry::Box legalizationArea, double boxSize = 50000);

    void separateBoxCells(geometry::Box box, circuit::Cell targetCell, const std::vector<circuit::Cell> & cells, Rtree & cellsRtree, std::vector<circuit::Cell> & cellsInsideBox, std::vector<circuit::Cell> & cellsOnTheEdge);

private:
    design::Design & mDesign;
};
}
}

#endif // LEGALIZATIONBOX_H
