#ifndef ILPLEGALIZATIONWITHCONSTRAINTGRAPH_H
#define ILPLEGALIZATIONWITHCONSTRAINTGRAPH_H

#include <sys/time.h>

#include <gurobi_c++.h>

#include <ophidian/legalization/ConstraintGraph.h>

#include <ophidian/legalization/TransitiveClosureGraph.h>


namespace ophidian {
namespace legalization {
class ILPLegalizationWithConstraintGraph
{
public:
    ILPLegalizationWithConstraintGraph(design::Design & design);

    void setInitialLocations(design::Design & design);

    util::micrometer_t legalize(const std::vector<circuit::Cell> & cells, const geometry::Box &legalizationArea, bool trial = false);
    util::micrometer_t legalize(ConstraintGraph<LeftComparator> & horizontalGraph, ConstraintGraph<BelowComparator> & verticalGraph, const std::vector<circuit::Cell> & cells, const geometry::Box &legalizationArea, bool trial = false);

private:
    design::Design & mDesign;

    entity_system::Property<circuit::Cell, util::Location> mInitialLocations;

public:
    double mTimeBuildingGraph = 0;
    double mTimeBuildingModel = 0;
    double mTimeSolvingModel = 0;
};
}
}

#endif // ILPLEGALIZATIONWITHCONSTRAINTGRAPH_H
