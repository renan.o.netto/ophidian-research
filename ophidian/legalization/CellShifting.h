#ifndef CELLSHIFTING_H
#define CELLSHIFTING_H

#include <sys/time.h>

#include <gurobi_c++.h>

#include <ophidian/legalization/Subrows.h>
#include <ophidian/legalization/FenceRegionIsolation.h>
#include <ophidian/legalization/RectilinearFences.h>

#include <ophidian/design/Design.h>

namespace ophidian {
namespace legalization {
class CellSlice : public entity_system::EntityBase
{
public:
    using entity_system::EntityBase::EntityBase;
};


class CellShifting
{
public:
    using CellRtreeNode = std::pair<geometry::Box, circuit::Cell>;
    using CellsRtree = boost::geometry::index::rtree<CellRtreeNode, boost::geometry::index::rstar<16> >;

    class CellSlicePairComparator
    {
    public:
        bool operator()(const std::pair<CellSlice, util::micrometer_t> & cellPair1, const std::pair<CellSlice, util::micrometer_t> & cellPair2) {
            return cellPair1.second < cellPair2.second;
        }
    };

    CellShifting(design::Design & design);

    void setInitialLocations(design::Design & design);

    void shiftCellsInsideRows();

    util::micrometer_t shiftCellsInsideRows(util::MultiBox & area, std::vector<circuit::Cell> & cells, util::micrometer_t maxDisplacement, bool trial = false, bool improve = true);

    util::micrometer_t shiftCellsInsideRows(util::MultiBox area, std::vector<circuit::Cell> & cells, util::micrometer_t maxDisplacement, CellsRtree & cellsRtree, bool trial = false, bool improve = true);

    util::micrometer_t shiftCellsInsideRowsWithManhattan(util::MultiBox area, std::vector<circuit::Cell> & cells, util::micrometer_t maxDisplacement, bool trial = false, bool improve = true);

    void initialize(const std::vector<circuit::Cell> & cells, const ophidian::util::MultiBox & legalizationArea);

    util::micrometer_t legalizeCell(circuit::Cell cell, util::Location targetLocation, bool trial);

private:
    void sliceCells(std::vector<circuit::Cell> & cells);

    design::Design & mDesign;

    Subrows mSubrows;

    entity_system::Property<circuit::Cell, util::Location> mCellsInitialLocations;

    entity_system::EntitySystem<CellSlice> mCellSlices;
    entity_system::Property<circuit::Cell, std::vector<CellSlice>> mCircuitCellsSlices;
    entity_system::Property<CellSlice, circuit::Cell> mSlice2Cell;
    entity_system::Property<CellSlice, placement::RowAlignment> mSliceAlignment;
    entity_system::Property<CellSlice, std::string> mSliceNames;

    std::vector<circuit::Cell> mCells;
    util::MultiBox mLegalizationArea;

public:
    double mRuntime;
};
}
}


#endif // CELLSHIFTING_H
