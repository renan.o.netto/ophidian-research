#ifndef WRITECSV_H
#define WRITECSV_H

#include <fstream>

#include <ophidian/design/Design.h>

#include <ophidian/legalization/Subrows.h>
#include <ophidian/legalization/BinDecomposition.h>

namespace ophidian {
namespace legalization {
class WriteCsv
{
public:
    using RtreeNode = std::pair<geometry::Box, circuit::Cell>;
    using Rtree = boost::geometry::index::rtree<RtreeNode, boost::geometry::index::rstar<16> >;

    WriteCsv(design::Design & design, std::string fileName);

    void writeCsvForLegalizationArea(geometry::Box & area, const std::vector<circuit::Cell> & cells, circuit::Cell targetCell, util::micrometer_t result);

    void writeCsvForLegalizationArea(geometry::Box & area, const std::vector<circuit::Cell> & cells, util::micrometer_t result, bool boolResult);

    void writeCsvOfPixelsForLegalizationArea(geometry::Box & area, const std::vector<circuit::Cell> & cells, util::micrometer_t result);

    void writeCsvOfHistogram(geometry::Box & area, const std::vector<circuit::Cell> & cells, util::micrometer_t result, bool boolResult, double runtime);

    void writeCsvOfHistogramWithAreaOfMultirowCells(geometry::Box area, const std::vector<circuit::Cell> & cells, util::micrometer_t result, bool boolResult, double runtime);

    void writeCsvWithEverything(geometry::Box area, const std::vector<circuit::Cell> & cells, util::micrometer_t totalDisplacement, util::micrometer_t avgDisplacement, util::micrometer_t maxDisplacement, double hpwlReduction, bool legalizationResult);

    void writeCsvForSingleSubrow(geometry::Box & area, const std::vector<circuit::Cell> & cells, util::micrometer_t displacement);

private:
    design::Design & mDesign;

    std::ofstream mFileStream;
};
}
}

#endif // WRITECSV_H
