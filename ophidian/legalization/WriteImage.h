#ifndef WRITEIMAGE_H
#define WRITEIMAGE_H

#include <random>
#include <SFML/Graphics.hpp>
#include <ophidian/design/Design.h>

namespace ophidian {
namespace legalization {
class WriteImage
{
public:
    WriteImage(design::Design & design);

    //! filename = something.png
    void writeImageOfLegalizationArea(const geometry::Box & area,
                                      const std::vector<circuit::Cell> & cells,
                                      util::micrometer_t result,
                                      const std::string& filename);

private:
    design::Design & mDesign;
};
}
}



#endif // WRITEIMAGE_H
