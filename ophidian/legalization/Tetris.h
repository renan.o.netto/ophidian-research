#ifndef TETRIS_H
#define TETRIS_H

#include <ophidian/design/Design.h>
#include <ophidian/legalization/CandidateLocations.h>
#include <ophidian/legalization/Subrows.h>


namespace ophidian {
namespace legalization {

class Tetris
{
public:
    Tetris(design::Design & design);

    bool legalize(std::vector<circuit::Cell> cells, geometry::Box legalizationArea);

private:
    design::Design & mDesign_;
    entity_system::Property<circuit::Cell, util::Location> mInitialLocations_;
};
}
}

#endif // TETRIS_H
