#ifndef LEGALIZATIONIMPROVEMENTWITHCELLSHIFTING_H
#define LEGALIZATIONIMPROVEMENTWITHCELLSHIFTING_H

#include <sys/time.h>
#include <unordered_set>

#include <ophidian/design/Design.h>

#include <ophidian/legalization/CellShifting.h>

#include <ophidian/legalization/RectilinearFences.h>
#include <ophidian/legalization/FenceRegionIsolation.h>

#include <ophidian/legalization/LegalizationCheck.h>

#include <ophidian/legalization/WriteCsv.h>
#include <ophidian/legalization/WriteImage.h>

#include <ophidian/legalization/LegalizationPrediction.h>

#include <ophidian/legalization/LegalizationBox.h>
#include <ophidian/legalization/CandidateLocations.h>
#include <ophidian/legalization/CandidateCells.h>

namespace ophidian {
namespace legalization {
class LegalizationImprovementWithCellShifting
{
public:
    using RtreeNode = std::pair<geometry::Box, circuit::Cell>;
    using Rtree = boost::geometry::index::rtree<RtreeNode, boost::geometry::index::rstar<16> >;

    struct pair_hash {
        inline std::size_t operator()(const std::pair<int,int> & v) const {
            return v.first*31+v.second;
        }
    };

    class CellPairComparator
    {
public:
        bool operator()(const std::pair<circuit::Cell, util::micrometer_t> & cellPair1, const std::pair<circuit::Cell, util::micrometer_t> & cellPair2) {
            return cellPair1.second > cellPair2.second;
        }
    };

    LegalizationImprovementWithCellShifting(design::Design & design, std::string csvName, std::string circuitName = "");

    void setInitial(design::Design & design);

    void run(bool improve = true);

    void fixLegalization(std::vector<circuit::Cell> & cells, geometry::Box area);

    void improveLegalization(std::vector<circuit::Cell> & cells, geometry::Box area);

    void improveCell(std::vector<circuit::Cell> & cells, circuit::Cell cell, geometry::Box area, unsigned maxCandidates, bool improve = true);
protected:
    design::Design & mDesign;

    entity_system::Property<circuit::Cell, util::Location> mInitialLocations;
    entity_system::Property<circuit::Cell, bool> mInitialFixed;

    Subrows mSubrows;
    Subrows mFreeSpacesSubrows;

    CellShifting mLegalizer;
//    CellLegalizer mLegalizer;

    util::micrometer_t mMaxDisplacement;

    Rtree mCellsRtree;

    WriteCsv mWriteCsv;
//    WriteImage mWriteImage;

    LegalizationPrediction mLegalizationPrediction;

    LegalizationBox mLegalizationBox;
    CandidateLocations mCandidateLocations;
    CandidateCells mPickCandidateCells;

    unsigned mCountUnlegalized;
    unsigned mCount;
    unsigned mCountGlobal = 0;
    unsigned mAvoidedTrials;

    double mRuntime;

    std::string mCircuitName;
};
}
}

#endif // LEGALIZATIONIMPROVEMENTWITHCELLSHIFTING_H
