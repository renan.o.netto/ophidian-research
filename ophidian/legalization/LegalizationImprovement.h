#ifndef LEGALIZATIONIMPROVEMENT_H
#define LEGALIZATIONIMPROVEMENT_H

#include <sys/time.h>
#include <unordered_set>

#include <ophidian/design/Design.h>

#include <ophidian/legalization/CellLegalizer.h>

#include <ophidian/legalization/ILPLegalizationWithConstraintGraph.h>
#include <ophidian/legalization/CellShifting.h>
#include <ophidian/legalization/CellLegalizer.h>

#include <ophidian/legalization/RectilinearFences.h>
#include <ophidian/legalization/FenceRegionIsolation.h>

#include <ophidian/legalization/LegalizationCheck.h>

namespace ophidian {
namespace legalization {
class LegalizationImprovement
{
public:
    struct pair_hash {
        inline std::size_t operator()(const std::pair<int,int> & v) const {
            return v.first*31+v.second;
        }
    };

    class CellPairComparator
    {
public:
        bool operator()(const std::pair<circuit::Cell, util::micrometer_t> & cellPair1, const std::pair<circuit::Cell, util::micrometer_t> & cellPair2) {
            return cellPair1.second > cellPair2.second;
        }
    };

    LegalizationImprovement(design::Design & design);

    void setInitial(design::Design & design);

    void fixLegalization(std::vector<circuit::Cell> & cells, geometry::Box area);

    void improveLegalizationForWholeCircuit();

    void improveLegalization(std::vector<circuit::Cell> & cells, geometry::Box area);

    void improveCell(std::vector<circuit::Cell> & cells, std::pair<circuit::Cell, util::micrometer_t> cellPair, geometry::Box area, unsigned maxCandidates, bool improve = true);

    void findCandidateLocations(circuit::Cell cell, unsigned numberOfCandidates, geometry::Box area, std::vector<util::Location> & candidateLocations);

    geometry::Box createLegalizationBox(circuit::Cell cell, util::Location candidateLocation, geometry::Box legalizationArea);

    void separateBoxCells(geometry::Box box, circuit::Cell targetCell, const std::vector<circuit::Cell> & cells, std::vector<circuit::Cell> & cellsInsideBox, std::vector<circuit::Cell> & cellsOnTheEdge, std::vector<circuit::Cell> & cellsOutsideBox);
private:
    design::Design & mDesign;

    entity_system::Property<circuit::Cell, util::Location> mInitialLocations;
    entity_system::Property<circuit::Cell, bool> mInitialFixed;

    Subrows mSubrows;

    CellShifting mLegalizer;
    ILPLegalizationWithConstraintGraph mGraphLegalizer;
//    CellLegalizer mLegalizer;

    util::micrometer_t mMaxDisplacement;
};
}
}

#endif // LEGALIZATIONIMPROVEMENT_H
