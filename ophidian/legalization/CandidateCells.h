#ifndef CANDIDATECELLS_H
#define CANDIDATECELLS_H

#include <ophidian/design/Design.h>

#include <ophidian/legalization/LegalizationCheck.h>

namespace ophidian {
namespace legalization {
class CandidateCells
{
    class CellPairComparator
    {
public:
        bool operator()(const std::pair<circuit::Cell, util::micrometer_t> & cellPair1, const std::pair<circuit::Cell, util::micrometer_t> & cellPair2) {
            return cellPair1.second > cellPair2.second;
        }
    };

public:
    CandidateCells(design::Design & design);

    void setInitial(design::Design & design);

    void pickCandidateCellsWithGreatestDisplacement(unsigned numberOfCandidates, const std::vector<circuit::Cell> & cells, std::vector<circuit::Cell> & candidateCells);

    void pickCandidateCellsWithOverlaps(const std::vector<circuit::Cell> & cells, std::vector<circuit::Cell> & candidateCells);

private:
    design::Design & mDesign;

    entity_system::Property<circuit::Cell, util::Location> mInitialLocations;
};
}
}

#endif // CANDIDATECELLS_H
