#ifndef ILPLEGALIZATION_H
#define ILPLEGALIZATION_H

#include <ophidian/design/Design.h>
#include <ophidian/legalization/CandidateLocations.h>
#include <ophidian/geometry/Models.h>
#include "gurobi_c++.h"
#include <ophidian/legalization/Subrows.h>
#include <ophidian/legalization/LegalizationCheck.h>


namespace ophidian {
namespace legalization {

class ILPLegalization
{
    using box = geometry::Box;
    using tree_node = std::pair<box, std::string>;
    using rtree = boost::geometry::index::rtree<tree_node, boost::geometry::index::rstar<16>>;
public:
    ILPLegalization(design::Design & design);

    bool legalizePlacement(std::vector<circuit::Cell> cells, ophidian::geometry::Box legalizationArea, unsigned numberOfCandidates = 10);

    void setInitialLocations(design::Design & design);
private:
    void findCandidateLocations(std::vector<circuit::Cell> cells, ophidian::geometry::Box legalizationArea, unsigned numberOfCandidates);
    void addExistanceConstraints(std::vector<circuit::Cell> cells, GRBModel & model);
    void addOverlapConstraints(std::vector<circuit::Cell> cells, GRBModel & model);
    void addObjective(std::vector<circuit::Cell> cells, GRBModel & model);
    void placeSolution(std::vector<circuit::Cell> cells);
    void solve(GRBModel & model);

    GRBEnv mGRBenv_;
    design::Design & mDesign;
    entity_system::Property<ophidian::circuit::Cell, std::vector<GRBVar>> mCells2VecGRBVar_;
    entity_system::Property<circuit::Cell, std::vector<util::Location>> mCell2CandidateLocations_;
    entity_system::Property<circuit::Cell, util::Location> mInitialLocations_;
    rtree candidateLocations_;
    std::unordered_map<std::string, GRBVar> mVarName2GRBVar_;
};
}
}

#endif // ILPLEGALIZATION_H
