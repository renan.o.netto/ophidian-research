#ifndef KDTREELEGALIZATION_H
#define KDTREELEGALIZATION_H

#include <ophidian/design/Design.h>
#include <ophidian/legalization/Subrows.h>
#include <ophidian/geometry/Models.h>
#include <ophidian/legalization/MultirowAbacus.h>
#include <ophidian/util/KDTreePartitioning.h>

#include <ophidian/legalization/WriteCsv.h>
#include <ophidian/legalization/WriteImage.h>

#include <ophidian/legalization/LegalizationPrediction.h>

#include <ophidian/legalization/PythonPrediction.h>

namespace ophidian {
namespace legalization {

class PartitionedLegalization
{
private:
    using Cell = ophidian::circuit::Cell;
    using Box = geometry::Box;
    using RTreeNode = std::pair<Box, Cell>;
    using RTree = boost::geometry::index::rtree<RTreeNode, boost::geometry::index::rstar<16> >;
    using Node = typename util::KDTreePartitioning<Cell>::Node;
public:
    PartitionedLegalization(design::Design & design, Box placeableArea, std::string modelFile);
    void legalize(unsigned int i, unsigned int maximumDisplacement);
    void legalizeWithPrediction(unsigned int i, unsigned int maximumDisplacement);

    void allignCellsToNearestSite();
    void removeMacroblocksOverlaps();
private:

    design::Design & mDesign;
    Box mPlaceableArea;
    entity_system::Property<Cell, util::Location> mCellsInitialLocations;
    entity_system::Property<Cell, bool> mInitialFixed;

    LegalizationPrediction mLegalizationPrediction;
};
}
}

#endif // KDTREELEGALIZATION_H
