#include "Tetris.h"

namespace ophidian {
namespace legalization {

Tetris::Tetris(design::Design & design) :
    mDesign_(design),
    mInitialLocations_(mDesign_.netlist().makeProperty<util::Location>(circuit::Cell()))
{
    for(auto cellIt = mDesign_.netlist().begin(circuit::Cell()); cellIt != mDesign_.netlist().end(circuit::Cell()); ++cellIt)
        mInitialLocations_[*cellIt] = mDesign_.placement().cellLocation(*cellIt);
}

bool Tetris::legalize(std::vector<circuit::Cell> cells, ophidian::geometry::Box legalizationArea){

    std::sort(cells.begin(), cells.end(), [&](circuit::Cell c1, circuit::Cell c2){return mInitialLocations_[c1].toPoint().x() < mInitialLocations_[c2].toPoint().x();});

    auto rowHeight = mDesign_.floorplan().siteUpperRightCorner(*mDesign_.floorplan().sitesRange().begin()).toPoint().y();
    auto siteWidth = mDesign_.floorplan().siteUpperRightCorner(*mDesign_.floorplan().sitesRange().begin()).toPoint().x();
    legalization::Subrows subrows(mDesign_);
    subrows.createSubrows(cells, util::MultiBox({legalizationArea}));
    auto firstFreeLocation = subrows.makeProperty<util::Location>(legalization::Subrow());

    //set first free location
    for(auto subrow : subrows.range(legalization::Subrow()))
        firstFreeLocation[subrow] = subrows.origin(subrow);

    for(auto cell : cells){
        if(mDesign_.placement().isFixed(cell))
            continue;

        auto cellGeometry = mDesign_.placementMapping().geometry(cell)[0];
        auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
        auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();
        auto cellAlignment = mDesign_.placementMapping().alignment(cell);
        util::Location targetLocation;
        targetLocation.x(util::micrometer_t(std::round(mInitialLocations_[cell].toPoint().x() / siteWidth) * siteWidth));
        targetLocation.y(util::micrometer_t(std::round(mInitialLocations_[cell].toPoint().y() / rowHeight) * rowHeight));

        std::vector<Subrow> closestSubrows;
        util::Location bestLocation;
        legalization::Subrow bestSubrow;
        double bestLocationDisplacement = std::numeric_limits<double>::max();
        unsigned numberOfRows = 10;

        //while not found any new subrow
        while(bestLocationDisplacement == std::numeric_limits<double>::max()){
            subrows.findClosestSubrows(numberOfRows, targetLocation, closestSubrows);
            for (auto subrow : closestSubrows) {
                auto subrowOrigin = subrows.origin(subrow);
                auto subrowUpperCorner = subrows.upperCorner(subrow);

                placement::RowAlignment locationAlignment = (((int)(subrowOrigin.toPoint().y() / rowHeight) % 2) == 0) ? placement::RowAlignment::EVEN : placement::RowAlignment::ODD;
                if (cellAlignment != placement::RowAlignment::NA && cellAlignment != locationAlignment) {
                    continue;
                }

                //set cell inside subrow
                util::Location location(targetLocation.x(), subrowOrigin.y());
                if (targetLocation.x() < firstFreeLocation[subrow].x()) {
                    location.x(firstFreeLocation[subrow].x());
                } else if (targetLocation.x() + util::micrometer_t(cellWidth) > subrowUpperCorner.x()) {
                    location.x(subrowUpperCorner.x() - util::micrometer_t(cellWidth));
                }
                //check if location is outside of legalization area.
                if (location.toPoint().x() + cellWidth > subrowUpperCorner.toPoint().x() ||
                        location.toPoint().y() + cellHeight > subrowUpperCorner.toPoint().y() ||
                        location.toPoint().x() < firstFreeLocation[subrow].toPoint().x() ||
                        location.toPoint().y() < firstFreeLocation[subrow].toPoint().y()) {
                    continue;
                }
                //check if location improves the displacement
                double displacement = std::abs(location.toPoint().x() - mInitialLocations_[cell].toPoint().x()) + std::abs(location.toPoint().y() - mInitialLocations_[cell].toPoint().y());
                if(displacement <= bestLocationDisplacement){
                    bestLocation = location;
                    bestLocationDisplacement = displacement;
                    bestSubrow = subrow;
                }

            }
            if(numberOfRows >= subrows.rowCount() && bestLocationDisplacement == std::numeric_limits<double>::max()) {
//                std::cout << "could not find location" << std::endl;
                return false;
            }
            numberOfRows *= 2;
            closestSubrows.clear();
        }
        //place cell
        mDesign_.placement().placeCell(cell, bestLocation);
        firstFreeLocation[bestSubrow] = util::Location(bestLocation.toPoint().x()+cellWidth, bestLocation.toPoint().y());
    }
    return true;
}

}
}
