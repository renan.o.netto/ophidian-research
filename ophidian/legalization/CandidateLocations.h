#ifndef CANDIDATELOCATIONS_H
#define CANDIDATELOCATIONS_H

#include <ophidian/design/Design.h>
#include <ophidian/legalization/Subrows.h>

namespace ophidian {
namespace legalization {
class CandidateLocations
{
public:
    CandidateLocations(design::Design & design);

    void findCandidateLocations(circuit::Cell cell, util::Location targetLocation, unsigned numberOfCandidates, geometry::Box area, Subrows & subrows, std::vector<util::Location> & candidateLocations);

    void findCandidateLocationsGrid(circuit::Cell cell, util::Location targetLocation, unsigned numberOfRows, unsigned numberOfSteps, geometry::Box area, Subrows & subrows, std::vector<util::Location> & candidateLocations);

private:
    design::Design & mDesign;
};
}
}

#endif // CANDIDATELOCATIONS_H
