#include "iccad2017SolutionQuality.h"

namespace ophidian
{
namespace legalization
{

ICCAD2017SolutionQuality::ICCAD2017SolutionQuality(ophidian::design::Design &design, ophidian::design::Design &originalDesign, std::string circuitName):
    mCircuitName(circuitName),
    mInitialLocations(design.netlist().makeProperty<util::Location>(ophidian::circuit::Cell())),
    mInitialFixed(design.netlist().makeProperty<bool>(ophidian::circuit::Cell())),
    mDesign(design),
    mOriginalDesign(originalDesign),
    mRowHeight(mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y())
{
    for (auto cellIt = originalDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != originalDesign.netlist().end(ophidian::circuit::Cell()); cellIt++)
    {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mInitialFixed[currentDesignCell] = originalDesign.placement().isFixed(cell);
        mInitialLocations[currentDesignCell] = originalDesign.placement().cellLocation(cell);
    }

    // mRowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
}

double ICCAD2017SolutionQuality::avgMovementScore() {
    std::unordered_map<double, std::pair<double, std::size_t>> cellHeight2Counter;
    for (auto cellIt = mDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != mDesign.netlist().end(ophidian::circuit::Cell()); cellIt++)
        if (!mDesign.placement().isFixed(*cellIt))
        {
            auto cellHeight = mDesign.placementMapping().geometry(*cellIt)[0].max_corner().y() - mDesign.placementMapping().geometry(*cellIt)[0].min_corner().y();
            double displacement = cellDisplacement(mDesign.placement().cellLocation(*cellIt), mInitialLocations[*cellIt]);
            auto displacementHeightCounter = cellHeight2Counter.find(cellHeight);
            if (displacementHeightCounter == cellHeight2Counter.end())
                cellHeight2Counter[cellHeight] = std::pair<double, std::size_t>(displacement, 1);
            else
                cellHeight2Counter[cellHeight] = std::pair<double, size_t>(cellHeight2Counter[cellHeight].first + displacement, cellHeight2Counter[cellHeight].second + 1);
        }
    double averageDisplacement = 0;
    for (auto displacementCounter : cellHeight2Counter)
        averageDisplacement += (displacementCounter.second.first/displacementCounter.second.second);
    averageDisplacement = (averageDisplacement / mRowHeight) / cellHeight2Counter.size();
    return averageDisplacement;
}

double ICCAD2017SolutionQuality::maxMovementScore() {
    return 1 + (maximumCellMovement()/100.0) * fmm();
}

double ICCAD2017SolutionQuality::hpwlScore() {
    double originalHPWL = hpwl(mOriginalDesign);
    double currentHPWL = hpwl(mDesign);

    double score = std::max((currentHPWL - originalHPWL) / originalHPWL, 0.0) * (1 + std::max(BETA * fof(), 0.2));

    return score;
}

double ICCAD2017SolutionQuality::totalDisplacement() {
    double displacement = 0.0;
    for (auto cellIt = mDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != mDesign.netlist().end(ophidian::circuit::Cell()); cellIt++)
        displacement+= cellDisplacement(mDesign.placement().cellLocation(*cellIt), mInitialLocations[*cellIt]);
    return displacement;
}

double ICCAD2017SolutionQuality::avgDisplacement() {
    double displacement = 0.0;
    unsigned int amount = 0;
    for (auto cellIt = mDesign.netlist().begin(ophidian::circuit::Cell()); cellIt != mDesign.netlist().end(ophidian::circuit::Cell()); cellIt++) {
        if (mInitialFixed[*cellIt] == false) {
            displacement+= cellDisplacement(mDesign.placement().cellLocation(*cellIt), mInitialLocations[*cellIt]);
            amount++;
        }
    }
    return (displacement/mRowHeight)/amount;
}

int ICCAD2017SolutionQuality::maximumCellMovement() {
    double maximumCellMovement = 0;
    ophidian::circuit::Cell cellID;
    ophidian::circuit::Cell previousID;

    for (auto cell : mDesign.netlist().range(circuit::Cell())) {
        if (cellDisplacement(mDesign.placement().cellLocation(cell), mInitialLocations[cell]) >= maximumCellMovement) {
            maximumCellMovement = cellDisplacement(mDesign.placement().cellLocation(cell), mInitialLocations[cell]);
            previousID = cellID;
            cellID = cell;
        }
    }

    return maximumCellMovement / mRowHeight;
}

double ICCAD2017SolutionQuality::hpwl(ophidian::design::Design & design)
{
    double designHpwl = 0;
    for (auto it = design.netlist().begin(circuit::Net()); it < design.netlist().end(circuit::Net()); it++) {
        designHpwl += hpwl(design, *it);
    }
    return designHpwl;
}

double ICCAD2017SolutionQuality::avghpwl(ophidian::design::Design & design)
{
    double designHpwl = 0;
    for (auto it = design.netlist().begin(circuit::Net()); it < design.netlist().end(circuit::Net()); it++) {
        designHpwl += hpwl(design, *it);
    }
    return designHpwl / design.netlist().size(circuit::Net());
}

double ICCAD2017SolutionQuality::fmm() {
    double displacement = 0;

    for (auto cell : mDesign.netlist().range(circuit::Cell())) {
        if (cellDisplacement(mDesign.placement().cellLocation(cell), mInitialLocations[cell])/mRowHeight > mConstraints.at(mCircuitName).maximumMovement)
            displacement += cellDisplacement(mDesign.placement().cellLocation(cell), mInitialLocations[cell]);
    }

    displacement /= mRowHeight * mConstraints.at(mCircuitName).maximumMovement;
    return std::max(displacement, 1.0);
}

double ICCAD2017SolutionQuality::fof() {
    // TODO

    using Point = ophidian::geometry::Point;
    using Box = ophidian::geometry::Box;
    using RNode = std::pair<Box, ophidian::circuit::Cell>;
    using RTree = boost::geometry::index::rtree<RNode, boost::geometry::index::rstar<16>>;

    RTree tree;

    for (auto & cell : mDesign.netlist().range(circuit::Cell())) {
        auto std_cell = mDesign.libraryMapping().cellStdCell(cell);

        auto cell_geometry = mDesign.library().geometry(std_cell)[0];

        auto cell_location_point = mDesign.placement().cellLocation(cell).toPoint();

        Box b(cell_location_point, Point(cell_location_point.x() + cell_geometry.max_corner().x(), cell_location_point.y() + cell_geometry.max_corner().y()));
        tree.insert(std::make_pair(b, cell));
    }

    double totalOverflow = 0.0;

    auto singleBinArea = 64 * mRowHeight;
    double dMax = mConstraints.at(mCircuitName).maximumUtilization;
    double totalAreaMovableCells = 0.0;

    for (double y = 0; y < mDesign.floorplan().chipUpperRightCorner().toPoint().y() - (8 * mRowHeight); y += (8 * mRowHeight)) {
        for (double x = 0; x < mDesign.floorplan().chipUpperRightCorner().toPoint().x() - (8 * mRowHeight); x += (8 * mRowHeight)) {
            Box binBox({x, y}, {x + (8 * mRowHeight), y + (8 * mRowHeight)});
            std::vector<RNode> binCells;
            tree.query(boost::geometry::index::intersects(binBox), std::back_inserter(binCells));

            double movableArea = 0;
            double freeSpace = singleBinArea;

            for (auto & cellNode : binCells) {
                auto cell = cellNode.second;
                auto std_cell = mDesign.libraryMapping().cellStdCell(cell);
                auto cell_geometry = mDesign.library().geometry(std_cell);

                double covered_area = 0;

                for(auto cell_box : cell_geometry) {
                    geometry::Box intersection;
                    boost::geometry::intersection(cell_box, binBox, intersection);
                    covered_area += boost::geometry::area(intersection);
                }

                if (mDesign.placement().isFixed(cell)) {
                    freeSpace -= covered_area;
                } else {
                    movableArea += covered_area;
                }
            }

            double overflow = movableArea - freeSpace * dMax;
            totalOverflow += overflow;
        }
    }

    return totalOverflow;
//    return totalOverflow * singleBinArea * dMax * totalAreaMovableCells;
}

double ICCAD2017SolutionQuality::hpwl(ophidian::design::Design & design, const ophidian::circuit::Net & net) {
    double minX = std::numeric_limits<double>::max(),
           minY = std::numeric_limits<double>::max(),
           maxX = std::numeric_limits<double>::lowest(),
           maxY = std::numeric_limits<double>::lowest();

    for (auto pin : design.netlist().pins(net)) {
        util::Location pinLocation;

        auto input = design.netlist().input(pin);
        auto output = design.netlist().output(pin);

        if (input != circuit::Input()) {
            pinLocation = design.placement().inputPadLocation(input);
        } else if (output != circuit::Output()) {
            pinLocation = design.placement().outputPadLocation(output);
        } else {
            pinLocation = design.placementMapping().location(pin);
        }

        minX = std::min(pinLocation.toPoint().x(), minX);
        minY = std::min(pinLocation.toPoint().y(), minY);
        maxX = std::max(pinLocation.toPoint().x(), maxX);
        maxY = std::max(pinLocation.toPoint().y(), maxY);
    }
    return std::abs(maxX - minX) + std::abs(maxY - minY);
}

} // namespace legalization
} // namespace ophidian
