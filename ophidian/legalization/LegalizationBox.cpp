#include "LegalizationBox.h"

namespace ophidian {
namespace legalization {
LegalizationBox::LegalizationBox(design::Design &design)
    : mDesign(design) {

}

geometry::Box LegalizationBox::createLegalizationBox(circuit::Cell cell, util::Location candidateLocation, geometry::Box legalizationArea, double boxSize)
{
    auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
    auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
    auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();

    util::micrometer_t boxHalfWidth = util::micrometer_t(std::max(boxSize, cellWidth/2));
    util::micrometer_t boxHalfHeight = util::micrometer_t(std::max(boxSize, cellHeight/2));
    util::Location boxOrigin(candidateLocation.x() - boxHalfWidth, candidateLocation.y() - boxHalfHeight);
    boxOrigin.x(std::max(boxOrigin.x(), util::micrometer_t(legalizationArea.min_corner().x())));
    boxOrigin.y(std::max(boxOrigin.y(), util::micrometer_t(legalizationArea.min_corner().y())));
    util::Location boxUpperCorner(candidateLocation.x() + boxHalfWidth, candidateLocation.y() + boxHalfHeight);
    boxUpperCorner.x(std::min(boxUpperCorner.x(), util::micrometer_t(legalizationArea.max_corner().x())));
    boxUpperCorner.y(std::min(boxUpperCorner.y(), util::micrometer_t(legalizationArea.max_corner().y())));
    geometry::Box legalizationBox(boxOrigin.toPoint(), boxUpperCorner.toPoint());

    return legalizationBox;
}

void LegalizationBox::separateBoxCells(geometry::Box box, circuit::Cell targetCell, const std::vector<circuit::Cell> &cells, Rtree &cellsRtree, std::vector<circuit::Cell> &cellsInsideBox, std::vector<circuit::Cell> &cellsOnTheEdge)
{
    std::vector<RtreeNode> nodesInsideBox;
    nodesInsideBox.reserve(cells.size());
    std::vector<RtreeNode> nodesOnTheEdge;
    nodesOnTheEdge.reserve(cells.size());
    cellsRtree.query(boost::geometry::index::within(box), std::back_inserter(nodesInsideBox));
    cellsRtree.query(boost::geometry::index::overlaps(box), std::back_inserter(nodesOnTheEdge));

    cellsInsideBox.reserve(nodesInsideBox.size());
    cellsOnTheEdge.reserve(nodesOnTheEdge.size());
    cellsInsideBox.push_back(targetCell);
    for (auto node : nodesInsideBox) {
        if (node.second != targetCell) {
            cellsInsideBox.push_back(node.second);
        }
    }
    for (auto node : nodesOnTheEdge) {
        if (node.second != targetCell) {
            cellsOnTheEdge.push_back(node.second);
        }
    }
}
}
}
