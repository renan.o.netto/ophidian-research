#include "PythonPrediction.h"

namespace ophidian {
namespace legalization {

PythonPrediction::PythonPrediction(design::Design &design)
    : mWriteImage(design), mWriteCsv(design, "test.csv")
{

}

bool PythonPrediction::run(const geometry::Box &area, const std::vector<circuit::Cell> & cells){
//    mWriteImage.writeImageOfLegalizationArea(area, cells, ophidian::util::micrometer_t(0), "circuit.png");
    mWriteCsv.writeCsvWithEverything(area, cells, util::micrometer_t(0), util::micrometer_t(0), util::micrometer_t(0), 0, 0);

    char * file = new char[strlen(mDotPy.c_str())];
    strcpy(file, mDotPy.c_str());
//    PyObject * pyobject = PyFile_FromString(file, "r");
    FILE * fp = _Py_fopen(mDotPy.c_str(), "r");

    Py_SetProgramName(L"python");
    Py_Initialize();
    PyRun_SimpleFile(fp, mDotPy.c_str());
//    PyRun_SimpleFileEx(PyFile_AsFile(pyobject), mDotPy.c_str(), 1);
    Py_Finalize();

    delete []file;

    std::ifstream fileStream;
    fileStream.open("result", std::ios::in);

    if (!fileStream.good()) {
        return false;
    }

    bool result;
    fileStream >> result;
    return result;
}

}
}
