#include "TransitiveClosureGraph.h"

namespace ophidian {
namespace legalization {
TransitiveClosureGraph::TransitiveClosureGraph(design::Design & design)
    : mDesign(design),
      mCell2Node(design.netlist().makeProperty<lemon::ListDigraph::Node>(circuit::Cell())),
      mNode2Cell(mGraph), mArcCosts(mGraph), mArcTypes(mGraph),
      mMinimumLocations(mGraph), mMaximumLocations(mGraph)
{
    mSource = lemon::INVALID;
    mSink = lemon::INVALID;
}

void TransitiveClosureGraph::buildConstraintGraph(const std::vector<circuit::Cell> &cells, geometry::Box area)
{
    for (auto cell : cells) {
        auto node = mGraph.addNode();
        mCell2Node[cell] = node;
        mNode2Cell[node] = cell;
    }

    for (auto cell1It  = cells.begin(); cell1It != cells.end(); cell1It++) {
        auto cell1 = *cell1It;
        auto node1 = mCell2Node[cell1];
        for (auto cell2It = cells.begin(); cell2It != cell1It; cell2It++) {
            auto cell2 = *cell2It;
            auto node2 = mCell2Node[cell2];

            auto cell1Name = mDesign.netlist().name(cell1);
            auto cell2Name = mDesign.netlist().name(cell2);

            auto cell1Box = mDesign.placementMapping().geometry(cell1)[0];
            auto cell1Width = cell1Box.max_corner().x() - cell1Box.min_corner().x();
            auto cell1Height = cell1Box.max_corner().y() - cell1Box.min_corner().y();

            auto cell2Box = mDesign.placementMapping().geometry(cell2)[0];
            auto cell2Width = cell2Box.max_corner().x() - cell2Box.min_corner().x();
            auto cell2Height = cell2Box.max_corner().y() - cell2Box.min_corner().y();

            std::pair<double, double> cell1XProjection(cell1Box.min_corner().x(), cell1Box.max_corner().x());
            std::pair<double, double> cell2XProjection(cell2Box.min_corner().x(), cell2Box.max_corner().x());

            std::pair<double, double> cell1YProjection(cell1Box.min_corner().y(), cell1Box.max_corner().y());
            std::pair<double, double> cell2YProjection(cell2Box.min_corner().y(), cell2Box.max_corner().y());

            if (projectionOverlap(cell1YProjection, cell2YProjection)) {
                if (cell1Box.min_corner().x() < cell2Box.min_corner().x()) {
                    auto arc = mGraph.addArc(node1, node2);
                    mArcCosts[arc] = cell1Width;
                    mArcTypes[arc] = EdgeType::HORIZONTAL;
                } else {
                    auto arc = mGraph.addArc(node2, node1);
                    mArcCosts[arc] = cell2Width;
                    mArcTypes[arc] = EdgeType::HORIZONTAL;
                }
            } else if (projectionOverlap(cell1XProjection, cell2XProjection)) {
                if (cell1Box.min_corner().y() < cell2Box.min_corner().y()) {
                    auto arc = mGraph.addArc(node1, node2);
                    mArcCosts[arc] = cell1Height;
                    mArcTypes[arc] = EdgeType::VERTICAL;
                } else {
                    auto arc = mGraph.addArc(node2, node1);
                    mArcCosts[arc] = cell2Height;
                    mArcTypes[arc] = EdgeType::VERTICAL;
                }
            } else {
                if (cell1Box.min_corner().y() < cell2Box.min_corner().y()) {
                    auto arc = mGraph.addArc(node1, node2);
                    mArcCosts[arc] = cell1Height;
                    mArcTypes[arc] = EdgeType::VERTICAL;
                } else {
                    auto arc = mGraph.addArc(node2, node1);
                    mArcCosts[arc] = cell2Height;
                    mArcTypes[arc] = EdgeType::VERTICAL;
                }
            }
        }
    }

    calculateSlacks(area);
}

bool TransitiveClosureGraph::hasHorizontalEdge(circuit::Cell cell1, circuit::Cell cell2)
{
    auto node1 = mCell2Node[cell1];
    auto node2 = mCell2Node[cell2];
    auto arc = lemon::findArc(mGraph, node1, node2);
    return  arc != lemon::INVALID && mArcTypes[arc] == EdgeType::HORIZONTAL;
}

bool TransitiveClosureGraph::hasVerticalEdge(circuit::Cell cell1, circuit::Cell cell2)
{
    auto node1 = mCell2Node[cell1];
    auto node2 = mCell2Node[cell2];
    auto arc = lemon::findArc(mGraph, node1, node2);
    return  arc != lemon::INVALID && mArcTypes[arc] == EdgeType::VERTICAL;
}

void TransitiveClosureGraph::removeTransitiveEdges()
{
    util::transitiveClosure(mGraph);
    util::transitiveReduction(mGraph);
}

void TransitiveClosureGraph::calculateSlacks(geometry::Box area)
{
    if (mSource != lemon::INVALID) {
        mGraph.erase(mSource);
    }
    if (mSink != lemon::INVALID) {
        mGraph.erase(mSink);
    }

    mSource = mGraph.addNode();
    mSink = mGraph.addNode();

    for (auto node = lemon::ListDigraph::NodeIt(mGraph); node != lemon::INVALID; ++node) {
        if (node == mSource || node == mSink) {
            continue;
        }
        if (countInputArcs(node, EdgeType::HORIZONTAL) == 0) {
            auto arc = mGraph.addArc(mSource, node);
            mArcCosts[arc] = 0;
            mArcTypes[arc] = EdgeType::HORIZONTAL;
        }
        if (countInputArcs(node, EdgeType::VERTICAL) == 0) {
            auto arc = mGraph.addArc(mSource, node);
            mArcCosts[arc] = 0;
            mArcTypes[arc] = EdgeType::VERTICAL;
        }

        if (countOutputArcs(node, EdgeType::HORIZONTAL) == 0) {
            auto cell = mNode2Cell[node];
            auto cellBox = mDesign.placementMapping().geometry(cell)[0];
            auto cellWidth = cellBox.max_corner().x() - cellBox.min_corner().x();

            auto arc = mGraph.addArc(node, mSink);
            mArcCosts[arc] = cellWidth;
            mArcTypes[arc] = EdgeType::HORIZONTAL;
        }
        if (countOutputArcs(node, EdgeType::VERTICAL) == 0) {
            auto cell = mNode2Cell[node];
            auto cellBox = mDesign.placementMapping().geometry(cell)[0];
            auto cellHeight = cellBox.max_corner().y() - cellBox.min_corner().y();

            auto arc = mGraph.addArc(node, mSink);
            mArcCosts[arc] = cellHeight;
            mArcTypes[arc] = EdgeType::VERTICAL;
        }
    }

    std::vector<lemon::ListDigraph::Node> sortedNodes;
    util::topologicalSort(mGraph, mSource, sortedNodes);

    mMinimumLocations[mSource] = util::Location(area.min_corner().x(), area.min_corner().y());
    mMaximumLocations[mSink] = util::Location(area.max_corner().x(), area.max_corner().y());

    for (auto node : sortedNodes) {
        auto cell = mNode2Cell[node];
        if (node != mSource && node != mSink && mDesign.placement().isFixed(cell)) {
            auto cellLocation = mDesign.placement().cellLocation(cell);
            mMinimumLocations[node] = cellLocation;
        } else {
            mMinimumLocations[node] = util::Location(area.min_corner().x(), area.min_corner().y());
            for (auto arc = lemon::ListDigraph::InArcIt(mGraph, node); arc != lemon::INVALID; ++arc) {
                auto source = mGraph.source(arc);
                if (mArcTypes[arc] == EdgeType::HORIZONTAL) {
                    mMinimumLocations[node].x(std::max(mMinimumLocations[node].x(), mMinimumLocations[source].x() + util::micrometer_t(mArcCosts[arc])));
                } else {
                    mMinimumLocations[node].y(std::max(mMinimumLocations[node].y(), mMinimumLocations[source].y() + util::micrometer_t(mArcCosts[arc])));
                }
            }
        }
    }

    for (auto nodeIt = sortedNodes.rbegin(); nodeIt != sortedNodes.rend(); nodeIt++) {
        auto node = *nodeIt;
        auto cell = mNode2Cell[node];
        if (node != mSource && node != mSink && mDesign.placement().isFixed(cell)) {
            auto cellLocation = mDesign.placement().cellLocation(cell);
            mMaximumLocations[node] = cellLocation;
        } else {
            mMaximumLocations[node] = util::Location(area.max_corner().x(), area.max_corner().y());
            for (auto arc = lemon::ListDigraph::OutArcIt(mGraph, node); arc != lemon::INVALID; ++arc) {
                auto target = mGraph.target(arc);
                if (mArcTypes[arc] == EdgeType::HORIZONTAL) {
                    mMaximumLocations[node].x(std::min(mMaximumLocations[node].x(), mMaximumLocations[target].x() - util::micrometer_t(mArcCosts[arc])));
                } else {
                    mMaximumLocations[node].y(std::min(mMaximumLocations[node].y(), mMaximumLocations[target].y() - util::micrometer_t(mArcCosts[arc])));
                }
            }
        }
    }
}

util::micrometer_t TransitiveClosureGraph::slack(circuit::Cell cell, EdgeType edgeType)
{
    auto node = mCell2Node[cell];
    if (edgeType == EdgeType::HORIZONTAL) {
        return mMaximumLocations[node].x() - mMinimumLocations[node].x();
    } else {
        return mMaximumLocations[node].y() - mMinimumLocations[node].y();
    }
}

util::micrometer_t TransitiveClosureGraph::worstSlack(EdgeType edgeType)
{
    util::micrometer_t worstSlack = util::micrometer_t(std::numeric_limits<double>::max());
    for (auto node = lemon::ListDigraph::NodeIt(mGraph); node != lemon::INVALID; ++node) {
        if (node != mSource && node != mSink) {
            auto cell = mNode2Cell[node];
            auto cellSlack = slack(cell, edgeType);
            worstSlack = std::min(worstSlack, cellSlack);
        }
    }
    return worstSlack;
}

void TransitiveClosureGraph::exportGraph(std::string horizontalFileName, std::string verticalFileName)
{
    std::ofstream horizontalGraphFile;
    horizontalGraphFile.open (horizontalFileName);

    std::ofstream verticalGraphFile;
    verticalGraphFile.open (verticalFileName);

    horizontalGraphFile << "digraph G {" << std::endl;
    verticalGraphFile << "digraph G {" << std::endl;

    for (auto arc = lemon::ListDigraph::ArcIt(mGraph); arc != lemon::INVALID; ++arc) {
        auto source = mGraph.source(arc);
        auto target = mGraph.target(arc);

        if (mArcTypes[arc] == EdgeType::HORIZONTAL) {
            horizontalGraphFile << "\"" << mGraph.id(source) << " " << mMinimumLocations[source].x() << ", " << mMaximumLocations[source].x() << "\""
                      << " -> " << "\"" << mGraph.id(target) << " " << mMinimumLocations[target].x() << ", " << mMaximumLocations[target].x() << "\""
                            << " [label=" << mArcCosts[arc] << "];" << std::endl;
        } else {
            verticalGraphFile << "\"" << mGraph.id(source) << " " << mMinimumLocations[source].y() << ", " << mMaximumLocations[source].y() << "\""
                      << " -> " << "\"" << mGraph.id(target) << " " << mMinimumLocations[target].y() << ", " << mMaximumLocations[target].y() << "\""
                            << " [label=" << mArcCosts[arc] << "];" << std::endl;
        }
    }

    horizontalGraphFile << "}" << std::endl;
    verticalGraphFile << "}" << std::endl;
}

bool TransitiveClosureGraph::projectionOverlap(std::pair<double, double> projection1, std::pair<double, double> projection2)
{
    if (projection1.first <= projection2.first) {
        return projection2.first < projection1.second;
    } else {
        return projection1.first < projection2.second;
    }
}

unsigned TransitiveClosureGraph::countInputArcs(lemon::ListDigraph::Node node, EdgeType edgeType)
{
    unsigned count = 0;
    for (auto arc = lemon::ListDigraph::InArcIt(mGraph, node); arc != lemon::INVALID; ++arc) {
        if (mArcTypes[arc] == edgeType) {
            count++;
        }
    }
    return count;
}

unsigned TransitiveClosureGraph::countOutputArcs(lemon::ListDigraph::Node node, EdgeType edgeType)
{
    unsigned count = 0;
    for (auto arc = lemon::ListDigraph::OutArcIt(mGraph, node); arc != lemon::INVALID; ++arc) {
        if (mArcTypes[arc] == edgeType) {
            count++;
        }
    }
    return count;
}
}
}
