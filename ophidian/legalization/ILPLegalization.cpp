#include "ILPLegalization.h"

namespace ophidian {
namespace legalization {

ILPLegalization::ILPLegalization(design::Design & design) :
    mDesign(design),
    mCells2VecGRBVar_(mDesign.netlist().makeProperty<std::vector<GRBVar>>(circuit::Cell())),
    mCell2CandidateLocations_(mDesign.netlist().makeProperty<std::vector<util::Location>>(circuit::Cell())),
    mInitialLocations_(mDesign.netlist().makeProperty<util::Location>(circuit::Cell())),
    mGRBenv_(){
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
        mInitialLocations_[*cellIt] = mDesign.placement().cellLocation(*cellIt);
}

bool ILPLegalization::legalizePlacement(std::vector<circuit::Cell> cells, ophidian::geometry::Box legalizationArea, unsigned numberOfCandidates){
    GRBModel model = GRBModel(mGRBenv_);

    findCandidateLocations(cells, legalizationArea, numberOfCandidates);

    addExistanceConstraints(cells, model);

    //TODO remove self overlap existance constraints
    addOverlapConstraints(cells, model);

    addObjective(cells, model);

    solve(model);

    auto status = model.get(GRB_IntAttr_Status);

    if (status == GRB_OPTIMAL || status == GRB_SUBOPTIMAL) {
        placeSolution(cells);
    }

//    model.write("gurobi.lp");

    return (status == GRB_OPTIMAL || status == GRB_SUBOPTIMAL);
//    return ophidian::legalization::legalizationCheck(mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping(), mDesign.netlist(), mDesign.fences());
}

void ILPLegalization::setInitialLocations(design::Design &design){
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++)
        mInitialLocations_[*cellIt] = design.placement().cellLocation(*cellIt);
}

void ILPLegalization::findCandidateLocations(std::vector<circuit::Cell> cells, ophidian::geometry::Box legalizationArea, unsigned numberOfCandidates){
    legalization::Subrows subrows(mDesign);
    subrows.createSubrows(cells, util::MultiBox({legalizationArea}));

    ophidian::legalization::CandidateLocations locationFinder(mDesign);
    for (auto cell : cells){
        if(mDesign.placement().isFixed(cell))
            continue;
        std::vector<util::Location> locations;
        locationFinder.findCandidateLocationsGrid(cell, mInitialLocations_[cell], numberOfCandidates, numberOfCandidates, legalizationArea, subrows, locations);
        mCell2CandidateLocations_[cell] = locations;
    }
}

void ILPLegalization::addExistanceConstraints(std::vector<circuit::Cell> cells, GRBModel & model){
    for (auto cell : cells){
        if(mDesign.placement().isFixed(cell))
            continue;
        std::vector<GRBVar> vars;
        unsigned locationIndex = 0;
        for(auto location : mCell2CandidateLocations_[cell]){
            std::string variableName = mDesign.netlist().name(cell) + boost::lexical_cast<std::string>(locationIndex++);
            GRBVar var = model.addVar(0.0, 1.0, 0.0, GRB_BINARY, variableName);
            vars.push_back(var);
            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
            auto cellBox = box(location.toPoint(), geometry::Point(location.toPoint().x()+(cellGeometry.max_corner().x()-cellGeometry.min_corner().x()),
                                                                   location.toPoint().y()+(cellGeometry.max_corner().y()-cellGeometry.min_corner().y())));
            candidateLocations_.insert(tree_node(cellBox, variableName));
            mVarName2GRBVar_[variableName] = var;
        }
        mCells2VecGRBVar_[cell] = vars;
        GRBLinExpr ExistanceConstraint = 0.0;
        for (auto var : vars)
            ExistanceConstraint += var;
        model.addConstr(ExistanceConstraint == 1);
    }
    model.update();
}

void ILPLegalization::addOverlapConstraints(std::vector<circuit::Cell> cells, GRBModel & model){
    for (auto cell : cells){
        if(mDesign.placement().isFixed(cell))
            continue;
        for(unsigned i = 0; i < mCell2CandidateLocations_[cell].size(); i++){
            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
            auto location = mCell2CandidateLocations_[cell].at(i);
            auto cellBox = box(location.toPoint(), geometry::Point(location.toPoint().x()+(cellGeometry.max_corner().x()-cellGeometry.min_corner().x()),
                                                                   location.toPoint().y()+(cellGeometry.max_corner().y()-cellGeometry.min_corner().y())));

            std::vector<tree_node> intersectingNodes;
            candidateLocations_.query(boost::geometry::index::overlaps(cellBox), std::back_inserter(intersectingNodes));
            candidateLocations_.query(boost::geometry::index::covers(cellBox), std::back_inserter(intersectingNodes));
            candidateLocations_.query(boost::geometry::index::covered_by(cellBox), std::back_inserter(intersectingNodes));

            std::sort( intersectingNodes.begin(), intersectingNodes.end(), [](tree_node n1, tree_node n2){return n1.second < n2.second;});
            intersectingNodes.erase( std::unique( intersectingNodes.begin(), intersectingNodes.end(), [](tree_node n1, tree_node n2){return n1.second == n2.second;}), intersectingNodes.end());

            GRBLinExpr ExistanceConstraint = 0.0;
            for(auto node : intersectingNodes) {
                auto var = mVarName2GRBVar_[node.second];
                ExistanceConstraint += var;
            }
            model.addConstr(ExistanceConstraint <= 1);

            GRBVar variable = mCells2VecGRBVar_[cell].at(i);
            auto variableName = variable.get(GRB_StringAttr_VarName);


            candidateLocations_.remove(tree_node(cellBox, variableName));
        }
    }
    model.update();
}

void ILPLegalization::addObjective(std::vector<circuit::Cell> cells, GRBModel & model){
    GRBLinExpr objective = 0.0;
    for(auto cell : cells){
        if(mDesign.placement().isFixed(cell))
            continue;
        for(unsigned int i = 0; i < mCells2VecGRBVar_[cell].size(); ++i)
            objective += mCells2VecGRBVar_[cell].at(i) * (std::abs(mInitialLocations_[cell].toPoint().x()-mCell2CandidateLocations_[cell].at(i).toPoint().x()) +
                                                          std::abs(mInitialLocations_[cell].toPoint().y()-mCell2CandidateLocations_[cell].at(i).toPoint().y()));
    }
    model.setObjective(objective, GRB_MINIMIZE);
}

void ILPLegalization::placeSolution(std::vector<circuit::Cell> cells){
    for(auto cell : cells){
        if(mDesign.placement().isFixed(cell))
            continue;
        double x=0, y=0;
        for(unsigned int i = 0; i < mCells2VecGRBVar_[cell].size(); ++i){
            x += mCells2VecGRBVar_[cell].at(i).get(GRB_DoubleAttr_X) * mCell2CandidateLocations_[cell].at(i).toPoint().x();
            y += mCells2VecGRBVar_[cell].at(i).get(GRB_DoubleAttr_X) * mCell2CandidateLocations_[cell].at(i).toPoint().y();
        }
        mDesign.placement().placeCell(cell, util::Location(x,y));
    }
}

void ILPLegalization::solve(GRBModel &model){
    model.set(GRB_IntParam_OutputFlag, 0);

    model.optimize();
}

}
}
