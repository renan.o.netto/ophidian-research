#ifndef PYTHONPREDICTION_H
#define PYTHONPREDICTION_H

#include <Python.h>
#include <string>
#include <fstream>

#include <ophidian/legalization/WriteImage.h>
#include <ophidian/legalization/WriteCsv.h>

namespace ophidian {
namespace legalization {

class PythonPrediction
{
public:
    PythonPrediction(design::Design & design);

    bool run(const geometry::Box &area, const std::vector<circuit::Cell> & cells);

private:
    WriteImage mWriteImage;
    WriteCsv mWriteCsv;
    std::string mDotPy = "/home/renan/workspace/ophidian-renan/python/test_histogram_ann_with_single_sample.py";
};
}
}

#endif // PYTHONPREDICTION_H
