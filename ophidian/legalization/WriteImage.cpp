#include "WriteImage.h"

namespace ophidian {
namespace legalization {

WriteImage::WriteImage(design::Design & design)
    : mDesign(design) {

}

typedef struct {
    double r;
    double g;
    double b;
} RGB;

typedef struct {
    double h;
    double s;
    double v;
} HSV;

RGB hsv_to_rgb(HSV in)
{
    if(in.s <= 0.0)
        return {in.v, in.v, in.v};

    double      hh, p, q, t, ff;
    long        i;

    hh = in.h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = in.v * (1.0 - in.s);
    q = in.v * (1.0 - (in.s * ff));
    t = in.v * (1.0 - (in.s * (1.0 - ff)));

    switch(i) {
    case 0:
        return {in.v, t, p};
    case 1:
        return {q, in.v, p};
    case 2:
        return {p, in.v, t};
    case 3:
        return {p, q, in.v};
    case 4:
        return {t, p, in.v};
    case 5:
    default:
        return {in.v, p, q};
    }
}

void WriteImage::writeImageOfLegalizationArea(const geometry::Box &area,
                                              const std::vector<circuit::Cell> &cells,
                                              util::micrometer_t result,
                                              const std::string& filename)
{
    std::vector<std::array<sf::Vertex, 4>> cellsQuad(cells.size());
    std::array<sf::Vertex, 4> simpleQuad;

    //! Used for color function
    std::normal_distribution<double> distribution(0.5, 0.05);
    std::default_random_engine engine;

    for (auto i = 0; i < cells.size(); ++i)
    {
        auto cellGeometry = mDesign.placementMapping().geometry(cells.at(i));

        sf::Color color;
        if (mDesign.placement().isFixed(cells.at(i)))
            color = sf::Color::Blue;
        else
        {
            auto rgb_color = hsv_to_rgb({300.0, 0.8, std::max(0.0, std::min(distribution(engine), 1.0))});
            color = sf::Color(static_cast<sf::Uint8>(rgb_color.r*255),
                              static_cast<sf::Uint8>(rgb_color.g*255),
                              static_cast<sf::Uint8>(rgb_color.b*255));
        }

        for (auto cellBoxIt = cellGeometry.begin(); cellBoxIt != cellGeometry.end(); cellBoxIt++)
        {
            simpleQuad[0].position = sf::Vector2f(cellBoxIt->min_corner().x(), cellBoxIt->min_corner().y());
            simpleQuad[1].position = sf::Vector2f(cellBoxIt->max_corner().x(), cellBoxIt->min_corner().y());
            simpleQuad[2].position = sf::Vector2f(cellBoxIt->max_corner().x(), cellBoxIt->max_corner().y());
            simpleQuad[3].position = sf::Vector2f(cellBoxIt->min_corner().x(), cellBoxIt->max_corner().y());

            simpleQuad[0].color = color;
            simpleQuad[1].color = color;
            simpleQuad[2].color = color;
            simpleQuad[3].color = color;
        }

        cellsQuad.push_back(simpleQuad);
    }

    sf::Vector2f min_corner(area.min_corner().x(), area.min_corner().y());
    sf::Vector2f max_corner(area.max_corner().x(), area.max_corner().y());
    sf::View view(sf::FloatRect(min_corner, max_corner));

    sf::RenderTexture texture;
    texture.create(500, 500);
    texture.setView(view);
    texture.clear(sf::Color::White);
    texture.draw(reinterpret_cast<const sf::Vertex*>(cellsQuad.data()), cellsQuad.size() * 4, sf::Quads);
    texture.setView(texture.getDefaultView());

    texture.getTexture().copyToImage().saveToFile(filename);
}

}
}

