#ifndef ICCAD2017LEGALIZATION_H
#define ICCAD2017LEGALIZATION_H

#include <ophidian/legalization/MultirowAbacus.h>
#include <ophidian/design/Design.h>
#include <ophidian/legalization/FenceRegionIsolation.h>
#include <ophidian/legalization/PartitionedLegalization.h>
#include <ophidian/legalization/CellByCellLegalization.h>
#include <ophidian/legalization/RectilinearFences.h>
#include <ophidian/legalization/CellAlignment.h>

namespace ophidian
{
namespace legalization
{
class iccad2017Legalization
{
public:
    iccad2017Legalization(ophidian::design::Design & design, std::string modelFile);

    void legalize();
    void kdtreeLegalization(unsigned int i, unsigned int maximumDisplacement);
    void isolateFloorplan();
    void restoreFloorplan();
private:
    void flipCells();
    void legalizeFences();
    void allignCellsToNearestSite();

    ophidian::design::Design & mDesign;
    MultirowAbacus mMultirowAbacus;
    FenceRegionIsolation mFenceRegionIsolation;
    std::vector<ophidian::circuit::Cell> mFloorplanBlocks;
    unsigned mCountFloorplanBlocks;
    ophidian::geometry::Box mPlaceableArea;

    std::string mModelFile;
};

} // namespace legalization
} // namespace ophidian
#endif // ICCAD2017LEGALIZATION_H
