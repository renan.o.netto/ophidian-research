#include "LegalizationImprovement.h"

namespace ophidian {
namespace legalization {
LegalizationImprovement::LegalizationImprovement(design::Design &design)
    : mDesign(design),
      mSubrows(design),
      mLegalizer(design),
      mInitialLocations(design.netlist().makeProperty<util::Location>(circuit::Cell())),
      mInitialFixed(design.netlist().makeProperty<bool>(circuit::Cell())),
      mMaxDisplacement(util::micrometer_t(0)),
      mGraphLegalizer(design)
{
    setInitial(mDesign);
}

void LegalizationImprovement::setInitial(design::Design &design)
{
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mInitialLocations[currentDesignCell] = design.placement().cellLocation(cell);
        mInitialFixed[currentDesignCell] = design.placement().isFixed(cell);
    }

    mLegalizer.setInitialLocations(design);

    mGraphLegalizer.setInitialLocations(design);
}

void LegalizationImprovement::fixLegalization(std::vector<circuit::Cell> &cells, geometry::Box area)
{
    std::vector<circuit::Cell> illegalCells;
    getUnaligned(mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping(), mDesign.netlist(), cells, illegalCells);
    getOutsideBoundaries(mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping(), mDesign.netlist(), mDesign.fences(), cells, illegalCells);
    getOverlapping(mDesign.placementMapping(), mDesign.netlist(), cells, illegalCells);

    if (illegalCells.size() == 0) {
        return;
    }

    std::vector<circuit::Cell> legalCells;
    legalCells.reserve(cells.size());
    for (auto cell : cells) {
        if (std::find(illegalCells.begin(), illegalCells.end(), cell) == illegalCells.end()) {
            legalCells.push_back(cell);
        }
    }
    legalCells.shrink_to_fit();

//    std::cout << "number of illegal cells " << illegalCells.size() << std::endl;

    util::MultiBox multiboxArea({area});
    mSubrows.createSubrows(cells, multiboxArea);

    unsigned cellIndex = 0;
    for (auto cell : illegalCells) {
        auto cellName = mDesign.netlist().name(cell);

//        std::cout << "cell " << cellIndex++ << " " << cellName << std::endl;

        auto cellLocation = mDesign.placement().cellLocation(cell);
        auto initialLocation = mInitialLocations[cell];

//        if (cellName == "h0/g58237_u0") {
//            util::Debug::mDebug = true;
//        }

        auto displacement = util::micrometer_t(std::abs(cellLocation.toPoint().x() - initialLocation.toPoint().x()) +
                std::abs(cellLocation.toPoint().y() - initialLocation.toPoint().y()));

        std::pair<circuit::Cell, util::micrometer_t> cellPair(cell, displacement);
        legalCells.push_back(cell);
        improveCell(legalCells, cellPair, area, 100000, false);


//        if (cellName == "h0/g58237_u0") {
//            util::Debug::mDebug = false;
//        }
    }
}

void LegalizationImprovement::improveLegalizationForWholeCircuit()
{
    for (auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        if (!mDesign.placement().isFixed(cell)) {
            auto cellLocation = mDesign.placement().cellLocation(cell);
            auto initialLocation = mInitialLocations[cell];
            auto displacement = util::micrometer_t(std::abs(cellLocation.toPoint().x() - initialLocation.toPoint().x()) + std::abs(cellLocation.toPoint().y() - initialLocation.toPoint().y()));
            mMaxDisplacement = std::max(mMaxDisplacement, displacement);
        }
    }

    FenceRegionIsolation fenceIsolation(mDesign);

    RectilinearFences rectFences(mDesign);
    rectFences.addBlocksToRectilinearFences();

    for (auto fence : mDesign.fences().range()) {
        auto fenceName = mDesign.fences().name(fence);
//        std::cout << "fence " << fenceName << std::endl;
        auto fenceArea = mDesign.fences().area(fence);
        geometry::Box boundingBox;
        boost::geometry::envelope(fenceArea.toMultiPolygon(), boundingBox);
        std::vector<circuit::Cell> cells (mDesign.fences().members(fence).begin(), mDesign.fences().members(fence).end());
//        std::cout << "fixing" << std::endl;
        fixLegalization(cells, boundingBox);
//        std::cout << "improving " << std::endl;
        improveLegalization(cells, boundingBox);
    }

    rectFences.eraseBlocks();

    fenceIsolation.isolateAllFenceCells();

    std::vector<circuit::Cell> cells;
    cells.reserve(mDesign.netlist().size(circuit::Cell()));
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
    {
        if(!mDesign.placement().cellHasFence(*cellIt))
        {
            cells.push_back(*cellIt);
        }
    }

//    std::cout << "improving the rest of the circuit " << std::endl;

    geometry::Box chipArea(mDesign.floorplan().chipOrigin().toPoint(), mDesign.floorplan().chipUpperRightCorner().toPoint());
    fixLegalization(cells, chipArea);
    improveLegalization(cells, chipArea);

    fenceIsolation.restoreAllFenceCells();

    auto siteHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).y();
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
    {
        if (!mDesign.placement().isFixed(*cellIt)) {
            auto cellGeometry = mDesign.placementMapping().geometry(*cellIt);
            auto cellHeight = ophidian::util::micrometer_t(cellGeometry[0].max_corner().y() - cellGeometry[0].min_corner().y());
            if(std::fmod((cellHeight/siteHeight), 2.0))
            {
                //Odd-sized cells
                auto cellPosition = mDesign.placement().cellLocation(*cellIt).y();
                if(std::fmod((cellPosition/siteHeight), 2.0))
                {
                    //placed in odd line -> flip cell
                    mDesign.placement().cellOrientation(*cellIt, "S");
                }
            }
        }
    }
}

void LegalizationImprovement::improveLegalization(std::vector<circuit::Cell> &cells, geometry::Box area)
{
    struct timeval startTime;
    gettimeofday(&startTime, NULL);

//    unsigned numberOfCellsToImprove = std::ceil(0.01*cells.size());
    unsigned numberOfCellsToImprove = std::min(0.01*cells.size(), 100.0);

    util::MultiBox multiboxArea({area});
    mSubrows.createSubrows(cells, multiboxArea);

    std::vector<std::pair<circuit::Cell, util::micrometer_t>> cellPairs;
    cellPairs.reserve(cells.size());
    for (auto cell : cells) {
        auto cellLocation = mDesign.placement().cellLocation(cell);
        auto cellDisplacement = (mDesign.placement().isFixed(cell)) ? util::micrometer_t(0) : util::micrometer_t(std::abs(cellLocation.toPoint().x() - mInitialLocations[cell].toPoint().x()) +
                                                                                                                 std::abs(cellLocation.toPoint().y() - mInitialLocations[cell].toPoint().y()));
        cellPairs.push_back(std::make_pair(cell, cellDisplacement));
    }

    std::sort(cellPairs.begin(), cellPairs.end(), CellPairComparator());

//    std::cout << "cells to improve " << numberOfCellsToImprove << std::endl;

    for (unsigned cellIndex = 0; cellIndex < numberOfCellsToImprove; cellIndex++) {
        auto cellPair = cellPairs[cellIndex];

        auto cell = cellPair.first;

        auto cellName = mDesign.netlist().name(cell);

//        std::cout << "cell " << cellIndex << " " << cellName << std::endl;

//        if (cellName == "eh1/g302687_u1") {
//            util::Debug::mDebug = true;
//        }

        auto cellDisplacement = cellPair.second;
//        std::cout << "cell displacement " << cellDisplacement << std::endl;

        improveCell(cells, cellPair, area, 100, true);

        auto newLocation = mDesign.placement().cellLocation(cell);
        auto newDisplacement = std::abs(newLocation.toPoint().x() - mInitialLocations[cell].toPoint().x()) +
                std::abs(newLocation.toPoint().y() - mInitialLocations[cell].toPoint().y());
    }

    struct timeval endTime;
    gettimeofday(&endTime, NULL);

//    auto runtime = endTime.tv_sec - startTime.tv_sec;
//    std::cout << "runtime " << runtime << std::endl;
}

void LegalizationImprovement::improveCell(std::vector<circuit::Cell> &cells, std::pair<circuit::Cell, util::micrometer_t> cellPair, geometry::Box area, unsigned maxCandidates, bool improve)
{
    util::MultiBox multiboxArea({area});

    auto cell = cellPair.first;
    auto cellDisplacement = units::unit_cast<double>(cellPair.second);

    auto cellName = mDesign.netlist().name(cell);
//        std::cout << "cell " << cellName << std::endl;
    auto cellLocation = mDesign.placement().cellLocation(cell);
    auto initialLocation = mInitialLocations[cell];

    if (util::Debug::mDebug) {
        std::cout << "cell location " << cellLocation.x() << ", " << cellLocation.y() << std::endl;
        std::cout << "cell initial location " << initialLocation.x() << ", " << initialLocation.y() << std::endl;
    }

    auto legalizationBox = createLegalizationBox(cell, initialLocation, area);
    std::vector<circuit::Cell> cellsInsideBox;
    std::vector<circuit::Cell> cellsOnTheEdge;
    std::vector<circuit::Cell> cellsOutsideBox;
    separateBoxCells(legalizationBox, cell, cells, cellsInsideBox, cellsOnTheEdge, cellsOutsideBox);
    for (auto cellOnTheEdge : cellsOnTheEdge) {
        mDesign.placement().fixLocation(cellOnTheEdge, true);
    }

    std::vector<circuit::Cell> legalizationCells;
    legalizationCells.reserve(cellsInsideBox.size() + cellsOnTheEdge.size());
    legalizationCells.insert(legalizationCells.begin(), cellsInsideBox.begin(), cellsInsideBox.end());
    legalizationCells.insert(legalizationCells.begin(), cellsOnTheEdge.begin(), cellsOnTheEdge.end());

//    std::cout << "legalization box " << legalizationBox.min_corner().x() << ", " << legalizationBox.min_corner().y()
//              << "-> " << legalizationBox.max_corner().x() << ", " << legalizationBox.max_corner().y() << std::endl;

//    std::cout << "number of cells " << legalizationCells.size() << std::endl;

    ConstraintGraph<LeftComparator> horizontalConstraintGraph(mDesign);
    ConstraintGraph<BelowComparator> verticalConstraintGraph(mDesign);
    horizontalConstraintGraph.buildConstraintGraph(legalizationCells, util::micrometer_t(legalizationBox.min_corner().x()), util::micrometer_t(legalizationBox.max_corner().x()));
    verticalConstraintGraph.buildConstraintGraph(legalizationCells, util::micrometer_t(legalizationBox.min_corner().y()), util::micrometer_t(legalizationBox.max_corner().y()));

    horizontalConstraintGraph.removeTransitiveEdges();
    verticalConstraintGraph.removeTransitiveEdges();

//    horizontalConstraintGraph.exportGraph("hgraph.gv");
//    verticalConstraintGraph.exportGraph("vgraph.gv");

    legalizationCells.push_back(cell);
    util::micrometer_t previousDisplacement(0);
    util::micrometer_t fixedCellsDisplacement(0);
    util::micrometer_t maxDisplacement(0);
    for (auto legalizationCell : legalizationCells) {
        auto currentLocation = mDesign.placement().cellLocation(legalizationCell);
        auto initialLocation = mInitialLocations[legalizationCell];
        auto xDisplacement = (currentLocation.toPoint().x() - initialLocation.toPoint().x())*(currentLocation.toPoint().x() - initialLocation.toPoint().x());
        auto yDisplacement = (currentLocation.toPoint().y() - initialLocation.toPoint().y())*(currentLocation.toPoint().y() - initialLocation.toPoint().y());
        auto displacement = util::micrometer_t(xDisplacement + yDisplacement);
        maxDisplacement = std::max(maxDisplacement, displacement);
        if (!mDesign.placement().isFixed(legalizationCell)) {
            previousDisplacement = previousDisplacement + displacement;
        } else {
            fixedCellsDisplacement = fixedCellsDisplacement + displacement;
        }
    }

//            previousDisplacement = previousDisplacement + maxDisplacement;

    for (auto legalizationCell : cellsOutsideBox) {
        auto currentLocation = mDesign.placement().cellLocation(legalizationCell);
        auto initialLocation = mInitialLocations[legalizationCell];
        auto xDisplacement = (currentLocation.toPoint().x() - initialLocation.toPoint().x())*(currentLocation.toPoint().x() - initialLocation.toPoint().x());
        auto yDisplacement = (currentLocation.toPoint().y() - initialLocation.toPoint().y())*(currentLocation.toPoint().y() - initialLocation.toPoint().y());
        auto displacement = util::micrometer_t(xDisplacement + yDisplacement);
        fixedCellsDisplacement = fixedCellsDisplacement + displacement;
    }

    util::MultiBox multibox({legalizationBox});

    unsigned numberOfCandidates = 10;
    util::micrometer_t bestResult = util::micrometer_t(std::numeric_limits<double>::max());
    util::Location bestLocation;
    std::unordered_set<std::pair<int, int>, pair_hash> visitedLocations;
    while (bestResult == util::micrometer_t(std::numeric_limits<double>::max())) {
        std::vector<util::Location> candidateLocations;
        findCandidateLocations(cell, numberOfCandidates, legalizationBox, candidateLocations);
//        findCandidateLocations(cell, numberOfCandidates, area, candidateLocations);

        if (util::Debug::mDebug) {
            std::cout << "candidate locations " << std::endl;
            for (auto location : candidateLocations) {
                std::cout << "candidate location " << location.x() << ", " << location.y() << std::endl;
            }
        }

        for (auto location : candidateLocations) {
            if (visitedLocations.find(std::make_pair((int)location.toPoint().x(), (int)location.toPoint().y())) != visitedLocations.end()) {
                continue;
            }
            visitedLocations.insert(std::make_pair((int)location.toPoint().x(), (int)location.toPoint().y()));

//            auto legalizationBox = createLegalizationBox(cell, location, area);
//            std::vector<circuit::Cell> cellsInsideBox;
//            std::vector<circuit::Cell> cellsOnTheEdge;
//            std::vector<circuit::Cell> cellsOutsideBox;
//            separateBoxCells(legalizationBox, cell, cells, cellsInsideBox, cellsOnTheEdge, cellsOutsideBox);
//            for (auto cellOnTheEdge : cellsOnTheEdge) {
//                mDesign.placement().fixLocation(cellOnTheEdge, true);
//            }

//            std::vector<circuit::Cell> legalizationCells;
//            legalizationCells.reserve(cellsInsideBox.size() + cellsOnTheEdge.size());
//            legalizationCells.insert(legalizationCells.begin(), cellsInsideBox.begin(), cellsInsideBox.end());
//            legalizationCells.insert(legalizationCells.begin(), cellsOnTheEdge.begin(), cellsOnTheEdge.end());

//            util::micrometer_t previousDisplacement(0);
//            util::micrometer_t fixedCellsDisplacement(0);
//            util::micrometer_t maxDisplacement(0);

//            for (auto legalizationCell : legalizationCells) {
//                auto currentLocation = mDesign.placement().cellLocation(legalizationCell);
//                auto initialLocation = mInitialLocations[legalizationCell];
//                auto xDisplacement = (currentLocation.toPoint().x() - initialLocation.toPoint().x())*(currentLocation.toPoint().x() - initialLocation.toPoint().x());
//                auto yDisplacement = (currentLocation.toPoint().y() - initialLocation.toPoint().y())*(currentLocation.toPoint().y() - initialLocation.toPoint().y());
//                auto displacement = util::micrometer_t(xDisplacement + yDisplacement);
//                maxDisplacement = std::max(maxDisplacement, displacement);
//                if (!mDesign.placement().isFixed(legalizationCell)) {
//                    previousDisplacement = previousDisplacement + displacement;
//                } else {
//                    fixedCellsDisplacement = fixedCellsDisplacement + displacement;
//                }
//            }

//        //            previousDisplacement = previousDisplacement + maxDisplacement;

//            for (auto legalizationCell : cellsOutsideBox) {
//                auto currentLocation = mDesign.placement().cellLocation(legalizationCell);
//                auto initialLocation = mInitialLocations[legalizationCell];
//                auto xDisplacement = (currentLocation.toPoint().x() - initialLocation.toPoint().x())*(currentLocation.toPoint().x() - initialLocation.toPoint().x());
//                auto yDisplacement = (currentLocation.toPoint().y() - initialLocation.toPoint().y())*(currentLocation.toPoint().y() - initialLocation.toPoint().y());
//                auto displacement = util::micrometer_t(xDisplacement + yDisplacement);
//                fixedCellsDisplacement = fixedCellsDisplacement + displacement;
//            }

//            util::MultiBox multibox({legalizationBox});

            mDesign.placement().placeCell(cell, location);

            horizontalConstraintGraph.addCell(cell);
            verticalConstraintGraph.addCell(cell);

//            auto result = mLegalizer.shiftCellsInsideRows(multibox, legalizationCells, mMaxDisplacement, true, improve);
            auto result = mGraphLegalizer.legalize(horizontalConstraintGraph, verticalConstraintGraph, legalizationCells, area, true);
//            auto result = mGraphLegalizer.legalize(legalizationCells, area, true);

            horizontalConstraintGraph.removeCell(cell);
            verticalConstraintGraph.removeCell(cell);


            //            mDesign.writeDefFile("test.def", legalizationCells);


            if (!improve) {
                previousDisplacement = util::micrometer_t(std::numeric_limits<double>::max());
            }
            if (result + fixedCellsDisplacement < bestResult && result < previousDisplacement) {
                bestResult = result + fixedCellsDisplacement;
                bestLocation = location;
            }

            if (util::Debug::mDebug) {
                std::cout << "candidate location " << location.x() << ", " << location.y() << std::endl;
                std::cout << "result " << result << std::endl;
                std::cout << "result + fixed " << result + fixedCellsDisplacement << std::endl;
                std::cout << "previous displacement " << previousDisplacement << std::endl;
                std::cout << "best result " << bestResult << std::endl;
            }

            mDesign.placement().placeCell(cell, cellLocation);

//            for (auto cellOnTheEdge : cellsOnTheEdge) {
//                auto cellName = mDesign.netlist().name(cellOnTheEdge);
//                if (cellName.find("block") == std::string::npos) {
//                    mDesign.placement().fixLocation(cellOnTheEdge, mInitialFixed[cellOnTheEdge]);
//                }
//            }
        }
        if (bestResult == util::micrometer_t(std::numeric_limits<double>::max()) && numberOfCandidates == maxCandidates) {
//            std::cout << "could not improve cell " << cellName << std::endl;
            break;
        }
        if (bestResult == util::micrometer_t(std::numeric_limits<double>::max())) {
            numberOfCandidates = std::min(numberOfCandidates * 2, maxCandidates);
        }
    }

    if (util::Debug::mDebug) {
        std::cout << "best location " << bestLocation.x() << ", " << bestLocation.y() << std::endl;
    }

    if (bestResult != util::micrometer_t(std::numeric_limits<double>::max())) {
//        auto legalizationBox = createLegalizationBox(cell, bestLocation, area);

//        std::vector<circuit::Cell> cellsInsideBox;
//        std::vector<circuit::Cell> cellsOnTheEdge;
//        std::vector<circuit::Cell> cellsOutsideBox;
//        separateBoxCells(legalizationBox, cell, cells, cellsInsideBox, cellsOnTheEdge, cellsOutsideBox);
//        for (auto cellOnTheEdge : cellsOnTheEdge) {

//            mDesign.placement().fixLocation(cellOnTheEdge, true);
//        }


//        std::vector<circuit::Cell> legalizationCells;
//        legalizationCells.reserve(cellsInsideBox.size() + cellsOnTheEdge.size());
//        legalizationCells.insert(legalizationCells.begin(), cellsInsideBox.begin(), cellsInsideBox.end());
//        legalizationCells.insert(legalizationCells.begin(), cellsOnTheEdge.begin(), cellsOnTheEdge.end());

        util::MultiBox multibox({legalizationBox});

        mDesign.placement().placeCell(cell, bestLocation);

        horizontalConstraintGraph.addCell(cell);
        verticalConstraintGraph.addCell(cell);

//        mLegalizer.shiftCellsInsideRows(multibox, legalizationCells, mMaxDisplacement, false, improve);
        mGraphLegalizer.legalize(horizontalConstraintGraph, verticalConstraintGraph, legalizationCells, area, false);
//        mGraphLegalizer.legalize(legalizationCells, area, false);

        horizontalConstraintGraph.removeCell(cell);
        verticalConstraintGraph.removeCell(cell);

//        for (auto cellOnTheEdge : cellsOnTheEdge) {
//            auto cellName = mDesign.netlist().name(cellOnTheEdge);
//            if (cellName.find("block") == std::string::npos) {
//                mDesign.placement().fixLocation(cellOnTheEdge, mInitialFixed[cellOnTheEdge]);
//            }
//        }
    }

    for (auto cellOnTheEdge : cellsOnTheEdge) {
        auto cellName = mDesign.netlist().name(cellOnTheEdge);
        if (cellName.find("block") == std::string::npos) {
            mDesign.placement().fixLocation(cellOnTheEdge, mInitialFixed[cellOnTheEdge]);
        }
    }
}

void LegalizationImprovement::findCandidateLocations(circuit::Cell cell, unsigned numberOfCandidates, geometry::Box area, std::vector<util::Location> & candidateLocations)
{
    auto cellName = mDesign.netlist().name(cell);

    auto rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    auto siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
    auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
    auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();
    auto cellAlignment = mDesign.placementMapping().alignment(cell);
    std::vector<Subrow> closestSubrows;

    auto alignedInitialLocation = mInitialLocations[cell];
    alignedInitialLocation.x(util::micrometer_t(std::round(alignedInitialLocation.toPoint().x() / siteWidth) * siteWidth));
    alignedInitialLocation.y(util::micrometer_t(std::round(alignedInitialLocation.toPoint().y() / siteWidth) * siteWidth));
    //    std::cout << "initial location " << mInitialLocations[cell].x() << ", " << mInitialLocations[cell].y() << std::endl;
    //    mSubrows.findClosestSubrows(numberOfCandidates, mInitialLocations[cell], closestSubrows);
    mSubrows.findClosestSubrows(numberOfCandidates, alignedInitialLocation, closestSubrows);

    for (auto subrow : closestSubrows) {
        auto subrowOrigin = mSubrows.origin(subrow);
        auto subrowUpperCorner = mSubrows.upperCorner(subrow);
        auto subrowCapacity = mSubrows.capacity(subrow);

        if (subrowCapacity < util::micrometer_t(cellWidth)) {
            continue;
        }

        placement::RowAlignment locationAlignment = (((int)(subrowOrigin.toPoint().y() / rowHeight) % 2) == 0) ? placement::RowAlignment::EVEN : placement::RowAlignment::ODD;
        if (cellAlignment != placement::RowAlignment::NA && cellAlignment != locationAlignment) {
            continue;
        }

        util::Location location(mInitialLocations[cell].x(), subrowOrigin.y());
        if (mInitialLocations[cell].x() < subrowOrigin.x()) {
            location.x(subrowOrigin.x());
        } else if (mInitialLocations[cell].x() + util::micrometer_t(cellWidth) > subrowUpperCorner.x()) {
            location.x(subrowUpperCorner.x() - util::micrometer_t(cellWidth));
        }

        if (location.toPoint().x() + cellWidth > area.max_corner().x() ||
                location.toPoint().y() + cellHeight > area.max_corner().y() ||
                location.toPoint().x() < area.min_corner().x() ||
                location.toPoint().y() < area.min_corner().y()) {
            continue;
        }

        candidateLocations.push_back(location);
    }
}

geometry::Box LegalizationImprovement::createLegalizationBox(circuit::Cell cell, util::Location candidateLocation, geometry::Box legalizationArea)
{
    auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
    auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();
    auto cellHeight = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();

    //    util::micrometer_t boxDimensions = util::micrometer_t(std::max(25000.0, cellWidth));
    //    util::micrometer_t boxHalfWidth = util::micrometer_t(legalizationArea.max_corner().x() - legalizationArea.min_corner().x());
    //    util::micrometer_t boxHalfHeight = util::micrometer_t(legalizationArea.max_corner().y() - legalizationArea.min_corner().y());
    util::micrometer_t boxHalfWidth = util::micrometer_t(std::max(10000.0, cellWidth));
    util::micrometer_t boxHalfHeight = util::micrometer_t(std::max(10000.0, cellHeight));
    util::Location boxOrigin(candidateLocation.x() - boxHalfWidth, candidateLocation.y() - boxHalfHeight);
    boxOrigin.x(std::max(boxOrigin.x(), util::micrometer_t(legalizationArea.min_corner().x())));
    boxOrigin.y(std::max(boxOrigin.y(), util::micrometer_t(legalizationArea.min_corner().y())));
    util::Location boxUpperCorner(candidateLocation.x() + boxHalfWidth, candidateLocation.y() + boxHalfHeight);
    boxUpperCorner.x(std::min(boxUpperCorner.x(), util::micrometer_t(legalizationArea.max_corner().x())));
    boxUpperCorner.y(std::min(boxUpperCorner.y(), util::micrometer_t(legalizationArea.max_corner().y())));
    geometry::Box legalizationBox(boxOrigin.toPoint(), boxUpperCorner.toPoint());

    return legalizationBox;
}

void LegalizationImprovement::separateBoxCells(geometry::Box box, circuit::Cell targetCell, const std::vector<circuit::Cell> &cells, std::vector<circuit::Cell> &cellsInsideBox, std::vector<circuit::Cell> &cellsOnTheEdge, std::vector<circuit::Cell> &cellsOutsideBox)
{
    cellsInsideBox.reserve(cells.size());
    cellsOnTheEdge.reserve(cells.size());
    cellsOutsideBox.reserve(cells.size());
    for (auto cell : cells) {
        auto fixedCell = mDesign.placement().isFixed(cell);
        if (fixedCell) {
            cellsOnTheEdge.push_back(cell);
            continue;
        }

        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];

        if (cell == targetCell) {
            continue;
        }

        if (boost::geometry::within(cellGeometry, box)) {
            cellsInsideBox.push_back(cell);
        } else if (boost::geometry::overlaps(cellGeometry, box)) {
            cellsOnTheEdge.push_back(cell);
        } else {
            cellsOutsideBox.push_back(cell);
        }
    }
}

}
}
