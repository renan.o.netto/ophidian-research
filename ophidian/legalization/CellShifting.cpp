#include "CellShifting.h"

namespace ophidian {
namespace legalization {
CellShifting::CellShifting(design::Design & design)
    : mDesign(design), mSubrows(design.netlist(), design.floorplan(), design.placement(), design.placementMapping()),
      mCircuitCellsSlices(mDesign.netlist().makeProperty<std::vector<CellSlice>>(circuit::Cell())),
      mSlice2Cell(mCellSlices), mSliceAlignment(mCellSlices), mSliceNames(mCellSlices),
      mCellsInitialLocations(design.netlist().makeProperty<util::Location>(circuit::Cell()))
{
    for (auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); cellIt++) {
        mCellsInitialLocations[*cellIt] = mDesign.placement().cellLocation(*cellIt);
    }

    mRuntime = 0;
}

void CellShifting::setInitialLocations(design::Design &design)
{
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mCellsInitialLocations[currentDesignCell] = design.placement().cellLocation(cell);
    }
}

void CellShifting::shiftCellsInsideRows()
{
    FenceRegionIsolation fenceRegionIsolation(mDesign);

    RectilinearFences rectFences(mDesign);
    rectFences.addBlocksToRectilinearFences();

    for(auto fence : mDesign.fences().range())
    {
        auto fenceArea = mDesign.fences().area(fence);
        geometry::Box boundingBox;
        boost::geometry::envelope(fenceArea.toMultiPolygon(), boundingBox);
        std::vector<circuit::Cell> cells (mDesign.fences().members(fence).begin(), mDesign.fences().members(fence).end());
        auto multibox = util::MultiBox({boundingBox});
        shiftCellsInsideRows(multibox, cells, util::micrometer_t(std::numeric_limits<double>::max()));
//        shiftCellsInsideRowsWithManhattan(util::MultiBox({boundingBox}), cells, util::micrometer_t(std::numeric_limits<double>::max()));
    }

    rectFences.eraseBlocks();

    fenceRegionIsolation.isolateAllFenceCells();

    std::vector<circuit::Cell> cells;
    cells.reserve(mDesign.netlist().size(circuit::Cell()));
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
    {
        if(!mDesign.placement().cellHasFence(*cellIt))
        {
            cells.push_back(*cellIt);
        }
    }
    cells.shrink_to_fit();
    geometry::Box chipArea(mDesign.floorplan().chipOrigin().toPoint(), mDesign.floorplan().chipUpperRightCorner().toPoint());
    util::MultiBox legalizationArea({chipArea});
    shiftCellsInsideRows(legalizationArea, cells, util::micrometer_t(std::numeric_limits<double>::max()));
//    shiftCellsInsideRowsWithManhattan(legalizationArea, cells, util::micrometer_t(std::numeric_limits<double>::max()));

    fenceRegionIsolation.restoreAllFenceCells();
}

util::micrometer_t CellShifting::shiftCellsInsideRows(util::MultiBox & area, std::vector<circuit::Cell> &cells, util::micrometer_t maxDisplacement, bool trial, bool improve)
{
    struct timeval startTime, endTime;
    gettimeofday(&startTime, NULL);

    mSubrows.createSubrows(cells, area);

    if (mSubrows.rowCount() == 0) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

//    if (util::Debug::mDebug) {
//        for (auto subrow : mSubrows.range(Subrow())) {
//            auto subrowOrigin = mSubrows.origin(subrow);
//            auto subrowUpperCorner = mSubrows.upperCorner(subrow);
//            std::cout << "subrow " << subrowOrigin.x() << ", " << subrowOrigin.y() << " -> "
//                      << subrowUpperCorner.x() << ", " << subrowUpperCorner.y() << std::endl;
//        }
//    }

    sliceCells(cells);

    float rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    float siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    entity_system::Property<Subrow, std::vector<CellSlice>> subrowsSlices(mSubrows.makeProperty<std::vector<CellSlice>>(Subrow()));
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];

            geometry::Box sliceBox(cellGeometry.min_corner(), {cellGeometry.max_corner().x(), cellGeometry.min_corner().y() + rowHeight});
            for (auto cellSlice : mCircuitCellsSlices[cell]) {
                auto subrow = mSubrows.findContainedSubrow(sliceBox);
                subrowsSlices[subrow].push_back(cellSlice);
                geometry::Box translatedBox;
                geometry::translate(sliceBox, 0, rowHeight, translatedBox);
                sliceBox = translatedBox;
            }
        }
    }

    GRBEnv env = GRBEnv();

    GRBModel model = GRBModel(env);

//    GRBVar maxDisplacement = model.addVar(0, (area[0].max_corner().x() - area[0].min_corner().x())*(area[0].max_corner().x() - area[0].min_corner().x()), 0, GRB_CONTINUOUS, "max_displacement_variable");

    unsigned rowIndex = 0;
    GRBQuadExpr objectiveFunction;
    entity_system::Property<CellSlice, GRBVar> sliceVariables(mCellSlices);
    for (auto subrow : mSubrows.range(Subrow())) {
        auto subrowCellSlices = subrowsSlices[subrow];
        auto subrowOrigin = mSubrows.origin(subrow).toPoint();
        auto subrowUpperCorner = mSubrows.upperCorner(subrow).toPoint();

        std::vector<std::pair<CellSlice, util::micrometer_t>> sortedCellSlices;
        sortedCellSlices.reserve(subrowCellSlices.size());
        for (auto slice : subrowCellSlices) {
            auto sliceCircuitCell = mSlice2Cell[slice];
//            auto cellLocation = mCellsInitialLocations[sliceCircuitCell];
            auto cellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
            sortedCellSlices.push_back(std::make_pair(slice, cellLocation.x()));
        }
        std::sort(sortedCellSlices.begin(), sortedCellSlices.end(), CellSlicePairComparator());

        for (std::size_t sliceIndex = 0; sliceIndex < sortedCellSlices.size(); sliceIndex++) {
            auto slice = sortedCellSlices[sliceIndex].first;
            auto circuitCell = mSlice2Cell[slice];
            auto cellGeometry = mDesign.placementMapping().geometry(circuitCell)[0];
            auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();

            auto cellName = mSliceNames[slice];
            std::string variableName = cellName + "_x_variable";
            auto sliceCircuitCell = mSlice2Cell[slice];
            auto currentCellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
//            GRBVar sliceXVariable = model.addVar(subrowOrigin.x() / siteWidth, (subrowUpperCorner.x() - cellWidth) / siteWidth, subrowOrigin.x() / siteWidth, GRB_CONTINUOUS, variableName);
            GRBVar sliceXVariable = model.addVar(subrowOrigin.x(), (subrowUpperCorner.x() - cellWidth), currentCellLocation.toPoint().x(), GRB_CONTINUOUS, variableName);
            sliceVariables[slice] = sliceXVariable;

//            objectiveFunction += (sliceXVariable * siteWidth - sliceInitialX) * (sliceXVariable * siteWidth - sliceInitialX);
//            objectiveFunction += (sliceXVariable - sliceInitialX) * (sliceXVariable - sliceInitialX);

            if (sliceIndex > 0) {
                auto previousSlice = sortedCellSlices[sliceIndex-1].first;
                auto previousSliceName = mSliceNames[previousSlice];
                auto previousSliceCell = mSlice2Cell[previousSlice];
                auto previousCellGeometry = mDesign.placementMapping().geometry(previousSliceCell)[0];
                auto previousSliceWidth = previousCellGeometry.max_corner().x() - previousCellGeometry.min_corner().x();
                auto previousSliceVariable = sliceVariables[previousSlice];

//                model.addConstr(previousSliceVariable + (previousSliceWidth / siteWidth) <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
                model.addConstr(previousSliceVariable + previousSliceWidth <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
            }
        }

        rowIndex++;
    }

    unsigned sliceForce = mDesign.floorplan().chipUpperRightCorner().toPoint().x();
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellSlices = mCircuitCellsSlices[cell];

            auto cellLocation = mDesign.placement().cellLocation(cell).toPoint();
            auto cellInitialLocation = mCellsInitialLocations[cell].toPoint();

            auto firstSlice = cellSlices[0];
            auto sliceVariable = sliceVariables[firstSlice];

            auto cellDisplacement = (sliceVariable - cellInitialLocation.x()) * (sliceVariable - cellInitialLocation.x());
            cellDisplacement += (cellLocation.y() - cellInitialLocation.y()) * (cellLocation.y() - cellInitialLocation.y());

//            auto cellXDisplacement = (sliceVariable - cellInitialLocation.x());
//            auto cellYDisplacement = std::abs(cellLocation.y() - cellInitialLocation.y());
//            model.addConstr(cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");
//            model.addConstr(-cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");

            objectiveFunction += cellDisplacement;

            for (unsigned sliceIndex = 1; sliceIndex < cellSlices.size(); sliceIndex++) {
                auto currentSlice = cellSlices[sliceIndex];
                auto currentSliceVariable = sliceVariables[currentSlice];
                auto currentSliceName = mSliceNames[currentSlice];

                auto previousSlice = cellSlices[sliceIndex-1];
                auto previousSliceVariable = sliceVariables[previousSlice];
                auto previousSliceName = mSliceNames[previousSlice];

                model.addConstr(currentSliceVariable == previousSliceVariable, currentSliceName + "_" + previousSliceName + "_slice_constraint");
    //            objectiveFunction += (currentSliceVariable - previousSliceVariable)*(currentSliceVariable - previousSliceVariable)*sliceForce;
            }
        }
    }

//    objectiveFunction += maxDisplacement;

    model.setObjective(objectiveFunction, GRB_MINIMIZE);
    model.set(GRB_IntParam_BarHomogeneous, GRB_BARHOMOGENEOUS_ON);
//    if (!util::Debug::mDebug) {
        model.set(GRB_IntParam_OutputFlag, 0);
//    }
    model.optimize();

//    model.write("cell_shifting.lp");

    auto status = model.get(GRB_IntAttr_Status);

    gettimeofday(&endTime, NULL);
    mRuntime += (endTime.tv_sec + endTime.tv_usec / 1e6) - (startTime.tv_sec + startTime.tv_usec / 1e6);

    if (status != GRB_OPTIMAL && status != GRB_SUBOPTIMAL) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

//    util::micrometer_t totalDisplacement(0);
    for (auto cellSlice : mCellSlices) {
        auto circuitCell = mSlice2Cell[cellSlice];
        auto cellName = mDesign.netlist().name(circuitCell);
        if (mCircuitCellsSlices[circuitCell][0] == cellSlice) {
            auto sliceVariable = sliceVariables[cellSlice];
            auto currentCellLocation = mDesign.placement().cellLocation(circuitCell);
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location without round " << sliceVariable.get(GRB_DoubleAttr_X) << ", " << currentCellLocation.y() << std::endl;
//            }
            //            auto cellXLocation = std::round(sliceVariable.get(GRB_DoubleAttr_X)) * siteWidth;
            auto cellXLocation = std::round((sliceVariable.get(GRB_DoubleAttr_X) + 0.01) / siteWidth) * siteWidth;
            //                auto cellXLocation = std::floor(sliceVariable.get(GRB_DoubleAttr_X) / siteWidth) * siteWidth;
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location with round " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//            }
            if (!trial) {
                mDesign.placement().placeCell(circuitCell, util::Location(cellXLocation, currentCellLocation.toPoint().y()));
            }

            auto initialLocation = mCellsInitialLocations[circuitCell];
            auto xDisplacement = std::abs(cellXLocation - initialLocation.toPoint().x());
            auto yDisplacement = std::abs(currentCellLocation.toPoint().y() - initialLocation.toPoint().y());
            auto displacement = util::micrometer_t(xDisplacement + yDisplacement);

//            totalDisplacement = totalDisplacement + displacement;

//            if (util::Debug::mDebug) {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "cell location " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//                std::cout << "cell initial location " << initialLocation.x() << ", " << initialLocation.y() << std::endl;
//                std::cout << "cell displacement " << displacement << std::endl;
//                std::cout << "max displacement " << maxDisplacement << std::endl;
//            }

//            if (improve && displacement > maxDisplacement) {
//                return util::micrometer_t(std::numeric_limits<double>::max());
//            }
        }
    }

//    return totalDisplacement;

    return util::micrometer_t(model.get(GRB_DoubleAttr_ObjVal));
}

util::micrometer_t CellShifting::shiftCellsInsideRows(util::MultiBox area, std::vector<circuit::Cell> &cells, util::micrometer_t maxDisplacement, CellsRtree &cellsRtree, bool trial, bool improve)
{
    struct timeval startTime, endTime;
    gettimeofday(&startTime, NULL);

    mSubrows.createSubrows(cells, area);

    if (mSubrows.rowCount() == 0) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

//    if (util::Debug::mDebug) {
//        for (auto subrow : mSubrows.range(Subrow())) {
//            auto subrowOrigin = mSubrows.origin(subrow);
//            auto subrowUpperCorner = mSubrows.upperCorner(subrow);
//            std::cout << "subrow " << subrowOrigin.x() << ", " << subrowOrigin.y() << " -> "
//                      << subrowUpperCorner.x() << ", " << subrowUpperCorner.y() << std::endl;
//        }
//    }

    sliceCells(cells);

    float rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    float siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    entity_system::Property<Subrow, std::vector<CellSlice>> subrowsSlices(mSubrows.makeProperty<std::vector<CellSlice>>(Subrow()));
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];

            geometry::Box sliceBox(cellGeometry.min_corner(), {cellGeometry.max_corner().x(), cellGeometry.min_corner().y() + rowHeight});
            for (auto cellSlice : mCircuitCellsSlices[cell]) {
                auto subrow = mSubrows.findContainedSubrow(sliceBox);
                subrowsSlices[subrow].push_back(cellSlice);
                geometry::Box translatedBox;
                geometry::translate(sliceBox, 0, rowHeight, translatedBox);
                sliceBox = translatedBox;
            }
        }
    }

    GRBEnv env = GRBEnv();

    GRBModel model = GRBModel(env);

//    GRBVar maxDisplacement = model.addVar(0, (area[0].max_corner().x() - area[0].min_corner().x())*(area[0].max_corner().x() - area[0].min_corner().x()), 0, GRB_CONTINUOUS, "max_displacement_variable");

    unsigned rowIndex = 0;
    GRBQuadExpr objectiveFunction;
    entity_system::Property<CellSlice, GRBVar> sliceVariables(mCellSlices);
    for (auto subrow : mSubrows.range(Subrow())) {
        auto subrowCellSlices = subrowsSlices[subrow];
        auto subrowOrigin = mSubrows.origin(subrow).toPoint();
        auto subrowUpperCorner = mSubrows.upperCorner(subrow).toPoint();

        std::vector<std::pair<CellSlice, util::micrometer_t>> sortedCellSlices;
        sortedCellSlices.reserve(subrowCellSlices.size());
        for (auto slice : subrowCellSlices) {
            auto sliceCircuitCell = mSlice2Cell[slice];
//            auto cellLocation = mCellsInitialLocations[sliceCircuitCell];
            auto cellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
            sortedCellSlices.push_back(std::make_pair(slice, cellLocation.x()));
        }
        std::sort(sortedCellSlices.begin(), sortedCellSlices.end(), CellSlicePairComparator());

        for (std::size_t sliceIndex = 0; sliceIndex < sortedCellSlices.size(); sliceIndex++) {
            auto slice = sortedCellSlices[sliceIndex].first;
            auto circuitCell = mSlice2Cell[slice];
            auto cellGeometry = mDesign.placementMapping().geometry(circuitCell)[0];
            auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();

            auto cellName = mSliceNames[slice];
            std::string variableName = cellName + "_x_variable";
            auto sliceCircuitCell = mSlice2Cell[slice];
            auto currentCellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
//            GRBVar sliceXVariable = model.addVar(subrowOrigin.x() / siteWidth, (subrowUpperCorner.x() - cellWidth) / siteWidth, subrowOrigin.x() / siteWidth, GRB_CONTINUOUS, variableName);
            GRBVar sliceXVariable = model.addVar(subrowOrigin.x(), (subrowUpperCorner.x() - cellWidth), currentCellLocation.toPoint().x(), GRB_CONTINUOUS, variableName);
            sliceVariables[slice] = sliceXVariable;

//            objectiveFunction += (sliceXVariable * siteWidth - sliceInitialX) * (sliceXVariable * siteWidth - sliceInitialX);
//            objectiveFunction += (sliceXVariable - sliceInitialX) * (sliceXVariable - sliceInitialX);

            if (sliceIndex > 0) {
                auto previousSlice = sortedCellSlices[sliceIndex-1].first;
                auto previousSliceName = mSliceNames[previousSlice];
                auto previousSliceCell = mSlice2Cell[previousSlice];
                auto previousCellGeometry = mDesign.placementMapping().geometry(previousSliceCell)[0];
                auto previousSliceWidth = previousCellGeometry.max_corner().x() - previousCellGeometry.min_corner().x();
                auto previousSliceVariable = sliceVariables[previousSlice];

//                model.addConstr(previousSliceVariable + (previousSliceWidth / siteWidth) <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
                model.addConstr(previousSliceVariable + previousSliceWidth <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
            }
        }

        rowIndex++;
    }

    unsigned sliceForce = mDesign.floorplan().chipUpperRightCorner().toPoint().x();
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellSlices = mCircuitCellsSlices[cell];

            auto cellLocation = mDesign.placement().cellLocation(cell).toPoint();
            auto cellInitialLocation = mCellsInitialLocations[cell].toPoint();

            auto firstSlice = cellSlices[0];
            auto sliceVariable = sliceVariables[firstSlice];

            auto cellDisplacement = (sliceVariable - cellInitialLocation.x()) * (sliceVariable - cellInitialLocation.x());
            cellDisplacement += (cellLocation.y() - cellInitialLocation.y()) * (cellLocation.y() - cellInitialLocation.y());

//            auto cellXDisplacement = (sliceVariable - cellInitialLocation.x());
//            auto cellYDisplacement = std::abs(cellLocation.y() - cellInitialLocation.y());
//            model.addConstr(cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");
//            model.addConstr(-cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");

            objectiveFunction += cellDisplacement;

            for (unsigned sliceIndex = 1; sliceIndex < cellSlices.size(); sliceIndex++) {
                auto currentSlice = cellSlices[sliceIndex];
                auto currentSliceVariable = sliceVariables[currentSlice];
                auto currentSliceName = mSliceNames[currentSlice];

                auto previousSlice = cellSlices[sliceIndex-1];
                auto previousSliceVariable = sliceVariables[previousSlice];
                auto previousSliceName = mSliceNames[previousSlice];

                model.addConstr(currentSliceVariable == previousSliceVariable, currentSliceName + "_" + previousSliceName + "_slice_constraint");
    //            objectiveFunction += (currentSliceVariable - previousSliceVariable)*(currentSliceVariable - previousSliceVariable)*sliceForce;
            }
        }
    }

//    objectiveFunction += maxDisplacement;

    model.setObjective(objectiveFunction, GRB_MINIMIZE);
    model.set(GRB_IntParam_BarHomogeneous, GRB_BARHOMOGENEOUS_ON);
//    if (!util::Debug::mDebug) {
        model.set(GRB_IntParam_OutputFlag, 0);
//    }
    model.optimize();

//    model.write("cell_shifting.lp");

    auto status = model.get(GRB_IntAttr_Status);

    gettimeofday(&endTime, NULL);
    mRuntime += (endTime.tv_sec + endTime.tv_usec / 1e6) - (startTime.tv_sec + startTime.tv_usec / 1e6);

    if (status != GRB_OPTIMAL && status != GRB_SUBOPTIMAL) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

    gettimeofday(&startTime, NULL);
//    util::micrometer_t totalDisplacement(0);
    for (auto cellSlice : mCellSlices) {
        auto circuitCell = mSlice2Cell[cellSlice];
        auto cellName = mDesign.netlist().name(circuitCell);
        if (mCircuitCellsSlices[circuitCell][0] == cellSlice) {
            auto sliceVariable = sliceVariables[cellSlice];
            auto currentCellLocation = mDesign.placement().cellLocation(circuitCell);
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location without round " << sliceVariable.get(GRB_DoubleAttr_X) << ", " << currentCellLocation.y() << std::endl;
//            }
            //            auto cellXLocation = std::round(sliceVariable.get(GRB_DoubleAttr_X)) * siteWidth;
            auto cellXLocation = std::round((sliceVariable.get(GRB_DoubleAttr_X) + 0.01) / siteWidth) * siteWidth;
            //                auto cellXLocation = std::floor(sliceVariable.get(GRB_DoubleAttr_X) / siteWidth) * siteWidth;
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location with round " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//            }
            if (!trial) {
                auto currentCellGeometry = mDesign.placementMapping().geometry(circuitCell)[0];
                CellRtreeNode cellNode(currentCellGeometry, circuitCell);
                cellsRtree.remove(cellNode);
                mDesign.placement().placeCell(circuitCell, util::Location(cellXLocation, currentCellLocation.toPoint().y()));
                auto cellWidth = currentCellGeometry.max_corner().x() - currentCellGeometry.min_corner().x();
                auto cellHeight = currentCellGeometry.max_corner().y() - currentCellGeometry.min_corner().y();
                geometry::Box newCellBox({cellXLocation, currentCellLocation.toPoint().y()}, {cellXLocation + cellWidth, currentCellLocation.toPoint().y() + cellHeight});
                cellsRtree.insert(CellRtreeNode(newCellBox, circuitCell));
            }

            auto initialLocation = mCellsInitialLocations[circuitCell];
            auto xDisplacement = std::abs(cellXLocation - initialLocation.toPoint().x());
            auto yDisplacement = std::abs(currentCellLocation.toPoint().y() - initialLocation.toPoint().y());
            auto displacement = util::micrometer_t(xDisplacement + yDisplacement);

//            totalDisplacement = totalDisplacement + displacement;

//            if (util::Debug::mDebug) {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "cell location " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//                std::cout << "cell initial location " << initialLocation.x() << ", " << initialLocation.y() << std::endl;
//                std::cout << "cell displacement " << displacement << std::endl;
//                std::cout << "max displacement " << maxDisplacement << std::endl;
//            }

//            if (improve && displacement > maxDisplacement) {
//                gettimeofday(&endTime, NULL);
//                mRuntime += (endTime.tv_sec + endTime.tv_usec / 1e6) - (startTime.tv_sec + startTime.tv_usec / 1e6);
//                return util::micrometer_t(std::numeric_limits<double>::max());
//            }
        }
    }

    gettimeofday(&endTime, NULL);
    mRuntime += (endTime.tv_sec + endTime.tv_usec / 1e6) - (startTime.tv_sec + startTime.tv_usec / 1e6);

//    return totalDisplacement;

    return util::micrometer_t(model.get(GRB_DoubleAttr_ObjVal));
}

util::micrometer_t CellShifting::shiftCellsInsideRowsWithManhattan(util::MultiBox area, std::vector<circuit::Cell> &cells, util::micrometer_t maxDisplacement, bool trial, bool improve)
{
    mSubrows.createSubrows(cells, area);

    if (mSubrows.rowCount() == 0) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

//    if (util::Debug::mDebug) {
//        for (auto subrow : mSubrows.range(Subrow())) {
//            auto subrowOrigin = mSubrows.origin(subrow);
//            auto subrowUpperCorner = mSubrows.upperCorner(subrow);
//            std::cout << "subrow " << subrowOrigin.x() << ", " << subrowOrigin.y() << " -> "
//                      << subrowUpperCorner.x() << ", " << subrowUpperCorner.y() << std::endl;
//        }
//    }

    sliceCells(cells);

    float rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();
    float siteWidth = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().x();

    entity_system::Property<Subrow, std::vector<CellSlice>> subrowsSlices(mSubrows.makeProperty<std::vector<CellSlice>>(Subrow()));
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];

            geometry::Box sliceBox(cellGeometry.min_corner(), {cellGeometry.max_corner().x(), cellGeometry.min_corner().y() + rowHeight});
            for (auto cellSlice : mCircuitCellsSlices[cell]) {
                auto subrow = mSubrows.findContainedSubrow(sliceBox);
                subrowsSlices[subrow].push_back(cellSlice);
                geometry::Box translatedBox;
                geometry::translate(sliceBox, 0, rowHeight, translatedBox);
                sliceBox = translatedBox;
            }
        }
    }

    GRBEnv env = GRBEnv();

    GRBModel model = GRBModel(env);

//    GRBVar maxDisplacement = model.addVar(0, (area[0].max_corner().x() - area[0].min_corner().x())*(area[0].max_corner().x() - area[0].min_corner().x()), 0, GRB_CONTINUOUS, "max_displacement_variable");

    unsigned rowIndex = 0;
    GRBQuadExpr objectiveFunction;
    entity_system::Property<CellSlice, GRBVar> sliceVariables(mCellSlices);
    for (auto subrow : mSubrows.range(Subrow())) {
        auto subrowCellSlices = subrowsSlices[subrow];
        auto subrowOrigin = mSubrows.origin(subrow).toPoint();
        auto subrowUpperCorner = mSubrows.upperCorner(subrow).toPoint();

        std::vector<std::pair<CellSlice, util::micrometer_t>> sortedCellSlices;
        sortedCellSlices.reserve(subrowCellSlices.size());
        for (auto slice : subrowCellSlices) {
            auto sliceCircuitCell = mSlice2Cell[slice];
//            auto cellLocation = mCellsInitialLocations[sliceCircuitCell];
            auto cellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
            sortedCellSlices.push_back(std::make_pair(slice, cellLocation.x()));
        }
        std::sort(sortedCellSlices.begin(), sortedCellSlices.end(), CellSlicePairComparator());

        for (std::size_t sliceIndex = 0; sliceIndex < sortedCellSlices.size(); sliceIndex++) {
            auto slice = sortedCellSlices[sliceIndex].first;
            auto circuitCell = mSlice2Cell[slice];
            auto cellGeometry = mDesign.placementMapping().geometry(circuitCell)[0];
            auto cellWidth = cellGeometry.max_corner().x() - cellGeometry.min_corner().x();

            auto cellName = mSliceNames[slice];
            std::string variableName = cellName + "_x_variable";
            auto sliceCircuitCell = mSlice2Cell[slice];
            auto currentCellLocation = mDesign.placement().cellLocation(sliceCircuitCell);
//            GRBVar sliceXVariable = model.addVar(subrowOrigin.x() / siteWidth, (subrowUpperCorner.x() - cellWidth) / siteWidth, subrowOrigin.x() / siteWidth, GRB_CONTINUOUS, variableName);
            GRBVar sliceXVariable = model.addVar(subrowOrigin.x(), (subrowUpperCorner.x() - cellWidth), currentCellLocation.toPoint().x(), GRB_CONTINUOUS, variableName);
            sliceVariables[slice] = sliceXVariable;

//            objectiveFunction += (sliceXVariable * siteWidth - sliceInitialX) * (sliceXVariable * siteWidth - sliceInitialX);
//            objectiveFunction += (sliceXVariable - sliceInitialX) * (sliceXVariable - sliceInitialX);

            if (sliceIndex > 0) {
                auto previousSlice = sortedCellSlices[sliceIndex-1].first;
                auto previousSliceName = mSliceNames[previousSlice];
                auto previousSliceCell = mSlice2Cell[previousSlice];
                auto previousCellGeometry = mDesign.placementMapping().geometry(previousSliceCell)[0];
                auto previousSliceWidth = previousCellGeometry.max_corner().x() - previousCellGeometry.min_corner().x();
                auto previousSliceVariable = sliceVariables[previousSlice];

//                model.addConstr(previousSliceVariable + (previousSliceWidth / siteWidth) <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
                model.addConstr(previousSliceVariable + previousSliceWidth <= sliceXVariable, cellName + "_" + previousSliceName + "_overlap_constraint");
            }
        }

        rowIndex++;
    }

    auto areaWidth = area[0].max_corner().x() - area[1].min_corner().x();

    unsigned sliceForce = mDesign.floorplan().chipUpperRightCorner().toPoint().x();
    for (auto cell : cells) {
        if (!mDesign.placement().isFixed(cell)) {
            auto cellName = mDesign.netlist().name(cell);

            auto cellSlices = mCircuitCellsSlices[cell];

            auto cellLocation = mDesign.placement().cellLocation(cell).toPoint();
            auto cellInitialLocation = mCellsInitialLocations[cell].toPoint();

            auto firstSlice = cellSlices[0];
            auto sliceVariable = sliceVariables[firstSlice];

            GRBVar cellDisplacement = model.addVar(0, areaWidth, 0, GRB_CONTINUOUS, cellName + "_displacement");
            GRBVar d1 = model.addVar(0, 1, 0, GRB_BINARY, cellName + "_d1");
            GRBVar d2 = model.addVar(0, 1, 0, GRB_BINARY, cellName + "_d2");

            model.addConstr(cellDisplacement - (sliceVariable - cellInitialLocation.x()) <= 2*areaWidth*d1, cellName + "_d1_constraint_1");
            model.addConstr(0 <= cellDisplacement - (sliceVariable - cellInitialLocation.x()), cellName + "_d1_constraint_2");

            model.addConstr(cellDisplacement - (cellInitialLocation.x() - sliceVariable) <= 2*areaWidth*d2, cellName + "_d2_constraint_1");
            model.addConstr(0 <= cellDisplacement - (cellInitialLocation.x() - sliceVariable), cellName + "_d2_constraint_2");

            model.addConstr(d1 + d2 == 1, cellName + "d1_d2_constraint");

//            auto cellDisplacement = (sliceVariable - cellInitialLocation.x()) * (sliceVariable - cellInitialLocation.x());
//            cellDisplacement += (cellLocation.y() - cellInitialLocation.y()) * (cellLocation.y() - cellInitialLocation.y());

//            auto cellXDisplacement = (sliceVariable - cellInitialLocation.x());
//            auto cellYDisplacement = std::abs(cellLocation.y() - cellInitialLocation.y());
//            model.addConstr(cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");
//            model.addConstr(-cellXDisplacement + cellYDisplacement <= units::unit_cast<double>(maxDisplacement), cellName + "_max_displacement_constraint");

            objectiveFunction += cellDisplacement + std::abs(cellLocation.y() - cellInitialLocation.y());

            for (unsigned sliceIndex = 1; sliceIndex < cellSlices.size(); sliceIndex++) {
                auto currentSlice = cellSlices[sliceIndex];
                auto currentSliceVariable = sliceVariables[currentSlice];
                auto currentSliceName = mSliceNames[currentSlice];

                auto previousSlice = cellSlices[sliceIndex-1];
                auto previousSliceVariable = sliceVariables[previousSlice];
                auto previousSliceName = mSliceNames[previousSlice];

                model.addConstr(currentSliceVariable == previousSliceVariable, currentSliceName + "_" + previousSliceName + "_slice_constraint");
    //            objectiveFunction += (currentSliceVariable - previousSliceVariable)*(currentSliceVariable - previousSliceVariable)*sliceForce;
            }
        }
    }

//    objectiveFunction += maxDisplacement;

    model.setObjective(objectiveFunction, GRB_MINIMIZE);
    model.set(GRB_IntParam_BarHomogeneous, GRB_BARHOMOGENEOUS_ON);
    if (!util::Debug::mDebug) {
        model.set(GRB_IntParam_OutputFlag, 0);
    }
    model.optimize();

//    model.write("cell_shifting.lp");

    auto status = model.get(GRB_IntAttr_Status);

    if (status != GRB_OPTIMAL && status != GRB_SUBOPTIMAL) {
        return util::micrometer_t(std::numeric_limits<double>::max());
    }

//    util::micrometer_t totalDisplacement(0);
    for (auto cellSlice : mCellSlices) {
        auto circuitCell = mSlice2Cell[cellSlice];
        auto cellName = mDesign.netlist().name(circuitCell);
        if (mCircuitCellsSlices[circuitCell][0] == cellSlice) {
            auto sliceVariable = sliceVariables[cellSlice];
            auto currentCellLocation = mDesign.placement().cellLocation(circuitCell);
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location without round " << sliceVariable.get(GRB_DoubleAttr_X) << ", " << currentCellLocation.y() << std::endl;
//            }
            //            auto cellXLocation = std::round(sliceVariable.get(GRB_DoubleAttr_X)) * siteWidth;
            auto cellXLocation = std::round((sliceVariable.get(GRB_DoubleAttr_X) + 0.01) / siteWidth) * siteWidth;
            //                auto cellXLocation = std::floor(sliceVariable.get(GRB_DoubleAttr_X) / siteWidth) * siteWidth;
//            if (util::Debug::mDebug && cellName == "h0/g57504_u0") {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "location with round " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//            }
            if (!trial) {
                mDesign.placement().placeCell(circuitCell, util::Location(cellXLocation, currentCellLocation.toPoint().y()));
            }

            auto initialLocation = mCellsInitialLocations[circuitCell];
            auto xDisplacement = std::abs(cellXLocation - initialLocation.toPoint().x());
            auto yDisplacement = std::abs(currentCellLocation.toPoint().y() - initialLocation.toPoint().y());
            auto displacement = util::micrometer_t(xDisplacement + yDisplacement);

//            totalDisplacement = totalDisplacement + displacement;

//            if (util::Debug::mDebug) {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "cell location " << cellXLocation << ", " << currentCellLocation.y() << std::endl;
//                std::cout << "cell initial location " << initialLocation.x() << ", " << initialLocation.y() << std::endl;
//                std::cout << "cell displacement " << displacement << std::endl;
//                std::cout << "max displacement " << maxDisplacement << std::endl;
//            }

            if (improve && displacement > maxDisplacement) {
                return util::micrometer_t(std::numeric_limits<double>::max());
            }
        }
    }

//    return totalDisplacement;
    return util::micrometer_t(model.get(GRB_DoubleAttr_ObjVal));
}

void CellShifting::initialize(const std::vector<circuit::Cell> &cells, const util::MultiBox &legalizationArea)
{
    mLegalizationArea = legalizationArea;
}

util::micrometer_t CellShifting::legalizeCell(circuit::Cell cell, util::Location targetLocation, bool trial)
{
    auto cellLocation = mDesign.placement().cellLocation(cell);

    mCells.push_back(cell);

    mDesign.placement().placeCell(cell, targetLocation);
    auto result = shiftCellsInsideRows(mLegalizationArea, mCells, util::micrometer_t(std::numeric_limits<double>::max()), trial);

    if (trial) {
        mDesign.placement().placeCell(cell, cellLocation);
        mCells.pop_back();
    }

    return result;
}

void CellShifting::sliceCells(std::vector<circuit::Cell> & cells)
{
    mCellSlices.clear();
    float rowHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).toPoint().y();

    for (auto cell : cells) {
        mCircuitCellsSlices[cell].clear();
        if (!mDesign.placement().isFixed(cell)) {
            auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
            auto cellHeigth = cellGeometry.max_corner().y() - cellGeometry.min_corner().y();

            int numberOfSlices = cellHeigth/rowHeight;
            for (unsigned sliceIndex = 0; sliceIndex < numberOfSlices; sliceIndex++) {
                auto cellSlice = mCellSlices.add();
                mCircuitCellsSlices[cell].push_back(cellSlice);
                mSlice2Cell[cellSlice] = cell;
                if (sliceIndex == 0) {
                    auto cellAlignment = mDesign.library().cellAlignment(mDesign.libraryMapping().cellStdCell(cell));
                    mSliceAlignment[cellSlice] = cellAlignment;
                } else {
                    mSliceAlignment[cellSlice] = placement::RowAlignment::NA;
                }

                auto cellName = mDesign.netlist().name(cell);
                std::string sliceName = cellName + "_slice" + boost::lexical_cast<std::string>(sliceIndex);
                mSliceNames[cellSlice] = sliceName;
            }
        }
    }
}
}
}
