#ifndef OPHIDIAN_LEGALIZATION_CELLLEGALIZER_H
#define OPHIDIAN_LEGALIZATION_CELLLEGALIZER_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/box.hpp>

#include <ophidian/circuit/Netlist.h>
#include <ophidian/design/Design.h>
#include <ophidian/geometry/Models.h>

#include <ophidian/legalization/Subrows.h>

#include <ophidian/util/Debug.h>

#include <vector>

typedef ophidian::geometry::Point Point;
typedef boost::geometry::model::box<ophidian::geometry::Point> Box;
typedef std::pair<Box, ophidian::circuit::Cell> RNode;
typedef boost::geometry::index::rtree<RNode, boost::geometry::index::rstar<16>> RTree;

namespace ophidian
{
namespace legalization
{

class CellLegalizer {
 public:
	CellLegalizer(design::Design & design);
	~CellLegalizer();

    void initialize(const std::vector<circuit::Cell> & cells, const ophidian::util::MultiBox & legalizationArea);

    void buildRtree(const std::vector<circuit::Cell> & legalizedCells);

    bool legalizeCell(const circuit::Cell & targetCell, const geometry::Point & targetPosition, const std::vector<circuit::Cell> & legalizedCells, const Box & legalizationRegion, bool trial = false);

    double legalizeCellWithCost(const circuit::Cell & targetCell, const geometry::Point & targetPosition, const std::vector<circuit::Cell> & legalizedCells, const Box & legalizationRegion, bool trial = false);

    util::micrometer_t legalizeCell(circuit::Cell cell, util::Location targetLocation, bool trial = false);

    bool adjustLocationToSubrowMaxBoundary(circuit::Cell cell, util::Location & targetLocation);

    void updateSubrowsRightBoundary(circuit::Cell cell, util::Location & targetLocation);

 private:
	design::Design & mDesign;

    entity_system::Property<circuit::Cell, util::Location> mInitialLocations;
    entity_system::Property<circuit::Cell, util::Location> mLegalLocations;

    std::vector<circuit::Cell> mLegalizedCells;
    util::MultiBox mLegalizationArea;

    Subrows mSubrows;
    entity_system::Property<Subrow, util::micrometer_t> mSubrowsRightBoundary;

    std::vector<std::pair<circuit::Cell, util::Location>> mMovedCells;

    RTree mTree;
};

} // namespace legalization
} // namespace ophidian

#endif
