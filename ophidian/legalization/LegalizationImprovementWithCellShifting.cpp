#include "LegalizationImprovementWithCellShifting.h"

namespace ophidian {
namespace legalization {
LegalizationImprovementWithCellShifting::LegalizationImprovementWithCellShifting(design::Design &design, std::string csvName, std::string circuitName)
    : mDesign(design),
      mSubrows(design),
      mFreeSpacesSubrows(design),
      mLegalizer(design),
      mInitialLocations(design.netlist().makeProperty<util::Location>(circuit::Cell())),
      mInitialFixed(design.netlist().makeProperty<bool>(circuit::Cell())),
      mMaxDisplacement(util::micrometer_t(0)),
      mWriteCsv(design, csvName),
//      mWriteImage(design),
      mLegalizationPrediction(design, ""),
      mLegalizationBox(design),
      mCandidateLocations(design),
      mPickCandidateCells(design),
      mCircuitName(circuitName)
{
    setInitial(mDesign);
}

void LegalizationImprovementWithCellShifting::setInitial(design::Design &design)
{
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mInitialLocations[currentDesignCell] = design.placement().cellLocation(cell);
        mInitialFixed[currentDesignCell] = design.placement().isFixed(cell);
    }

    mPickCandidateCells.setInitial(design);

    mLegalizer.setInitialLocations(design);
}

void LegalizationImprovementWithCellShifting::run(bool improve)
{
    for (auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        if (!mDesign.placement().isFixed(cell)) {
            auto cellLocation = mDesign.placement().cellLocation(cell);
            auto initialLocation = mInitialLocations[cell];
            auto displacement = util::micrometer_t(std::abs(cellLocation.toPoint().x() - initialLocation.toPoint().x()) + std::abs(cellLocation.toPoint().y() - initialLocation.toPoint().y()));
            mMaxDisplacement = std::max(mMaxDisplacement, displacement);
        }
    }

    FenceRegionIsolation fenceIsolation(mDesign);

    RectilinearFences rectFences(mDesign);
    rectFences.addBlocksToRectilinearFences();

    unsigned fenceIndex = 0;
    for (auto fence : mDesign.fences().range()) {
        auto fenceName = mDesign.fences().name(fence);
        std::cout << "fence " << fenceName << std::endl;
        auto fenceArea = mDesign.fences().area(fence);
        geometry::Box boundingBox;
        boost::geometry::envelope(fenceArea.toMultiPolygon(), boundingBox);
        std::vector<circuit::Cell> cells (mDesign.fences().members(fence).begin(), mDesign.fences().members(fence).end());
        if (improve) {
            improveLegalization(cells, boundingBox);
        } else {
            fixLegalization(cells, boundingBox);
        }
    }

    rectFences.eraseBlocks();

    fenceIsolation.isolateAllFenceCells();

    std::vector<circuit::Cell> cells;
    cells.reserve(mDesign.netlist().size(circuit::Cell()));
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
    {
        if(!mDesign.placement().cellHasFence(*cellIt))
        {
            cells.push_back(*cellIt);
        }
    }

    std::cout << "improving the rest of the circuit " << std::endl;

    geometry::Box chipArea(mDesign.floorplan().chipOrigin().toPoint(), mDesign.floorplan().chipUpperRightCorner().toPoint());
    if (improve) {
        improveLegalization(cells, chipArea);
    } else {
        fixLegalization(cells, chipArea);
    }

    fenceIsolation.restoreAllFenceCells();

    auto siteHeight = mDesign.floorplan().siteUpperRightCorner(*mDesign.floorplan().sitesRange().begin()).y();
    for(auto cellIt = mDesign.netlist().begin(circuit::Cell()); cellIt != mDesign.netlist().end(circuit::Cell()); ++cellIt)
    {
        if (!mDesign.placement().isFixed(*cellIt)) {
            auto cellGeometry = mDesign.placementMapping().geometry(*cellIt);
            auto cellHeight = ophidian::util::micrometer_t(cellGeometry[0].max_corner().y() - cellGeometry[0].min_corner().y());
            if(std::fmod((cellHeight/siteHeight), 2.0))
            {
                //Odd-sized cells
                auto cellPosition = mDesign.placement().cellLocation(*cellIt).y();
                if(std::fmod((cellPosition/siteHeight), 2.0))
                {
                    //placed in odd line -> flip cell
                    mDesign.placement().cellOrientation(*cellIt, "S");
                }
            }
        }
    }
}

void LegalizationImprovementWithCellShifting::fixLegalization(std::vector<circuit::Cell> &cells, geometry::Box area)
{
    std::vector<circuit::Cell> illegalCells;
    mPickCandidateCells.pickCandidateCellsWithOverlaps(cells, illegalCells);

    if (illegalCells.size() == 0) {
        return;
    }

    std::unordered_map<std::string, circuit::Cell> illegalCellsMap;
    for (auto cell : illegalCells) {
        auto cellName = mDesign.netlist().name(cell);
        illegalCellsMap[cellName] = cell;
    }

    std::vector<circuit::Cell> legalCells;
    legalCells.reserve(cells.size());
    for (auto cell : cells) {
        auto cellName = mDesign.netlist().name(cell);
        if (std::find(illegalCells.begin(), illegalCells.end(), cell) == illegalCells.end()) {
            legalCells.push_back(cell);
        }
    }
    legalCells.shrink_to_fit();

    mCellsRtree.clear();
    for (auto cell : legalCells) {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];
        mCellsRtree.insert(RtreeNode(cellBox, cell));
    }

    util::MultiBox multiboxArea({area});
    mSubrows.createSubrows(cells, multiboxArea);

    mCountUnlegalized = 0;
    mCount = 0;

    unsigned cellIndex = 0;
    for (auto cell : illegalCells) {
        cellIndex++;
//        if (cellIndex % 100 == 0) {
//            std::cout << "cell " << cellIndex << std::endl;
//            std::cout << "total count " << mCount << std::endl;
//            std::cout << "unlegalized " << mCountUnlegalized << std::endl;
//        }

        auto cellName = mDesign.netlist().name(cell);

        auto cellLocation = mDesign.placement().cellLocation(cell);
        auto initialLocation = mInitialLocations[cell];

        auto cellGeometry = mDesign.placementMapping().geometry(cell)[0];
        mCellsRtree.insert(RtreeNode(cellGeometry, cell));
        legalCells.push_back(cell);

        improveCell(legalCells, cell, area, 10000, false);
    }

//    std::cout << "unlegalized regions during fixing " << mCountUnlegalized << std::endl;
//    std::cout << "regions during fixing " << mCount << std::endl;
//    std::cout << "rate during fixing " << (double)mCountUnlegalized / (double)mCount << std::endl;
}

void LegalizationImprovementWithCellShifting::improveLegalization(std::vector<circuit::Cell> &cells, geometry::Box area)
{
    struct timeval startTime;
    gettimeofday(&startTime, NULL);

    mCellsRtree.clear();
    for (auto cell : cells) {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];
        mCellsRtree.insert(RtreeNode(cellBox, cell));
    }

    unsigned numberOfCellsToImprove = std::min(0.05*cells.size(), 100.0);
//    unsigned numberOfCellsToImprove = 1;

    util::MultiBox multiboxArea({area});
    mSubrows.createSubrows(cells, multiboxArea);

    std::vector<circuit::Cell> candidateCells;
    mPickCandidateCells.pickCandidateCellsWithGreatestDisplacement(numberOfCellsToImprove, cells, candidateCells);

    mCountUnlegalized = 0;
    mCount = 0;
    mAvoidedTrials = 0;

    for (auto cell : cells) {
        mDesign.placement().fixLocation(cell, true);
    }
    mFreeSpacesSubrows.createSubrows(cells, multiboxArea);
    for (auto cell : cells) {
        auto cellName = mDesign.netlist().name(cell);
        if (cellName.find("block") == std::string::npos) {
            mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
        }
    }

//    std::cout << "cells to improve " << numberOfCellsToImprove << std::endl;

    unsigned cellIndex = 0;
    for (auto cell : candidateCells) {
        cellIndex++;
//        std::cout << "cell " << cellIndex << std::endl;
//        std::cout << "total count " << mCount << std::endl;
//        std::cout << "unlegalized " << mCountUnlegalized << std::endl;

        auto cellName = mDesign.netlist().name(cell);

//        std::cout << "cell " << cellIndex << " " << cellName << std::endl;

        improveCell(cells, cell, area, 100, true);

        auto newLocation = mDesign.placement().cellLocation(cell);
        auto newDisplacement = std::abs(newLocation.toPoint().x() - mInitialLocations[cell].toPoint().x()) +
                std::abs(newLocation.toPoint().y() - mInitialLocations[cell].toPoint().y());

//        std::cout << "new displacement " << newDisplacement << std::endl;
    }

    std::cout << "unlegalized regions during improvement " << mCountUnlegalized << std::endl;
    std::cout << "regions during improvement " << mCount << std::endl;
    std::cout << "rate during improvement " << (double)mCountUnlegalized / (double)mCount << std::endl;
    std::cout << "avoided trials during improvement " << mAvoidedTrials << std::endl;

//    std::cout << "runtime of cell shifting " << mLegalizer.mRuntime << std::endl;
//    std::cout << "runtime of prediction " << mLegalizationPrediction.mRuntime << std::endl;
//    std::cout << "runtime of candidate locations " << mRuntime << std::endl;

    struct timeval endTime;
    gettimeofday(&endTime, NULL);

//    auto runtime = endTime.tv_sec - startTime.tv_sec;
//    std::cout << "runtime " << runtime << std::endl;
}

void LegalizationImprovementWithCellShifting::improveCell(std::vector<circuit::Cell> &cells, circuit::Cell cell, geometry::Box area, unsigned maxCandidates, bool improve)
{
    util::MultiBox multiboxArea({area});

    if (!improve) {
        for (auto otherCell : cells) {
            if (otherCell != cell) {
                mDesign.placement().fixLocation(cell, true);
            }
        }
        mFreeSpacesSubrows.createSubrows(cells, multiboxArea);
        for (auto otherCell : cells) {
            auto cellName = mDesign.netlist().name(otherCell);
            if (otherCell != cell && cellName.find("block") == std::string::npos) {
                mDesign.placement().fixLocation(cell, mInitialFixed[cell]);
            }
        }
    }

    auto cellName = mDesign.netlist().name(cell);
//        std::cout << "cell " << cellName << std::endl;
    auto cellLocation = mDesign.placement().cellLocation(cell);
    auto initialLocation = mInitialLocations[cell];

//    if (util::Debug::mDebug) {
//        std::cout << "cell location " << cellLocation.x() << ", " << cellLocation.y() << std::endl;
//        std::cout << "cell initial location " << initialLocation.x() << ", " << initialLocation.y() << std::endl;
//    }

    unsigned numberOfCandidates = 10;
    util::micrometer_t bestResult = util::micrometer_t(std::numeric_limits<double>::max());
    util::Location bestLocation;
    std::unordered_set<std::pair<int, int>, pair_hash> visitedLocations;
    while (bestResult == util::micrometer_t(std::numeric_limits<double>::max())) {
        std::vector<util::Location> candidateLocations;
        if (improve) {
            mCandidateLocations.findCandidateLocations(cell, initialLocation, numberOfCandidates / 2, area, mSubrows, candidateLocations);
            mCandidateLocations.findCandidateLocations(cell, initialLocation, numberOfCandidates / 2, area, mFreeSpacesSubrows, candidateLocations);
        } else {
            mCandidateLocations.findCandidateLocations(cell, initialLocation, numberOfCandidates, area, mFreeSpacesSubrows, candidateLocations);
        }

        if (util::Debug::mDebug) {
            std::cout << "candidate locations " << std::endl;
            for (auto location : candidateLocations) {
                std::cout << "candidate location " << location.x() << ", " << location.y() << std::endl;
            }
        }

        for (auto location : candidateLocations) {
            if (visitedLocations.find(std::make_pair((int)location.toPoint().x(), (int)location.toPoint().y())) != visitedLocations.end()) {
                continue;
            }
            visitedLocations.insert(std::make_pair((int)location.toPoint().x(), (int)location.toPoint().y()));

            auto legalizationBox = mLegalizationBox.createLegalizationBox(cell, location, area);
            std::vector<circuit::Cell> cellsInsideBox;
            std::vector<circuit::Cell> cellsOnTheEdge;
            mLegalizationBox.separateBoxCells(legalizationBox, cell, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);
            for (auto cellOnTheEdge : cellsOnTheEdge) {
                mDesign.placement().fixLocation(cellOnTheEdge, true);
            }

            std::vector<circuit::Cell> legalizationCells;
            legalizationCells.reserve(cellsInsideBox.size() + cellsOnTheEdge.size());
            legalizationCells.insert(legalizationCells.begin(), cellsInsideBox.begin(), cellsInsideBox.end());
            legalizationCells.insert(legalizationCells.begin(), cellsOnTheEdge.begin(), cellsOnTheEdge.end());

            util::micrometer_t previousDisplacement(0);
            util::micrometer_t maxDisplacement(0);

            for (auto legalizationCell : legalizationCells) {
                auto currentLocation = mDesign.placement().cellLocation(legalizationCell);
                auto initialLocation = mInitialLocations[legalizationCell];
                auto xDisplacement = (currentLocation.toPoint().x() - initialLocation.toPoint().x())*(currentLocation.toPoint().x() - initialLocation.toPoint().x());
                auto yDisplacement = (currentLocation.toPoint().y() - initialLocation.toPoint().y())*(currentLocation.toPoint().y() - initialLocation.toPoint().y());
                auto displacement = util::micrometer_t(xDisplacement + yDisplacement);
                maxDisplacement = std::max(maxDisplacement, displacement);
                if (!mDesign.placement().isFixed(legalizationCell)) {
                    previousDisplacement = previousDisplacement + displacement;
                }
            }

            util::MultiBox multibox({legalizationBox});

            mDesign.placement().placeCell(cell, location);

            auto result = util::micrometer_t(std::numeric_limits<double>::max());
//            if (!improve || mLegalizationPrediction.predictWithHistogram(legalizationBox, legalizationCells)) {
                result = mLegalizer.shiftCellsInsideRows(multibox, legalizationCells, mMaxDisplacement, true, improve);
//            result = mLegalizer.shiftCellsInsideRows(multibox, legalizationCells, util::micrometer_t(std::numeric_limits<double>::max()), true, improve);
//            } else {
//                mAvoidedTrials++;
//            }
            if (result == util::micrometer_t(std::numeric_limits<double>::max())) {
                mCountUnlegalized++;
            }
            mCount++;
            mCountGlobal++;

//            if (mCount == 32 || mCount == 33) {
//                std::cout << "cell " << cellName << std::endl;
//                std::cout << "target location " << location.x() << ", " << location.y() << std::endl;
//                std::cout << "box " << legalizationBox.min_corner().x() << ", " << legalizationBox.min_corner().y() << " -> "
//                          << legalizationBox.max_corner().x() << ", " << legalizationBox.max_corner().y() << std::endl;
//                mDesign.writeDefFile("test" + boost::lexical_cast<std::string>(mCount) + ".def", legalizationCells);
//            }

//            if (improve) {
////                mWriteCsv.writeCsvForLegalizationArea(legalizationBox, legalizationCells, cell, result);
//                auto directory = (result == util::micrometer_t(std::numeric_limits<double>::max())) ? "images_illegal/" : "images_legal/";
//                auto fileName =  directory + mCircuitName  + "_" + boost::lexical_cast<std::string>(mCountGlobal) + ".png";
//                mWriteImage.writeImageOfLegalizationArea(legalizationBox, cells, result, fileName);
//            }

            if (!improve) {
                previousDisplacement = util::micrometer_t(std::numeric_limits<double>::max());
            }

            if (util::Debug::mDebug) {
                std::cout << "candidate location " << location.x() << ", " << location.y() << std::endl;
                std::cout << "result " << result << std::endl;
                std::cout << "previous displacement " << previousDisplacement << std::endl;
                std::cout << "best result " << bestResult << std::endl;
            }

            if (result < bestResult && result < previousDisplacement) {
                bestResult = result;
                bestLocation = location;
            }

            mDesign.placement().placeCell(cell, cellLocation);

            for (auto cellOnTheEdge : cellsOnTheEdge) {
                auto cellName = mDesign.netlist().name(cellOnTheEdge);
                if (cellName.find("block") == std::string::npos) {
                    mDesign.placement().fixLocation(cellOnTheEdge, mInitialFixed[cellOnTheEdge]);
                }
            }
        }
        if (bestResult == util::micrometer_t(std::numeric_limits<double>::max()) && numberOfCandidates == maxCandidates) {
//            std::cout << "could not improve cell " << cellName << std::endl;
            break;
        }
        if (bestResult == util::micrometer_t(std::numeric_limits<double>::max())) {
            numberOfCandidates = std::min(numberOfCandidates * 2, maxCandidates);
        }
    }

//    if (util::Debug::mDebug) {
//        std::cout << "best location " << bestLocation.x() << ", " << bestLocation.y() << std::endl;
//    }

    if (bestResult != util::micrometer_t(std::numeric_limits<double>::max())) {
        auto legalizationBox = mLegalizationBox.createLegalizationBox(cell, bestLocation, area);
//        auto legalizationBox = createLegalizationBox(cell, bestLocation, area);

        std::vector<circuit::Cell> cellsInsideBox;
        std::vector<circuit::Cell> cellsOnTheEdge;
        mLegalizationBox.separateBoxCells(legalizationBox, cell, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);
//        separateBoxCells(legalizationBox, cell, cells, cellsInsideBox, cellsOnTheEdge);
        for (auto cellOnTheEdge : cellsOnTheEdge) {
            mDesign.placement().fixLocation(cellOnTheEdge, true);
        }

        std::vector<circuit::Cell> legalizationCells;
        legalizationCells.reserve(cellsInsideBox.size() + cellsOnTheEdge.size());
        legalizationCells.insert(legalizationCells.begin(), cellsInsideBox.begin(), cellsInsideBox.end());
        legalizationCells.insert(legalizationCells.begin(), cellsOnTheEdge.begin(), cellsOnTheEdge.end());

        util::MultiBox multibox({legalizationBox});

        auto currentGeometry = mDesign.placementMapping().geometry(cell)[0];
        mCellsRtree.remove(RtreeNode(currentGeometry, cell));

        mDesign.placement().placeCell(cell, bestLocation);

        mLegalizer.shiftCellsInsideRows(multibox, legalizationCells, mMaxDisplacement, mCellsRtree, false, improve);

        for (auto cellOnTheEdge : cellsOnTheEdge) {
            auto cellName = mDesign.netlist().name(cellOnTheEdge);
            if (cellName.find("block") == std::string::npos) {
                mDesign.placement().fixLocation(cellOnTheEdge, mInitialFixed[cellOnTheEdge]);
            }
        }
    }
}


}
}
