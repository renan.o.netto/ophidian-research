#include "CandidateCells.h"

namespace ophidian {
namespace legalization {
CandidateCells::CandidateCells(design::Design &design)
    : mDesign(design),
      mInitialLocations(design.netlist().makeProperty<util::Location>(circuit::Cell()))
{

}

void CandidateCells::setInitial(design::Design &design)
{
    for (auto cellIt = design.netlist().begin(circuit::Cell()); cellIt != design.netlist().end(circuit::Cell()); cellIt++) {
        auto cell = *cellIt;
        auto cellName = design.netlist().name(cell);
        auto currentDesignCell = mDesign.netlist().find(circuit::Cell(), cellName);
        mInitialLocations[currentDesignCell] = design.placement().cellLocation(cell);
    }
}

void CandidateCells::pickCandidateCellsWithGreatestDisplacement(unsigned numberOfCandidates, const std::vector<circuit::Cell> &cells, std::vector<circuit::Cell> &candidateCells)
{
    candidateCells.clear();

    std::vector<std::pair<circuit::Cell, util::micrometer_t>> cellPairs;
    cellPairs.reserve(cells.size());
    for (auto cell : cells) {
        auto cellLocation = mDesign.placement().cellLocation(cell);
        auto cellDisplacement = (mDesign.placement().isFixed(cell)) ? util::micrometer_t(0) : util::micrometer_t(std::abs(cellLocation.toPoint().x() - mInitialLocations[cell].toPoint().x()) +
                                                                                                                 std::abs(cellLocation.toPoint().y() - mInitialLocations[cell].toPoint().y()));
        cellPairs.push_back(std::make_pair(cell, cellDisplacement));
    }

    std::sort(cellPairs.begin(), cellPairs.end(), CellPairComparator());

    for (unsigned cellIndex = 0; cellIndex < numberOfCandidates; cellIndex++) {
        auto cellPair = cellPairs.at(cellIndex);
        candidateCells.push_back(cellPair.first);
    }
}

void CandidateCells::pickCandidateCellsWithOverlaps(const std::vector<circuit::Cell> &cells, std::vector<circuit::Cell> &candidateCells)
{
    candidateCells.clear();

    std::vector<circuit::Cell> illegalCells;
    getUnaligned(mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping(), mDesign.netlist(), cells, illegalCells);
    getOutsideBoundaries(mDesign.floorplan(), mDesign.placement(), mDesign.placementMapping(), mDesign.netlist(), mDesign.fences(), cells, illegalCells);
    getOverlapping(mDesign.placementMapping(), mDesign.netlist(), cells, illegalCells);

    if (illegalCells.size() == 0) {
        return;
    }

    std::vector<std::pair<circuit::Cell, util::micrometer_t>> cellPairs;
    cellPairs.reserve(cells.size());
    for (auto cell : illegalCells) {
        auto cellBox = mDesign.placementMapping().geometry(cell)[0];
        auto cellArea = boost::geometry::area(cellBox);
        cellPairs.push_back(std::make_pair(cell, util::micrometer_t(cellArea)));
    }

    std::sort(cellPairs.begin(), cellPairs.end(), CellPairComparator());

    for (auto cellPair : cellPairs) {
        candidateCells.push_back(cellPair.first);
    }
}
}
}
