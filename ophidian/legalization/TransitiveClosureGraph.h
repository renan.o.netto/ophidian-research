#ifndef TRANSITIVECLOSUREGRAPH_H
#define TRANSITIVECLOSUREGRAPH_H

#include <fstream>

#include <lemon/list_graph.h>

#include <ophidian/util/GraphOperations.h>

#include <ophidian/design/Design.h>

namespace ophidian {
namespace legalization {
class TransitiveClosureGraph
{
public:
    enum class EdgeType
    {
        HORIZONTAL, VERTICAL
    };

    TransitiveClosureGraph(design::Design &design);

    void buildConstraintGraph(const std::vector<circuit::Cell> & cells, geometry::Box area);

    bool hasHorizontalEdge(circuit::Cell cell1, circuit::Cell cell2);

    bool hasVerticalEdge(circuit::Cell cell1, circuit::Cell cell2);

    void removeTransitiveEdges();

    void calculateSlacks(geometry::Box area);

    util::micrometer_t slack(circuit::Cell cell, EdgeType edgeType);

    util::micrometer_t worstSlack(EdgeType edgeType);

    void exportGraph(std::string horizontalFileName, std::string verticalFileName);
private:
    bool projectionOverlap(std::pair<double, double> projection1, std::pair<double, double> projection2);

    unsigned countInputArcs(lemon::ListDigraph::Node node, EdgeType edgeType);

    unsigned countOutputArcs(lemon::ListDigraph::Node node, EdgeType edgeType);

    design::Design & mDesign;

    lemon::ListDigraph mGraph;

    lemon::ListDigraph::Node mSource;
    lemon::ListDigraph::Node mSink;

    entity_system::Property<circuit::Cell, lemon::ListDigraph::Node> mCell2Node;
    lemon::ListDigraph::NodeMap<circuit::Cell> mNode2Cell;

    lemon::ListDigraph::ArcMap<double> mArcCosts;
    lemon::ListDigraph::ArcMap<EdgeType> mArcTypes;

    lemon::ListDigraph::NodeMap<util::Location> mMinimumLocations;
    lemon::ListDigraph::NodeMap<util::Location> mMaximumLocations;
};
}
}

#endif // TRANSITIVECLOSUREGRAPH_H
