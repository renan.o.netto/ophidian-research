#ifndef OPHIDIANLEGALIZATIONPREDICTION_H
#define OPHIDIANLEGALIZATIONPREDICTION_H

#include <sys/time.h>

//#include <tensorflow/core/protobuf/meta_graph.pb.h>
#include <tensorflow/core/public/session.h>
#include <tensorflow/core/public/session_options.h>

#include <ophidian/geometry/Models.h>

#include <ophidian/design/Design.h>
#include <ophidian/legalization/Subrows.h>

#include <iostream>

namespace ophidian {
namespace legalization {
class LegalizationPrediction
{
public:
    using RtreeNode = std::pair<geometry::Box, circuit::Cell>;
    using Rtree = boost::geometry::index::rtree<RtreeNode, boost::geometry::index::rstar<16> >;

    LegalizationPrediction(design::Design & design, std::string modelFile);

    bool predict(const geometry::Box &area, const std::vector<circuit::Cell> & cells);

    bool predict(std::string imageFileName);

//    tensorflow::Status readTensorFromImageFile(std::string file_name, const int input_height, const int input_width, const float input_mean, const float input_std, std::vector<tensorflow::Tensor>* out_tensors);

private:
    void checkStatus(const tensorflow::Status & status);

    void addDensityAndAreas(const geometry::Box & area, const std::vector<circuit::Cell> & cells, tensorflow::Tensor & inputs);
    void addHistogramData(const geometry::Box & area, const std::vector<circuit::Cell> & cells, tensorflow::Tensor & inputs);

    design::Design & mDesign;

    tensorflow::Session * mSession;
    tensorflow::GraphDef mGraphDef;

    Subrows mSubrows;

public:
    double mRuntime;
};
}
}

#endif // OPHIDIANLEGALIZATIONPREDICTION_H
