#include <catch.hpp>
#include <iostream>
#include <fstream>
#include "apps/cada001/wrapper/wrapper.h"
#include <ophidian/legalization/MultirowAbacus.h>
#include <ophidian/legalization/LegalizationCheck.h>
#include <ophidian/legalization/iccad2017Legalization.h>
#include <ophidian/design/Design.h>
#include <ophidian/design/DesignBuilder.h>
#include <ophidian/legalization/iccad2017SolutionQuality.h>
#include <string>

#include <chrono>
#include <random>
#include <omp.h>

void runMultirowAbacusICCAD2015(std::string strategy, unsigned int i, unsigned int maximumDisplacement, std::string lef, std::string def, std::string verilog, std::string output_def, std::string modelFile){
    ophidian::designBuilder::ICCAD2015ContestDesignBuilder ICCAD2015DesignBuilder(lef, def, verilog);
    ICCAD2015DesignBuilder.build();
    ophidian::design::Design & design = ICCAD2015DesignBuilder.design();

    ophidian::legalization::iccad2017Legalization iccad2017(design, modelFile);

    ophidian::legalization::ICCAD2017SolutionQuality quality(design, design, "circuit");


    auto t1 = std::chrono::high_resolution_clock::now();
    if(strategy == "legalization")
        iccad2017.legalize();
    else if(strategy == "kdtreelegalization")
        iccad2017.kdtreeLegalization(i, maximumDisplacement);
    else{
        std::cout<<"error wrong strategy"<<std::endl;
        return;
    }
    auto t2 = std::chrono::high_resolution_clock::now();

    if(!ophidian::legalization::legalizationCheck(design.floorplan(), design.placement(), design.placementMapping(), design.netlist(), design.fences()))
        std::cout<<"Circuit is ilegal."<<std::endl;

//    std::cout<<"Total Displacement: "<<quality.totalDisplacement()<<std::endl;
//    std::cout<<"Average Displacement: "<<quality.avgDisplacement()<<std::endl;
//    std::cout<<"Maximum Cell Movement(lines): "<<quality.maximumCellMovement()<<std::endl;

    std::cout<<quality.totalDisplacement()<<", "<<quality.avgDisplacement()<<", "<<quality.maximumCellMovement()<<", "<<quality.hpwl(design)<<", "<<quality.avghpwl(design)<<", "<<std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count()<<std::endl;

    //    design.writeDefFile(output_def);
}

void runMultirowAbacusICCAD2017(std::string strategy, unsigned int i, unsigned int maximumDisplacement, std::string tech_lef, std::string cell_lef, std::string input_def, unsigned int cpu, std::string placement_constraints, std::string output_def, std::string modelFile){
    omp_set_num_threads(cpu);
    ophidian::designBuilder::ICCAD2017ContestDesignBuilder ICCAD2017DesignBuilder(cell_lef, tech_lef, input_def);
    ICCAD2017DesignBuilder.build();
    ophidian::design::Design & design = ICCAD2017DesignBuilder.design();


    ophidian::legalization::iccad2017Legalization iccad2017(design, modelFile);

    ophidian::legalization::ICCAD2017SolutionQuality quality(design, design, "circuit");

    auto t1 = std::chrono::high_resolution_clock::now();
    if(strategy == "legalization")
        iccad2017.legalize();
    else if(strategy == "kdtreelegalization")
        iccad2017.kdtreeLegalization(i, maximumDisplacement);
    else{
        std::cout<<"error wrong strategy"<<std::endl;
        return;
    }
    auto t2 = std::chrono::high_resolution_clock::now();

    if(!ophidian::legalization::legalizationCheck(design.floorplan(), design.placement(), design.placementMapping(), design.netlist(), design.fences()))
        std::cout<<"Circuit is ilegal."<<std::endl;

    std::cout<<quality.totalDisplacement()<<", "<<quality.avgDisplacement()<<", "<<quality.maximumCellMovement()<<", "<<quality.hpwl(design)<<", "<<quality.avghpwl(design)<<", "<<std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count()<<std::endl;
}

void randomPlacementWrite(std::string lef, std::string def, std::string verilog, std::string output_def){
    ophidian::designBuilder::ICCAD2015ContestDesignBuilder ICCAD2015DesignBuilder(lef, def, verilog);
    ICCAD2015DesignBuilder.build();
    ophidian::design::Design & design = ICCAD2015DesignBuilder.design();

    std::default_random_engine gen(42);
    std::uniform_int_distribution<> dis(-10000, 10000);

    for (auto cellIt = design.netlist().begin(ophidian::circuit::Cell()); cellIt != design.netlist().end(ophidian::circuit::Cell()); cellIt++)
    {
        if (design.placement().isFixed(*cellIt) == false)
            design.placement().placeCell(*cellIt, ophidian::util::Location(design.placement().cellLocation(*cellIt).toPoint().x()+dis(gen),
                                                                           design.placement().cellLocation(*cellIt).toPoint().y()+dis(gen)));
    }
    design.writeDefFile(output_def);
}

TEST_CASE("ICCAD2015 Kdtree legalization maximum displacement", "[maximum_displacement]") {
    std::vector<std::string> circuits = {"superblue10", "superblue18", "superblue4", "superblue7", "superblue1", "superblue16", "superblue3", "superblue5"};

    std::vector<unsigned int> maximumDisplacement = {5, 10, 15};

    for(auto maxDispl : maximumDisplacement){
        std::cout<<"CircuitName, InitialNumbeOfPartitions, NumberOfMerges, MergesByMaxDisplacement, TotalDisplacement, AverageDisplacement, MaximumDisplacement, TotalHPWL, AverageHPWL, Runtime"<<std::endl;
        for(auto c : circuits){
            std::cout<<c<<", "<<9<<", ";
            runMultirowAbacusICCAD2015("kdtreelegalization",
                                       9,
                                       maxDispl,
                                       "./input_files/"+c+"/"+c+".lef",
                                       "./input_files/SBCCI2018_benchmarks/MODIFIED"+c+".def",
                                       "./input_files/"+c+"/"+c+".v",
                                       "./input_files/9/"+c+".def",
                                       "");
        }
    }
}

TEST_CASE("ICCAD2017 Kdtree legalization maximum displacement", "[maximum_displacement]") {
    std::vector<std::string> circuits = {"pci_bridge32_b_md2", "pci_bridge32_a_md2", "pci_bridge32_a_md1", "pci_bridge32_b_md1"};

    std::vector<unsigned int> maximumDisplacement = {5, 10, 15};

    for(auto maxDispl : maximumDisplacement){
        std::cout<<"CircuitName, InitialNumbeOfPartitions, NumberOfMerges, MergesByMaxDisplacement, TotalDisplacement, AverageDisplacement, MaximumDisplacement, TotalHPWL, AverageHPWL, Runtime"<<std::endl;
        for(auto c : circuits){
            std::cout<<c<<", "<<9<<", ";
            runMultirowAbacusICCAD2017("kdtreelegalization",
                                       9,
                                       maxDispl,
                                       "./input_files/benchmarks2017/"+c+"/tech.lef",
                                       "./input_files/benchmarks2017/"+c+"/cells_modified.lef",
                                       "./input_files/benchmarks2017/"+c+"/placed.def",
                                       4,
                                       "./input_files/benchmarks2017/"+c+"/placement.constraints",
                                       "./"+c+".def",
                                       "");
        }
    }
}

TEST_CASE("ICCAD2015 Kdtree legalization maximum displacement with prediction", "[maximum_displacement]") {
    std::vector<std::string> circuits = {"superblue10", "superblue18", "superblue4", "superblue7", "superblue1", "superblue16", "superblue3", "superblue5"};

    std::vector<std::string> modelFiles = {"./histogram_full_model_10000.pb", "./histogram_full_model_20000.pb", "./histogram_full_model_30000.pb"};

    for(auto modelFile : modelFiles){
        std::cout<<"CircuitName, InitialNumbeOfPartitions, NumberOfMerges, MergesByMaxDisplacement, TotalDisplacement, AverageDisplacement, MaximumDisplacement, TotalHPWL, AverageHPWL, Runtime"<<std::endl;
        for(auto c : circuits){
            std::cout<<c<<", "<<9<<", ";
            runMultirowAbacusICCAD2015("kdtreelegalization",
                                       9,
                                       5,
                                       "./input_files/"+c+"/"+c+".lef",
                                       "./input_files/SBCCI2018_benchmarks/MODIFIED"+c+".def",
                                       "./input_files/"+c+"/"+c+".v",
                                       "./input_files/9/"+c+".def",
                                       modelFile);
        }
    }
}

TEST_CASE("ICCAD2017 Kdtree legalization maximum displacement with prediction", "[maximum_displacement]") {
    std::vector<std::string> circuits = {"pci_bridge32_b_md2", "pci_bridge32_a_md2", "pci_bridge32_a_md1", "pci_bridge32_b_md1"};

    std::vector<std::string> modelFiles = {"./histogram_full_model_10000.pb", "./histogram_full_model_20000.pb", "./histogram_full_model_30000.pb"};

    for(auto modelFile : modelFiles){
        std::cout<<"CircuitName, InitialNumbeOfPartitions, NumberOfMerges, MergesByMaxDisplacement, TotalDisplacement, AverageDisplacement, MaximumDisplacement, TotalHPWL, AverageHPWL, Runtime"<<std::endl;
        for(auto c : circuits){
            std::cout<<c<<", "<<9<<", ";
            runMultirowAbacusICCAD2017("kdtreelegalization",
                                       9,
                                       5,
                                       "./input_files/benchmarks2017/"+c+"/tech.lef",
                                       "./input_files/benchmarks2017/"+c+"/cells_modified.lef",
                                       "./input_files/benchmarks2017/"+c+"/placed.def",
                                       4,
                                       "./input_files/benchmarks2017/"+c+"/placement.constraints",
                                       "./"+c+".def",
                                       modelFile);
        }
    }
}
