#include <catch.hpp>
#include <sys/time.h>

#include <ophidian/design/DesignBuilder.h>

#include <ophidian/util/KDTreePartitioning.h>
#include <ophidian/legalization/PartitionedLegalization.h>

#include <ophidian/legalization/CellAlignment.h>

#include <ophidian/legalization/MultirowAbacus.h>

#include <ophidian/legalization/WriteCsv.h>
#include <ophidian/legalization/WriteImage.h>

#include <ophidian/legalization/FenceRegionIsolation.h>

#include <ophidian/legalization/iccad2017Legalization.h>

#include <ophidian/legalization/iccad2017SolutionQuality.h>

void addMacroblocksAroundChipArea(ophidian::design::Design & design, std::vector<ophidian::circuit::Cell> & blockCells) {
    auto chipOrigin = design.floorplan().chipOrigin().toPoint();
    auto chipUpperCorner = design.floorplan().chipUpperRightCorner().toPoint();

    auto box1 = ophidian::geometry::Box(ophidian::geometry::Point(chipOrigin.x() - chipUpperCorner.x(), chipOrigin.y() - chipUpperCorner.y()), ophidian::geometry::Point(chipOrigin.x(), 2*chipUpperCorner.y()));
    auto box2 = ophidian::geometry::Box(ophidian::geometry::Point(chipOrigin.x() - chipUpperCorner.x(), chipOrigin.y() - chipUpperCorner.y()), ophidian::geometry::Point(2*chipUpperCorner.x(), chipOrigin.y()));
    auto box3 = ophidian::geometry::Box(ophidian::geometry::Point(chipOrigin.x() - chipUpperCorner.x(), chipUpperCorner.y()), ophidian::geometry::Point(2*chipUpperCorner.x(), 2*chipUpperCorner.y()));
    auto box4 = ophidian::geometry::Box(ophidian::geometry::Point(chipUpperCorner.x(), chipOrigin.y() - chipUpperCorner.y()), ophidian::geometry::Point(2*chipUpperCorner.x(), 2*chipUpperCorner.y()));

    std::vector<ophidian::geometry::Box> boxes({box1, box2, box3, box4});
    unsigned index = 0;
    for (auto box : boxes) {
        auto cellName = "outside_block_" + boost::lexical_cast<std::string>(index++);
        auto stdCell = design.standardCells().add(ophidian::standard_cell::Cell(), cellName);
        auto circuitCell = design.netlist().add(ophidian::circuit::Cell(), cellName);

        design.library().geometry(stdCell, ophidian::util::MultiBox({box}));
        design.library().cellAlignment(stdCell, ophidian::placement::RowAlignment::NA);
        design.placement().placeCell(circuitCell, ophidian::util::Location(0, 0));
        design.placement().fixLocation(circuitCell, true);
        design.libraryMapping().cellStdCell(circuitCell, stdCell);
        blockCells.push_back(circuitCell);
    }
}

void moveCellsRandomly(ophidian::design::Design & design, const std::vector<ophidian::circuit::Cell> & chipCells, ophidian::geometry::Box chipArea, std::mt19937_64 & gen){
    std::uniform_int_distribution<> dis(-10000, 10000);

    for (auto cell : chipCells)
    {
        if (design.placement().isFixed(cell) == false) {
            auto cellBox = design.placementMapping().geometry(cell)[0];
            auto cellWidth = cellBox.max_corner().x() - cellBox.min_corner().x();
            auto cellHeight = cellBox.max_corner().y() - cellBox.min_corner().y();

            auto newX = design.placement().cellLocation(cell).toPoint().x()+dis(gen);
            newX = std::min(chipArea.max_corner().x() - cellWidth, std::max(chipArea.min_corner().x(), newX));
            auto newY = design.placement().cellLocation(cell).toPoint().y()+dis(gen);
            newY = std::min(chipArea.max_corner().y() - cellHeight, std::max(chipArea.min_corner().y(), newY));
            design.placement().placeCell(cell, ophidian::util::Location(newX,
                                                                           newY));
        }
    }

    ophidian::legalization::CellAlignment cellAlignment(design);
    cellAlignment.alignCellsToSitesAndRows();

    ophidian::legalization::PartitionedLegalization partitioned(design, chipArea, "");
    partitioned.removeMacroblocksOverlaps();
}

void generateCsvFromPartitionsOfOneCircuit(std::string circuitName, ophidian::design::Design & design) {
    std::string defPath = "kd_tree_partitions/";

    std::mt19937_64 gen(0);

    ophidian::util::Location chipOrigin = design.floorplan().chipOrigin();
    ophidian::util::Location chipUpperCorner = design.floorplan().chipUpperRightCorner();

    ophidian::geometry::Box chipArea(chipOrigin.toPoint(), chipUpperCorner.toPoint());

    ophidian::util::KDTreeBuilder<ophidian::circuit::Cell> kdtreeBuilder;

    ophidian::legalization::MultirowAbacus multirowAbacus(design.netlist(), design.floorplan(), design.placement(), design.placementMapping());

    ophidian::entity_system::Property<ophidian::circuit::Cell, ophidian::util::Location> initialLocations(design.netlist().makeProperty<ophidian::util::Location>(ophidian::circuit::Cell()));

    ophidian::legalization::FenceRegionIsolation fenceIsolation(design);
    fenceIsolation.isolateAllFenceCells();

    ophidian::legalization::ICCAD2017SolutionQuality solutionQuality(design, design, circuitName);

    std::vector<ophidian::circuit::Cell> chipCells;
    chipCells.reserve(design.netlist().size(ophidian::circuit::Cell()));
    for(auto cellIt = design.netlist().begin(ophidian::circuit::Cell()); cellIt != design.netlist().end(ophidian::circuit::Cell()); ++cellIt)
    {
        if(!design.placement().cellHasFence(*cellIt))
        {
            chipCells.push_back(*cellIt);
        }
    }
    chipCells.shrink_to_fit();

    unsigned expectedNumberOfSamples = 1024;

    for (unsigned i = 1; i <= 9; i++) {
        unsigned count = 0;
        std::string csvName = "full_data_kdtree_abacus/data_" + circuitName + "_" + boost::lexical_cast<std::string>(i) + ".csv";
//        std::string csvName = "histogram_data_kdtree_abacus/histogram_normalized_" + circuitName + "_" + boost::lexical_cast<std::string>(i) + ".csv";
        ophidian::legalization::WriteCsv writeCsv(design, csvName);

        unsigned numberOfPartitions = std::pow(2, i);
        unsigned numberOfIterations = expectedNumberOfSamples / numberOfPartitions;

        std::cout << "i " << i << std::endl;
        std::cout << "number of iterations " << numberOfIterations << std::endl;
        for (unsigned iteration = 0; iteration < numberOfIterations; iteration++) {
            std::cout << "iteration " << iteration << std::endl;

            moveCellsRandomly(design, chipCells, chipArea, gen);

            auto oldHpwl = solutionQuality.hpwl(design);

            for (auto cell : chipCells) {
                initialLocations[cell] = design.placement().cellLocation(cell);
            }

            for(auto cell : chipCells) {
                kdtreeBuilder.add(design.placement().cellLocation(cell).toPoint(), cell);
            }

            ophidian::util::KDTreePartitioning<ophidian::circuit::Cell> kdTreePartitioning(kdtreeBuilder, chipArea, i);

            auto partitions = kdTreePartitioning.leafNodes();

            for (auto & partition : partitions) {
//                std::cout << "count " << count << std::endl;

                auto area = partition->range;
                std::vector<ophidian::circuit::Cell> cells;
                for(auto cell : partition->partition) {
                    cells.push_back(cell);
                }

                ophidian::util::MultiBox legalizationArea({area});

                auto abacusResult = multirowAbacus.legalizePlacement(cells, legalizationArea);
                auto boolResult = abacusResult != ophidian::util::micrometer_t(std::numeric_limits<double>::max());

                double totalDisplacement = 0;
                double maxDisplacement = 0;
                unsigned numberOfMovableCells = 0;
                for (auto cell : cells) {
                    if (!design.placement().isFixed(cell)) {
                        auto currentLocation = design.placement().cellLocation(cell).toPoint();
                        auto initialLocation = initialLocations[cell].toPoint();
                        auto cellDisplacement = std::abs(currentLocation.x() - initialLocation.x()) + std::abs(currentLocation.y() - initialLocation.y());
                        totalDisplacement += cellDisplacement;
                        numberOfMovableCells++;

                        maxDisplacement = std::max(maxDisplacement, cellDisplacement);
                    }
                }
                double avgDisplacement = totalDisplacement / numberOfMovableCells;

                auto boxArea = boost::geometry::area(area);

                if (boxArea > 0) {
                    auto newHpwl = solutionQuality.hpwl(design);
                    auto hpwlReduction = oldHpwl - newHpwl;

                    writeCsv.writeCsvWithEverything(area, cells, ophidian::util::micrometer_t(totalDisplacement), ophidian::util::micrometer_t(avgDisplacement), ophidian::util::micrometer_t(maxDisplacement), hpwlReduction, boolResult);
                }

                count++;
            }

            for (auto cell : chipCells) {
                auto initialLocation = initialLocations[cell];
                design.placement().placeCell(cell, initialLocation);
            }
        }

    }

}

void generateImagesFromPartitionsOfOneCircuit(std::string circuitName, ophidian::design::Design & design) {
    std::string resultFileName = "kdtree_images_with_macroblocks_around/" + circuitName + "/results.csv";
    std::ofstream resultFileStream;
    resultFileStream.open(resultFileName);
    resultFileStream << "file_name,avg_displacement,total_displacement,max_displacement,hpwl,legalization_result" << std::endl;

    std::mt19937_64 gen(0);

    ophidian::util::Location chipOrigin = design.floorplan().chipOrigin();
    ophidian::util::Location chipUpperCorner = design.floorplan().chipUpperRightCorner();

    ophidian::geometry::Box chipArea(chipOrigin.toPoint(), chipUpperCorner.toPoint());

    ophidian::util::KDTreeBuilder<ophidian::circuit::Cell> kdtreeBuilder;

    ophidian::legalization::MultirowAbacus multirowAbacus(design.netlist(), design.floorplan(), design.placement(), design.placementMapping());

    ophidian::entity_system::Property<ophidian::circuit::Cell, ophidian::util::Location> initialLocations(design.netlist().makeProperty<ophidian::util::Location>(ophidian::circuit::Cell()));

    ophidian::legalization::FenceRegionIsolation fenceIsolation(design);
    fenceIsolation.isolateAllFenceCells();

    ophidian::legalization::ICCAD2017SolutionQuality solutionQuality(design, design, circuitName);

    std::vector<ophidian::circuit::Cell> chipCells;
    std::vector<ophidian::circuit::Cell> fixedCells;
    chipCells.reserve(design.netlist().size(ophidian::circuit::Cell()));
    for(auto cellIt = design.netlist().begin(ophidian::circuit::Cell()); cellIt != design.netlist().end(ophidian::circuit::Cell()); ++cellIt)
    {
        if(!design.placement().cellHasFence(*cellIt))
        {
            chipCells.push_back(*cellIt);
        }

        if (design.placement().isFixed(*cellIt)) {
            fixedCells.push_back(*cellIt);
        }
    }
    chipCells.shrink_to_fit();

    std::vector<ophidian::circuit::Cell> blockCells;
    addMacroblocksAroundChipArea(design, blockCells);

    unsigned expectedNumberOfSamples = 1024;

    unsigned count = 0;
    for (unsigned i = 1; i <= 9; i++) {
        ophidian::legalization::WriteImage writeImage(design);

        unsigned numberOfPartitions = std::pow(2, i);
        unsigned numberOfIterations = expectedNumberOfSamples / numberOfPartitions;

        std::cout << "i " << i << std::endl;
        std::cout << "number of iterations " << numberOfIterations << std::endl;
        for (unsigned iteration = 0; iteration < numberOfIterations; iteration++) {
            std::cout << "iteration " << iteration << std::endl;

            moveCellsRandomly(design, chipCells, chipArea, gen);
            for (auto cell : chipCells) {
                initialLocations[cell] = design.placement().cellLocation(cell);
            }

            auto oldHpwl = solutionQuality.hpwl(design);

            for(auto cell : chipCells) {
                kdtreeBuilder.add(design.placement().cellLocation(cell).toPoint(), cell);
            }

            ophidian::util::KDTreePartitioning<ophidian::circuit::Cell> kdTreePartitioning(kdtreeBuilder, chipArea, i);

            auto partitions = kdTreePartitioning.leafNodes();

            for (auto & partition : partitions) {
//                std::cout << "count " << count << std::endl;

                auto area = partition->range;
                std::vector<ophidian::circuit::Cell> cells;
                for(auto cell : partition->partition) {
                    cells.push_back(cell);
                }

                ophidian::util::MultiBox legalizationArea({area});

                auto abacusResult = multirowAbacus.legalizePlacement(cells, legalizationArea);
                auto boolResult = abacusResult != ophidian::util::micrometer_t(std::numeric_limits<double>::max());

                double totalDisplacement = 0;
                double maxDisplacement = 0;
                unsigned numberOfMovableCells = 0;
                for (auto cell : cells) {
                    if (!design.placement().isFixed(cell)) {
                        auto currentLocation = design.placement().cellLocation(cell).toPoint();
                        auto initialLocation = initialLocations[cell].toPoint();
                        auto cellDisplacement = std::abs(currentLocation.x() - initialLocation.x()) + std::abs(currentLocation.y() - initialLocation.y());
                        totalDisplacement += cellDisplacement;
                        numberOfMovableCells++;

                        maxDisplacement = std::max(maxDisplacement, cellDisplacement);
                    }
                }
                double avgDisplacement = totalDisplacement / numberOfMovableCells;

                auto newHpwl = solutionQuality.hpwl(design);
                auto hpwlReduction = oldHpwl - newHpwl;

                auto boxArea = boost::geometry::area(area);

                if (boxArea > 0) {
                    for (auto blockCell : blockCells) {
                        cells.push_back(blockCell);
                    }
                    for (auto fixedCell : fixedCells) {
                        cells.push_back(fixedCell);
                    }

                    std::string fileName = "kdtree_images_with_macroblocks_around/" + circuitName + "/" + boost::lexical_cast<std::string>(count) + ".png";
                    std::cout << fileName << std::endl;
                    writeImage.writeImageOfLegalizationArea(area, cells, ophidian::util::micrometer_t(avgDisplacement), fileName);

                    resultFileStream << fileName << "," << avgDisplacement << "," << totalDisplacement << "," << maxDisplacement << "," << hpwlReduction << "," << boolResult << std::endl;
                }

                count++;
            }

            for (auto cell : chipCells) {
                auto initialLocation = initialLocations[cell];
                design.placement().placeCell(cell, initialLocation);
            }
        }

    }

}

TEST_CASE("generate csv from partitions", "[iccad2017][generate_csv_from_partitions]")
{
//    std::cout << "running for all circuits " << std::endl;

    std::vector<std::string> circuitNames = {
        "des_perf_b_md1",
        "des_perf_b_md2",
        "edit_dist_1_md1",
        "edit_dist_a_md2",
        "fft_2_md2",
        "fft_a_md2",
        "fft_a_md3",
        "pci_bridge32_a_md1",
        "des_perf_1",
        "des_perf_a_md1",
        "des_perf_a_md2",
        "edit_dist_a_md3",
        "pci_bridge32_a_md2",
        "pci_bridge32_b_md1",
        "pci_bridge32_b_md2",
        "pci_bridge32_b_md3"
    };

    for (auto circuitName : circuitNames)
    {
        std::cout << "running circuit: " << circuitName << std::endl;

        ophidian::designBuilder::ICCAD2017ContestDesignBuilder ICCAD2017DesignBuilder("./input_files/benchmarks2017/" + circuitName + "/cells_modified.lef",
                                                                                      "./input_files/benchmarks2017/" + circuitName + "/tech.lef",
                                                                                       "./input_files/benchmarks2017/" + circuitName + "/placed.def");
        ICCAD2017DesignBuilder.build();

        ophidian::design::Design & design = ICCAD2017DesignBuilder.design();

        generateCsvFromPartitionsOfOneCircuit(circuitName, design);
    }

    std::cout << std::endl;
}

TEST_CASE("generate csv from partitions from modified ispd 2015 circuits", "[ispd2015][generate_csv_from_partitions]")
{
//    std::cout << "running for all circuits " << std::endl;

    std::vector<std::string> circuitNames = {
//        "mgc_des_perf_1",
//        "mgc_des_perf_a",
        "mgc_des_perf_b",
        "mgc_edit_dist_a",
        "mgc_fft_1",
        "mgc_fft_2",
        "mgc_fft_a",
        "mgc_fft_b",
        "mgc_matrix_mult_1",
        "mgc_matrix_mult_a",
        "mgc_matrix_mult_b",
        "mgc_pci_bridge32_a",
        "mgc_pci_bridge32_b",
        "mgc_superblue11_a",
        "mgc_superblue12",
        "mgc_superblue16_a"
    };

    for (auto circuitName : circuitNames)
    {
        std::cout << "running circuit: " << circuitName << std::endl;

        ophidian::designBuilder::ModifiedISPD2015ContestDesignBuilder designBuilder("./input_files/benchmarks_ispd2015/" + circuitName + "/cells.lef",
                                                                                      "./input_files/benchmarks_ispd2015/" + circuitName + "/tech.lef",
                                                                                       "./input_files/benchmarks_ispd2015/" + circuitName + "/gp_" + circuitName + ".def",
                                                                                      "./input_files/benchmarks_ispd2015/" + circuitName + "/" + circuitName + ".def.size");
        designBuilder.build();

        ophidian::design::Design & design = designBuilder.design();

        generateCsvFromPartitionsOfOneCircuit(circuitName, design);
    }

    std::cout << std::endl;
}

TEST_CASE("generate csv from partitions of iccad 2015 circuits", "[iccad2015][generate_csv_from_partitions]")
{
//    std::cout << "running for all circuits " << std::endl;

    std::vector<std::string> circuitNames = {
        "superblue18",
        "superblue4",
        "superblue10",
        "superblue7",
        "superblue1",
        "superblue16",
        "superblue3",
        "superblue5"
    };

    for (auto circuitName : circuitNames)
    {
        std::cout << "running circuit: " << circuitName << std::endl;

        ophidian::designBuilder::ICCAD2015ContestDesignBuilder ICCAD2015DesignBuilder("./input_files/"+circuitName+"/"+circuitName+".lef",
                                                                                      "./input_files/"+circuitName+"/"+circuitName+".def",
//                                                                                      "./input_files/SBCCI2018_benchmarks/MODIFIED"+circuitName+".def",
                                                                                      "./input_files/"+circuitName+"/"+circuitName+".v");
        ICCAD2015DesignBuilder.build();

        ophidian::design::Design & design = ICCAD2015DesignBuilder.design();

        ophidian::legalization::iccad2017Legalization legalization(design, "");
        legalization.isolateFloorplan();

        generateCsvFromPartitionsOfOneCircuit(circuitName, design);

        legalization.restoreFloorplan();
    }

    std::cout << std::endl;
}

TEST_CASE("generate images from partitions", "[iccad2017][generate_image_from_partitions]")
{
//    std::cout << "running for all circuits " << std::endl;

    std::vector<std::string> circuitNames = {
        "des_perf_b_md1",
        "des_perf_b_md2",
        "edit_dist_1_md1",
        "edit_dist_a_md2",
        "fft_2_md2",
        "fft_a_md2",
        "fft_a_md3",
        "pci_bridge32_a_md1",
        "des_perf_1",
        "des_perf_a_md1",
        "des_perf_a_md2",
        "edit_dist_a_md3",
        "pci_bridge32_a_md2",
        "pci_bridge32_b_md1",
        "pci_bridge32_b_md2",
        "pci_bridge32_b_md3"
    };

    for (auto circuitName : circuitNames)
    {
        std::cout << "running circuit: " << circuitName << std::endl;

        ophidian::designBuilder::ICCAD2017ContestDesignBuilder ICCAD2017DesignBuilder("./input_files/benchmarks2017/" + circuitName + "/cells_modified.lef",
                                                                                      "./input_files/benchmarks2017/" + circuitName + "/tech.lef",
                                                                                       "./input_files/benchmarks2017/" + circuitName + "/placed.def");
        ICCAD2017DesignBuilder.build();

        ophidian::design::Design & design = ICCAD2017DesignBuilder.design();

        generateImagesFromPartitionsOfOneCircuit(circuitName, design);
    }

    std::cout << std::endl;
}
