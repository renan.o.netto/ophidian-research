#include <catch.hpp>

#include <ophidian/legalization/TransitiveClosureGraph.h>

#include "legalizationfixture.h"

TEST_CASE_METHOD(TransitiveClosureGraphCircuitFixture, "TCG for circuit with 5 cells", "[legalization][transitive_closure_graph]") {
    ophidian::legalization::TransitiveClosureGraph tcg(design_);

    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box area(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());
    tcg.buildConstraintGraph(cells, area);

    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    auto cell3 = design_.netlist().find(ophidian::circuit::Cell(), "cell3");
    auto cell4 = design_.netlist().find(ophidian::circuit::Cell(), "cell4");
    auto cell5 = design_.netlist().find(ophidian::circuit::Cell(), "cell5");

    SECTION("Constraints of cell1") {
        REQUIRE(tcg.hasHorizontalEdge(cell1, cell2));
        REQUIRE(!tcg.hasHorizontalEdge(cell2, cell1));
        REQUIRE(!tcg.hasVerticalEdge(cell1, cell2));
        REQUIRE(!tcg.hasVerticalEdge(cell2, cell1));

        REQUIRE(!tcg.hasHorizontalEdge(cell1, cell3));
        REQUIRE(!tcg.hasHorizontalEdge(cell3, cell1));
        REQUIRE(tcg.hasVerticalEdge(cell1, cell3));
        REQUIRE(!tcg.hasVerticalEdge(cell3, cell1));

        REQUIRE(!tcg.hasHorizontalEdge(cell1, cell4));
        REQUIRE(!tcg.hasHorizontalEdge(cell4, cell1));
        REQUIRE(tcg.hasVerticalEdge(cell1, cell4));
        REQUIRE(!tcg.hasVerticalEdge(cell4, cell1));

        REQUIRE(!tcg.hasHorizontalEdge(cell1, cell5));
        REQUIRE(!tcg.hasHorizontalEdge(cell5, cell1));
        REQUIRE(tcg.hasVerticalEdge(cell1, cell5));
        REQUIRE(!tcg.hasVerticalEdge(cell5, cell1));
    }

    SECTION("Constraints of cell2") {
        REQUIRE(!tcg.hasHorizontalEdge(cell2, cell3));
        REQUIRE(!tcg.hasHorizontalEdge(cell3, cell2));
        REQUIRE(tcg.hasVerticalEdge(cell2, cell3));
        REQUIRE(!tcg.hasVerticalEdge(cell3, cell2));

        REQUIRE(!tcg.hasHorizontalEdge(cell2, cell4));
        REQUIRE(!tcg.hasHorizontalEdge(cell4, cell2));
        REQUIRE(tcg.hasVerticalEdge(cell2, cell4));
        REQUIRE(!tcg.hasVerticalEdge(cell4, cell2));

        REQUIRE(!tcg.hasHorizontalEdge(cell2, cell5));
        REQUIRE(!tcg.hasHorizontalEdge(cell5, cell2));
        REQUIRE(tcg.hasVerticalEdge(cell2, cell5));
        REQUIRE(!tcg.hasVerticalEdge(cell5, cell2));
    }

    SECTION("Constraints of cell3") {
        REQUIRE(tcg.hasHorizontalEdge(cell3, cell4));
        REQUIRE(!tcg.hasHorizontalEdge(cell4, cell3));
        REQUIRE(!tcg.hasVerticalEdge(cell3, cell4));
        REQUIRE(!tcg.hasVerticalEdge(cell4, cell3));

        REQUIRE(!tcg.hasHorizontalEdge(cell3, cell5));
        REQUIRE(!tcg.hasHorizontalEdge(cell5, cell3));
        REQUIRE(tcg.hasVerticalEdge(cell3, cell5));
        REQUIRE(!tcg.hasVerticalEdge(cell5, cell3));
    }

    SECTION("Constraints of cell4") {
        REQUIRE(!tcg.hasHorizontalEdge(cell4, cell5));
        REQUIRE(!tcg.hasHorizontalEdge(cell5, cell4));
        REQUIRE(tcg.hasVerticalEdge(cell4, cell5));
        REQUIRE(!tcg.hasVerticalEdge(cell5, cell4));
    }
}
