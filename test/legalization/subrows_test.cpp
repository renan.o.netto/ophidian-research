#include <catch.hpp>

#include "legalizationfixture.h"

#include <ophidian/legalization/Subrows.h>

TEST_CASE_METHOD(LargerLegalCircuitFixture, "Splitting subrows in two pieces", "[legalization][subrows]") {
    ophidian::legalization::Subrows subrows(design_);

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::util::MultiBox multiBox({chipArea});

    subrows.createSubrows(cells, multiBox);
    subrows.splitSubrows(ophidian::util::micrometer_t(25));

    std::vector<ophidian::util::Location> expectedSubrowOrigins = {
        {0, 0},
        {0, 10},
        {0, 20},
        {0, 30},
        {25, 0},
        {25, 10},
        {25, 20},
        {25, 30},
    };
    std::vector<ophidian::util::Location> expectedSubrowUppers = {
        {25, 10},
        {25, 20},
        {25, 30},
        {25, 40},
        {50, 10},
        {50, 20},
        {50, 30},
        {50, 40},
    };

    std::vector<ophidian::util::Location> subrowOrigins;
    std::vector<ophidian::util::Location> subrowUppers;
    for (auto subrow : subrows.range(ophidian::legalization::Subrow())) {
        subrowOrigins.push_back(subrows.origin(subrow));
        subrowUppers.push_back(subrows.upperCorner(subrow));
    }

    REQUIRE(expectedSubrowOrigins.size() == subrowOrigins.size());
    REQUIRE(expectedSubrowUppers.size() == subrowUppers.size());
    REQUIRE(std::is_permutation(expectedSubrowOrigins.begin(), expectedSubrowOrigins.end(), subrowOrigins.begin()));
    REQUIRE(std::is_permutation(expectedSubrowUppers.begin(), expectedSubrowUppers.end(), subrowUppers.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "Splitting subrows in three pieces", "[legalization][subrows]") {
    ophidian::legalization::Subrows subrows(design_);

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::util::MultiBox multiBox({chipArea});

    subrows.createSubrows(cells, multiBox);
    subrows.splitSubrows(ophidian::util::micrometer_t(20));

    std::vector<ophidian::util::Location> expectedSubrowOrigins = {
        {0, 0},
        {0, 10},
        {0, 20},
        {0, 30},
        {20, 0},
        {20, 10},
        {20, 20},
        {20, 30},
        {40, 0},
        {40, 10},
        {40, 20},
        {40, 30},
    };
    std::vector<ophidian::util::Location> expectedSubrowUppers = {
        {20, 10},
        {20, 20},
        {20, 30},
        {20, 40},
        {40, 10},
        {40, 20},
        {40, 30},
        {40, 40},
        {50, 10},
        {50, 20},
        {50, 30},
        {50, 40},
    };

    std::vector<ophidian::util::Location> subrowOrigins;
    std::vector<ophidian::util::Location> subrowUppers;
    for (auto subrow : subrows.range(ophidian::legalization::Subrow())) {
        subrowOrigins.push_back(subrows.origin(subrow));
        subrowUppers.push_back(subrows.upperCorner(subrow));
    }

    REQUIRE(expectedSubrowOrigins.size() == subrowOrigins.size());
    REQUIRE(expectedSubrowUppers.size() == subrowUppers.size());
    REQUIRE(std::is_permutation(expectedSubrowOrigins.begin(), expectedSubrowOrigins.end(), subrowOrigins.begin()));
    REQUIRE(std::is_permutation(expectedSubrowUppers.begin(), expectedSubrowUppers.end(), subrowUppers.begin()));
}
