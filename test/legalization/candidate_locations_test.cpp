#include <catch.hpp>

#include "legalizationfixture.h"

#include <ophidian/legalization/CandidateLocations.h>

TEST_CASE_METHOD(LargerLegalCircuitFixture, "test of two candidate locations around cell initial location", "[candidate_locations]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    std::vector<ophidian::util::Location> candidateLocationsVector;
    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    ophidian::util::Location targetLocation(10, 20);

    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocations(cell, targetLocation, 2, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(10, 10),
        ophidian::util::Location(10, 20),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "test of four candidate locations around cell initial location", "[candidate_locations]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    std::vector<ophidian::util::Location> candidateLocationsVector;
    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    ophidian::util::Location targetLocation(10, 20);

    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocations(cell, targetLocation, 4, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(10, 0),
        ophidian::util::Location(10, 10),
        ophidian::util::Location(10, 20),
        ophidian::util::Location(10, 30),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "test of two candidate locations around other location", "[candidate_locations]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    std::vector<ophidian::util::Location> candidateLocationsVector;
    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    ophidian::util::Location targetLocation(20, 0);

    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocations(cell, targetLocation, 2, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(20, 0),
        ophidian::util::Location(20, 10),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "test of four candidate locations around other location", "[candidate_locations]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    std::vector<ophidian::util::Location> candidateLocationsVector;
    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    ophidian::util::Location targetLocation(20, 0);

    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocations(cell, targetLocation, 4, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(20, 0),
        ophidian::util::Location(20, 10),
        ophidian::util::Location(20, 20),
        ophidian::util::Location(20, 30),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "test of grid candidate locations", "[grid_candidates]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    ophidian::util::Location targetLocation(42, 0);

    std::vector<ophidian::util::Location> candidateLocationsVector;
    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocationsGrid(cell, targetLocation, 5, 1, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(35,30),
        ophidian::util::Location(40,30),
        ophidian::util::Location(35,10),
        ophidian::util::Location(40,10),
        ophidian::util::Location(35,20),
        ophidian::util::Location(40,20),
        ophidian::util::Location(35,0),
        ophidian::util::Location(40,0),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "search for invalid locations", "[grid_candidates]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell1");

    std::vector<ophidian::util::Location> candidateLocationsVector;
    ophidian::legalization::CandidateLocations candidateLocations(design_);

    auto siteWidth = design_.floorplan().siteUpperRightCorner(*design_.floorplan().sitesRange().begin()).toPoint().x();
    auto rowHeight = design_.floorplan().siteUpperRightCorner(*design_.floorplan().sitesRange().begin()).toPoint().y();

    //inside points
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(47, 3), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(2, 2), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(2, 48), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(48, 48), 5, 2, chipArea, subrows, candidateLocationsVector);

    //over the boundaries
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 0), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 50), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 50), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 0), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(25, 0), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(25, 50), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 25), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 25), 5, 2, chipArea, subrows, candidateLocationsVector);

    //outside points
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(-4, -4), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(-4, 53), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(54, 53), 5, 2, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(54, -3), 5, 2, chipArea, subrows, candidateLocationsVector);

    for(auto location : candidateLocationsVector){
        REQUIRE((std::fmod(location.toPoint().x(), siteWidth) == 0 && std::fmod(location.toPoint().y(), rowHeight) == 0));
        REQUIRE((location.toPoint().x() >= chipArea.min_corner().x() && location.toPoint().x() <= chipArea.max_corner().x()));
        REQUIRE((location.toPoint().y() >= chipArea.min_corner().y() && location.toPoint().y() <= chipArea.max_corner().y()));
    }
}

TEST_CASE_METHOD(MultirowAbacusFixture, "test of grid candidate locations for multirow cell", "[grid_candidates]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell6");
    ophidian::util::Location targetLocation(1, 47);

    std::vector<ophidian::util::Location> candidateLocationsVector;
    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocationsGrid(cell, targetLocation, 3, 2, chipArea, subrows, candidateLocationsVector);

    std::vector<ophidian::util::Location> expectedResults = {
        ophidian::util::Location(0,0),
        ophidian::util::Location(1,0),
        ophidian::util::Location(2,0),
        ophidian::util::Location(3,0),
        ophidian::util::Location(0,20),
        ophidian::util::Location(1,20),
        ophidian::util::Location(2,20),
        ophidian::util::Location(3,20),
    };

    REQUIRE(candidateLocationsVector.size() == expectedResults.size());
    REQUIRE(std::is_permutation(expectedResults.begin(), expectedResults.end(), candidateLocationsVector.begin()));
}

TEST_CASE_METHOD(MultirowAbacusFixture, "search for invalid locations for multirow cell", "[grid_candidates]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::Subrows subrows(design_);
    subrows.createSubrows(cells, ophidian::util::MultiBox({chipArea}));

    auto cell = design_.netlist().find(ophidian::circuit::Cell(), "cell6");
    ophidian::util::Location targetLocation(1, 47);

    std::vector<ophidian::util::Location> candidateLocationsVector;
    ophidian::legalization::CandidateLocations candidateLocations(design_);
    candidateLocations.findCandidateLocationsGrid(cell, targetLocation, 3, 2, chipArea, subrows, candidateLocationsVector);

    auto siteWidth = design_.floorplan().siteUpperRightCorner(*design_.floorplan().sitesRange().begin()).toPoint().x();
    auto rowHeight = design_.floorplan().siteUpperRightCorner(*design_.floorplan().sitesRange().begin()).toPoint().y();

    //inside points
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(47, 3), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(2, 2), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(2, 48), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(48, 48), 3, 5, chipArea, subrows, candidateLocationsVector);

    //over the boundaries
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 0), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 50), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 50), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 0), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(25, 0), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(25, 50), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(50, 25), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(0, 25), 3, 5, chipArea, subrows, candidateLocationsVector);

    //outside points
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(-4, -4), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(-4, 53), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(54, 53), 3, 5, chipArea, subrows, candidateLocationsVector);
    candidateLocations.findCandidateLocationsGrid(cell, ophidian::util::Location(54, -3), 3, 5, chipArea, subrows, candidateLocationsVector);

    for(auto location : candidateLocationsVector){
        REQUIRE((std::fmod(location.toPoint().x(), siteWidth) == 0 && std::fmod(location.toPoint().y(), rowHeight) == 0 && std::fmod(location.toPoint().y(), 2) == 0));
        REQUIRE((location.toPoint().x() >= chipArea.min_corner().x() && location.toPoint().x() <= chipArea.max_corner().x()));
        REQUIRE((location.toPoint().y() >= chipArea.min_corner().y() && location.toPoint().y() <= chipArea.max_corner().y()));
    }
}
