#include <catch.hpp>

#include "legalizationfixture.h"

#include <ophidian/legalization/LegalizationBox.h>

TEST_CASE_METHOD(LegalizationBoxCircuitFixture, "legalization box completely inside the legalization area", "[legalization_box]") {
    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    auto cell3 = design_.netlist().find(ophidian::circuit::Cell(), "cell3");
    auto cell4 = design_.netlist().find(ophidian::circuit::Cell(), "cell4");

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::util::Location location(10, 20);
    double boxSize = 10;

    ophidian::legalization::LegalizationBox legalizationBox(design_);
    auto box = legalizationBox.createLegalizationBox(cell1, location, chipArea, boxSize);

    REQUIRE(boost::geometry::equals(box.min_corner(), ophidian::geometry::Point(0, 10)));
    REQUIRE(boost::geometry::equals(box.max_corner(), ophidian::geometry::Point(20, 30)));

    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    std::vector<ophidian::circuit::Cell> cellsInsideBox;
    std::vector<ophidian::circuit::Cell> cellsOnTheEdge;
    legalizationBox.separateBoxCells(box, cell1, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);

    std::vector<ophidian::circuit::Cell> expectedInside = {
        cell1
    };
    std::vector<ophidian::circuit::Cell> expectedOnTheEdge;
    REQUIRE(expectedInside.size() == cellsInsideBox.size());
    REQUIRE(std::is_permutation(expectedInside.begin(), expectedInside.end(), cellsInsideBox.begin()));
    REQUIRE(expectedOnTheEdge.size() == cellsOnTheEdge.size());
    REQUIRE(std::is_permutation(expectedOnTheEdge.begin(), expectedOnTheEdge.end(), cellsOnTheEdge.begin()));
}

TEST_CASE_METHOD(LegalizationBoxCircuitFixture, "legalization box smaller than the cell dimensions", "[legalization_box]") {
    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    auto cell3 = design_.netlist().find(ophidian::circuit::Cell(), "cell3");
    auto cell4 = design_.netlist().find(ophidian::circuit::Cell(), "cell4");

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::util::Location location(10, 20);
    double boxSize = 4;

    ophidian::legalization::LegalizationBox legalizationBox(design_);
    auto box = legalizationBox.createLegalizationBox(cell1, location, chipArea, boxSize);

    REQUIRE(boost::geometry::equals(box.min_corner(), ophidian::geometry::Point(5, 15)));
    REQUIRE(boost::geometry::equals(box.max_corner(), ophidian::geometry::Point(15, 25)));

    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    std::vector<ophidian::circuit::Cell> cellsInsideBox;
    std::vector<ophidian::circuit::Cell> cellsOnTheEdge;
    legalizationBox.separateBoxCells(box, cell1, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);

    std::vector<ophidian::circuit::Cell> expectedInside = {
        cell1
    };
    std::vector<ophidian::circuit::Cell> expectedOnTheEdge;
    REQUIRE(expectedInside.size() == cellsInsideBox.size());
    REQUIRE(std::is_permutation(expectedInside.begin(), expectedInside.end(), cellsInsideBox.begin()));
    REQUIRE(expectedOnTheEdge.size() == cellsOnTheEdge.size());
    REQUIRE(std::is_permutation(expectedOnTheEdge.begin(), expectedOnTheEdge.end(), cellsOnTheEdge.begin()));
}

TEST_CASE_METHOD(LegalizationBoxCircuitFixture, "legalization box that does not fit inside the legalization area", "[legalization_box]") {
    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    auto cell3 = design_.netlist().find(ophidian::circuit::Cell(), "cell3");
    auto cell4 = design_.netlist().find(ophidian::circuit::Cell(), "cell4");

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::util::Location location(10, 20);
    double boxSize = 15;

    ophidian::legalization::LegalizationBox legalizationBox(design_);
    auto box = legalizationBox.createLegalizationBox(cell1, location, chipArea, boxSize);

    REQUIRE(boost::geometry::equals(box.min_corner(), ophidian::geometry::Point(0, 5)));
    REQUIRE(boost::geometry::equals(box.max_corner(), ophidian::geometry::Point(25, 35)));

    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    std::vector<ophidian::circuit::Cell> cellsInsideBox;
    std::vector<ophidian::circuit::Cell> cellsOnTheEdge;
    legalizationBox.separateBoxCells(box, cell1, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);

    std::vector<ophidian::circuit::Cell> expectedInside = {
        cell1
    };
    std::vector<ophidian::circuit::Cell> expectedOnTheEdge = {
        cell2
    };
    REQUIRE(expectedInside.size() == cellsInsideBox.size());
    REQUIRE(std::is_permutation(expectedInside.begin(), expectedInside.end(), cellsInsideBox.begin()));
    REQUIRE(expectedOnTheEdge.size() == cellsOnTheEdge.size());
    REQUIRE(std::is_permutation(expectedOnTheEdge.begin(), expectedOnTheEdge.end(), cellsOnTheEdge.begin()));
}

TEST_CASE_METHOD(LegalizationBoxCircuitFixture, "legalization box covering the whole legalization area", "[legalization_box]") {
    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    auto cell3 = design_.netlist().find(ophidian::circuit::Cell(), "cell3");
    auto cell4 = design_.netlist().find(ophidian::circuit::Cell(), "cell4");

    ophidian::geometry::Box chipArea(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::util::Location location(10, 20);
    double boxSize = 100;

    ophidian::legalization::LegalizationBox legalizationBox(design_);
    auto box = legalizationBox.createLegalizationBox(cell1, location, chipArea, boxSize);

    REQUIRE(boost::geometry::equals(box.min_corner(), ophidian::geometry::Point(0, 0)));
    REQUIRE(boost::geometry::equals(box.max_corner(), ophidian::geometry::Point(100, 100)));

    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));
    std::vector<ophidian::circuit::Cell> cellsInsideBox;
    std::vector<ophidian::circuit::Cell> cellsOnTheEdge;
    legalizationBox.separateBoxCells(box, cell1, cells, mCellsRtree, cellsInsideBox, cellsOnTheEdge);

    std::vector<ophidian::circuit::Cell> expectedInside = {
        cell1,
        cell2,
        cell3,
        cell4
    };
    std::vector<ophidian::circuit::Cell> expectedOnTheEdge;
    REQUIRE(expectedInside.size() == cellsInsideBox.size());
    REQUIRE(std::is_permutation(expectedInside.begin(), expectedInside.end(), cellsInsideBox.begin()));
    REQUIRE(expectedOnTheEdge.size() == cellsOnTheEdge.size());
    REQUIRE(std::is_permutation(expectedOnTheEdge.begin(), expectedOnTheEdge.end(), cellsOnTheEdge.begin()));
}
