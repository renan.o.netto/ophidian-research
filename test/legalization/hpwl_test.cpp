#include <catch.hpp>

#include <ophidian/design/Design.h>
#include <ophidian/design/DesignBuilder.h>

#include <ophidian/legalization/iccad2017SolutionQuality.h>

TEST_CASE("HPWL of simple", "[hpwl]") {
    ophidian::designBuilder::ICCAD2015ContestDesignBuilder ICCAD2015DesignBuilder("./input_files/simple/simple.lef",
                                                                                  "./input_files/simple/simple.def",
                                                                                  "./input_files/simple/simple.verilog");
    ICCAD2015DesignBuilder.build();

    ophidian::design::Design & design = ICCAD2015DesignBuilder.design();

    ophidian::legalization::ICCAD2017SolutionQuality solutionQuality(design, design, "simple");
    auto hpwl = solutionQuality.hpwl(design);
    std::cout << hpwl << std::endl;
//    auto inp1Hpwl = solutionQuality.hpwl(design, design.netlist().find(ophidian::circuit::Net(), "inp1"));

//    REQUIRE(inp1Hpwl == 1950);
}
