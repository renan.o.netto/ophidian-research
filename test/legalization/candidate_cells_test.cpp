#include <catch.hpp>

#include "legalizationfixture.h"

#include <ophidian/legalization/CandidateCells.h>

TEST_CASE_METHOD(LargerLegalCircuitFixture, "finding one candidate cell with greatest displacement", "[candidate_cells]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));

    ophidian::legalization::CandidateCells pickCandidateCells(design_);
    pickCandidateCells.setInitial(design_);

    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    design_.placement().placeCell(cell1, {15, 20});

    std::vector<ophidian::circuit::Cell> candidateCells;
    pickCandidateCells.pickCandidateCellsWithGreatestDisplacement(1, cells, candidateCells);

    REQUIRE(candidateCells.size() == 1);
    REQUIRE(candidateCells.at(0) == cell1);
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "finding two candidate cells with greatest displacement", "[candidate_cells]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));

    ophidian::legalization::CandidateCells pickCandidateCells(design_);
    pickCandidateCells.setInitial(design_);

    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    design_.placement().placeCell(cell1, {15, 20});
    design_.placement().placeCell(cell2, {24, 0});

    std::vector<ophidian::circuit::Cell> candidateCells;
    pickCandidateCells.pickCandidateCellsWithGreatestDisplacement(2, cells, candidateCells);

    REQUIRE(candidateCells.size() == 2);
    REQUIRE(candidateCells.at(0) == cell1);
    REQUIRE(candidateCells.at(1) == cell2);
}

TEST_CASE_METHOD(LargerLegalCircuitFixture, "finding two candidate cells with overlap", "[candidate_cells]") {
    std::vector<ophidian::circuit::Cell> cells(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell()));

    ophidian::legalization::CandidateCells pickCandidateCells(design_);
    pickCandidateCells.setInitial(design_);

    auto cell1 = design_.netlist().find(ophidian::circuit::Cell(), "cell1");
    auto cell2 = design_.netlist().find(ophidian::circuit::Cell(), "cell2");
    design_.placement().placeCell(cell1, {20, 0});

    std::vector<ophidian::circuit::Cell> candidateCells;
    pickCandidateCells.pickCandidateCellsWithOverlaps(cells, candidateCells);

    REQUIRE(candidateCells.size() == 1);
    REQUIRE(candidateCells.at(0) == cell2);
}
