#include <catch.hpp>
#include <ophidian/legalization/Tetris.h>
#include <ophidian/legalization/LegalizationCheck.h>
#include <ophidian/legalization/iccad2017Legalization.h>
#include "legalizationfixture.h"
#include <ophidian/design/DesignBuilder.h>

TEST_CASE_METHOD(LargerLegalCircuitFixture, "Tetris: simple tetris", "[Tetris]") {
    ophidian::legalization::Tetris tetris(design_);

    auto chipArea = ophidian::geometry::Box(design_.floorplan().chipOrigin().toPoint(), design_.floorplan().chipUpperRightCorner().toPoint());
    auto legalized = tetris.legalize(std::vector<ophidian::circuit::Cell>(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell())), chipArea);

    REQUIRE(ophidian::legalization::checkBoundaries(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist(), design_.fences()));
    REQUIRE(ophidian::legalization::checkAlignment(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist()));
    REQUIRE(ophidian::legalization::checkCellOverlaps(design_.placementMapping(), design_.netlist()));
}


TEST_CASE("Tetris: tetris", "[Tetris]") {
    std::string c = "superblue18";
    std::string lef = "./input_files/superblue18/superblue18.lef";
    std::string def = "./input_files/SBCCI2018_benchmarks/MODIFIEDsuperblue18.def";
    std::string verilog = "./input_files/superblue18/superblue18.v";
    ophidian::designBuilder::ICCAD2015ContestDesignBuilder ICCAD2015DesignBuilder(lef, def, verilog);
    ICCAD2015DesignBuilder.build();


    ophidian::design::Design & design_ = ICCAD2015DesignBuilder.design();
    ophidian::legalization::iccad2017Legalization iccad2017Legalization(design_);
    iccad2017Legalization.isolateFloorplan();

    ophidian::legalization::Tetris tetris(design_);

    ophidian::geometry::Point originCorner(std::numeric_limits<double>::max(), std::numeric_limits<double>::max());
    ophidian::geometry::Point upperCorner(0, 0);



    for(auto rowIt = design_.floorplan().rowsRange().begin(); rowIt != design_.floorplan().rowsRange().end(); rowIt++){
        auto rowOrigin = design_.floorplan().origin(*rowIt);
        auto rowSize = design_.floorplan().rowUpperRightCorner(*rowIt);
        auto rowUpperRightCorner = ophidian::geometry::Point(rowOrigin.toPoint().x() + rowSize.toPoint().x(), rowOrigin.toPoint().y() + rowSize.toPoint().y());
        originCorner.x(std::min(originCorner.x(), rowOrigin.toPoint().x()));
        originCorner.y(std::min(originCorner.y(), rowOrigin.toPoint().y()));

        upperCorner.x(std::max(upperCorner.x(), rowUpperRightCorner.x()));
        upperCorner.y(std::max(upperCorner.y(), rowUpperRightCorner.y()));
    }

    auto chipArea = ophidian::geometry::Box(originCorner, upperCorner);


    auto legalized = tetris.legalize(std::vector<ophidian::circuit::Cell>(design_.netlist().begin(ophidian::circuit::Cell()), design_.netlist().end(ophidian::circuit::Cell())), chipArea);

    iccad2017Legalization.restoreFloorplan();
    design_.writeDefFile("definho.def");

    REQUIRE(ophidian::legalization::checkBoundaries(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist(), design_.fences()));
    REQUIRE(ophidian::legalization::checkAlignment(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist()));
    REQUIRE(ophidian::legalization::checkCellOverlaps(design_.placementMapping(), design_.netlist()));
}

TEST_CASE("Tetris: test with random single row cells", "[Tetris]"){
    ophidian::util::Location chipOrigin(0,0);
    ophidian::util::Location chipUpperCorner(50,50);
    CircuitFixtureWithRandomCells circuit(chipOrigin, chipUpperCorner, 3);

    ophidian::geometry::Box chipArea(chipOrigin.toPoint(), chipUpperCorner.toPoint());

    ophidian::legalization::Tetris tetris(circuit.design_);

    auto legalized = tetris.legalize(std::vector<ophidian::circuit::Cell>(circuit.design_.netlist().begin(ophidian::circuit::Cell()), circuit.design_.netlist().end(ophidian::circuit::Cell())), chipArea);

//    for (auto cellIt = circuit.design_.netlist().begin(ophidian::circuit::Cell()); cellIt != circuit.design_.netlist().end(ophidian::circuit::Cell()); ++cellIt){
//        std::cout<<"("<<circuit.design_.placement().cellLocation(*cellIt).toPoint().x()<<","<<circuit.design_.placement().cellLocation(*cellIt).toPoint().y()<<")";
//    }
//    std::cout<<std::endl;

    auto & design_ = circuit.design_;

    REQUIRE(ophidian::legalization::checkBoundaries(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist(), design_.fences()));
    REQUIRE(ophidian::legalization::checkAlignment(design_.floorplan(), design_.placement(), design_.placementMapping(), design_.netlist()));
    REQUIRE(ophidian::legalization::checkCellOverlaps(design_.placementMapping(), design_.netlist()));
}
