#include <catch.hpp>
#include <ophidian/util/KDTree.h>

using namespace ophidian::util;
using Point = ophidian::geometry::Point;

class KDTreeFixture : public KDTree<std::string>{
public:
    KDTreeFixture(){
        reserve(points.size());
        for(auto & point : points)
            add(point.first, point.second);
    }
    const std::vector<std::pair<Point,std::string>> points = {{{1, 1.5}, "A"}, {{1.5, 3.5}, "B"}, {{2.5, 4.5}, "C"}, {{3, 2}, "D"}, {{3.5, 1.5}, "E"}, {{4.5, 2.5}, "F"}, {{5, 4.5}, "G"}};
};

TEST_CASE_METHOD(KDTreeFixture,"KDTree structure Test: ", "[kdtree]")
{
    build();

    REQUIRE(mNodes.size() == 7);

    REQUIRE(mRoot->data == "D");
    REQUIRE(mRoot->left->left->data == "A");
    REQUIRE(mRoot->right->right->data == "G");
    REQUIRE(mRoot->right->right->parent->data == "F");
    REQUIRE(mRoot->left->left->parent->data == "B");
}
