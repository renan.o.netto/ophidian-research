#include <catch.hpp>
#include <ophidian/util/KDTreePartitioning.h>

using namespace ophidian::util;
using Point = KDTreePartitioning<std::string>::Point;
using Range = KDTreePartitioning<std::string>::Range;

bool pointComparison(const Point & p1, const Point & p2){
    return p1.x() == p2.x() && p1.y() == p2.y();
}

bool rangeComparison(const Range & r1, const Range & r2){
    return pointComparison(r1.min_corner(), r2.min_corner()) && pointComparison(r1.max_corner(), r2.max_corner());
}

class BuilderFixture : public ophidian::util::KDTreeBuilder<std::string>{
public:
    BuilderFixture(){
        reserve(points.size());
        for(auto & point : points)
            add(point.first, point.second);
    }
    std::vector<std::pair<Point,std::string>> points = {{{1, 1.5}, "A"}, {{1.5, 3.5}, "B"}, {{2.5, 4.5}, "C"}, {{3, 2}, "D"}, {{3.5, 1.5}, "E"}, {{4.5, 2.5}, "F"}, {{5, 4.5}, "G"}};
};

class KDTreeFixture : public ophidian::util::KDTreePartitioning<std::string>{
public:
    KDTreeFixture(BuilderFixture & builder, const unsigned int maxDepth = std::numeric_limits<unsigned int>::max()):KDTreePartitioning(builder, maxDepth){
    }

    KDTreeFixture(BuilderFixture & builder, Range range, const unsigned int maxDepth = std::numeric_limits<unsigned int>::max()):KDTreePartitioning(builder, range, maxDepth){
    }

    Node * root(){
        return mRoot.get();
    }

    Node * left(Node * left_ptr){
        if(left_ptr->left.get())
            return left(left_ptr->left.get());
        else
            return left_ptr;
    }

    Node * right(Node * right_ptr){
        if(right_ptr->right.get())
            return right(right_ptr->right.get());
        else
            return right_ptr;
    }
};

TEST_CASE("Unlimited KDTreePartitioning Test: ", "[partitioning][kdtree]"){
    BuilderFixture builder;
    REQUIRE(builder.mElements.size() == 7);

    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)));
    REQUIRE(builder.mElements.size() == 0);

    auto root = kdtree.root();

    REQUIRE(root->parent == nullptr);
    REQUIRE(root->left->left->parent->parent == root);
    REQUIRE(root->right->right->parent->parent == root);

    REQUIRE(root->left->left->partition.front() == "A");
    REQUIRE(pointComparison(root->left->left->point, Point(1, 1.5)));
    REQUIRE(rangeComparison(root->left->left->range, Range(Point(0,0), Point(3,3.5))));

    REQUIRE(root->left->partition.front() == "B");
    REQUIRE(pointComparison(root->left->point, Point(1.5, 3.5)));
    REQUIRE(rangeComparison(root->left->range, Range(Point(0,0), Point(3,6))));

    REQUIRE(root->left->right->partition.front() == "C");
    REQUIRE(pointComparison(root->left->right->point, Point(2.5, 4.5)));
    REQUIRE(rangeComparison(root->left->right->range, Range(Point(0,3.5), Point(3,6))));

    REQUIRE(root->partition.front() == "D");
    REQUIRE(pointComparison(root->point, Point(3, 2)));
    REQUIRE(rangeComparison(root->range, Range(Point(0,0), Point(6,6))));

    REQUIRE(root->right->left->partition.front() == "E");
    REQUIRE(pointComparison(root->right->left->point, Point(3.5, 1.5)));
    REQUIRE(rangeComparison(root->right->left->range, Range(Point(3,0), Point(6,2.5))));

    REQUIRE(root->right->partition.front() == "F");
    REQUIRE(pointComparison(root->right->point, Point(4.5, 2.5)));
    REQUIRE(rangeComparison(root->right->range, Range(Point(3,0), Point(6,6))));

    REQUIRE(root->right->right->partition.front() == "G");
    REQUIRE(pointComparison(root->right->right->point, Point(5, 4.5)));
    REQUIRE(rangeComparison(root->right->right->range, Range(Point(3,2.5), Point(6,6))));

    REQUIRE(kdtree.leafNodes().size() == 4);
    REQUIRE(kdtree.nonLeafNodes().size() == 3);
}

TEST_CASE("Range bouding box Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder);
    auto root = kdtree.root();
    Range boudingBox = Range(Point(1, 1.5), Point(5, 4.5));
    REQUIRE(rangeComparison(root->range, boudingBox));
}

TEST_CASE("Limited to the root KDTreePartitioning Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)), 0);
    auto root = kdtree.root();

    REQUIRE(kdtree.leafNodes().size() == 1);
    REQUIRE(kdtree.nonLeafNodes().size() == 0);
    REQUIRE(pointComparison(root->point, Point(3, 2)));
    REQUIRE(rangeComparison(root->range, Range(Point(0,0), Point(6,6))));

    REQUIRE(kdtree.leafNodes().front()->partition.size() == 7);
}

TEST_CASE("Unbalanced KDTreePartitioning Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    builder.mElements.pop_back();
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)), 1);
    auto root = kdtree.root();

    REQUIRE(root->left->partition.size() == 3);
    REQUIRE(root->right->partition.size() == 2);
    REQUIRE(kdtree.leafNodes().size() == 2);
    REQUIRE(kdtree.nonLeafNodes().size() == 1);
}

TEST_CASE("Limited to depth 1 KDTreePartitioning Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)), 1);
    auto root = kdtree.root();

    REQUIRE(root->left->partition.size() == 3);
    REQUIRE(root->right->partition.size() == 3);
    REQUIRE(kdtree.leafNodes().size() == 2);
    REQUIRE(kdtree.nonLeafNodes().size() == 1);
}

TEST_CASE("Merge partitions KDTreePartitioning Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)));

    auto merge1 = kdtree.mergeNodes(kdtree.leafNodes());

    REQUIRE(merge1.size() == 2);
    REQUIRE(merge1.front()->partition.size() == 3);
    REQUIRE(merge1.front()->partition.front() == "B");
    REQUIRE(merge1.back()->partition.size() == 3);
    REQUIRE(merge1.back()->partition.front() == "F");

    auto merge2 = kdtree.mergeNodes(merge1);

    REQUIRE(merge2.size() == 1);
    REQUIRE(merge2.front()->partition.size() == 7);
    REQUIRE(merge2.back()->partition.front() == "D");
}

TEST_CASE("Merge partitions to the root KDTreePartitioning Test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)), 1);

    auto merge = kdtree.mergeNodes(kdtree.leafNodes());

    REQUIRE(merge.size() == 1);
    REQUIRE(merge.front()->partition.size() == 7);
    REQUIRE(merge.front()->partition.front() == "D");
}

TEST_CASE("Recursive merge test: ", "[partitioning][kdtree]")
{
    BuilderFixture builder;
    KDTreeFixture kdtree(builder, Range(Point(0,0), Point(6,6)));

    auto leafNodes =kdtree.leafNodes();
    leafNodes.pop_back();
    leafNodes.pop_back();
    leafNodes.pop_back();

    auto merge1 = kdtree.mergeNodes(leafNodes);
    auto merge2 = kdtree.mergeNodes(merge1);


    std::vector<std::string> expectedValues = {"A", "B", "C", "D", "E", "F", "G"};
    REQUIRE(std::is_permutation(merge2.front()->partition.begin(), merge2.front()->partition.end(), expectedValues.begin()));
    REQUIRE(merge2.front()->partition.size() ==  expectedValues.size());
}
