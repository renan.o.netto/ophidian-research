#include <catch.hpp>
#include <iostream>

#include <ophidian/legalization/LegalizationCheck.h>
#include <test/legalization/legalizationfixture.h>
#include <ophidian/legalization/ILPLegalization.h>

using namespace std;

TEST_CASE("Runt ILP", "[ilplegalization]") {
//    LegalCircuitFixture circuit;
    MultirowAbacusFixture circuit;
    std::vector<ophidian::circuit::Cell> circuit_cells(circuit.design_.netlist().begin(ophidian::circuit::Cell()), circuit.design_.netlist().end(ophidian::circuit::Cell()));
    ophidian::geometry::Box legalizationArea(circuit.design_.floorplan().chipOrigin().toPoint(), circuit.design_.floorplan().chipUpperRightCorner().toPoint());

    ophidian::legalization::ILPLegalization legalization(circuit.design_);
    auto result = legalization.legalizePlacement(circuit_cells, legalizationArea);
    REQUIRE(ophidian::legalization::legalizationCheck(circuit.design_.floorplan(), circuit.design_.placement(), circuit.design_.placementMapping(), circuit.design_.netlist(), circuit.design_.fences()));
}
