#include <catch.hpp>

#include <ophidian/placement/Size2Library.h>

using namespace ophidian;

class Size2LibraryFixture
{
public:
    Size2LibraryFixture() :
        library(stdCells),
        libraryMapping(netlist)
    {
        size = std::make_unique<ophidian::parser::Size>();
        parser.readFile("./input_files/single_cell_example.size", size);

        auto cell = netlist.add(ophidian::circuit::Cell(), "cell");
        auto stdCell = stdCells.add(ophidian::standard_cell::Cell(), "std_cell");
        ophidian::geometry::Box cellBox(ophidian::geometry::Point(0, 0), ophidian::geometry::Point(10, 10));
        library.geometry(stdCell, ophidian::util::MultiBox({cellBox}));
        libraryMapping.cellStdCell(cell, stdCell);

        chipFloorplan.add(floorplan::Site(), "core", ophidian::util::Location(10, 10));
    }
    parser::SizeParser parser;
    std::unique_ptr<parser::Size> size;
    circuit::Netlist netlist;
    standard_cell::StandardCells stdCells;
    placement::Library library;
    circuit::LibraryMapping libraryMapping;
    floorplan::Floorplan chipFloorplan;
};

TEST_CASE_METHOD(Size2LibraryFixture, "Size2Library on example with a single cell", "[size2library]")
{
    auto stdCell = stdCells.find(standard_cell::Cell(), "std_cell");

    auto cellBox = library.geometry(stdCell)[0];
    ophidian::geometry::Box oldBox(ophidian::geometry::Point(0, 0), ophidian::geometry::Point(10, 10));
    REQUIRE(boost::geometry::equals(cellBox, oldBox));

    ophidian::placement::size2Library(*size, library, netlist, libraryMapping, chipFloorplan);

    auto newCellBox = library.geometry(stdCell)[0];
    ophidian::geometry::Box newBox(ophidian::geometry::Point(0, 0), ophidian::geometry::Point(40, 40));
    REQUIRE(boost::geometry::equals(newCellBox, newBox));
}
