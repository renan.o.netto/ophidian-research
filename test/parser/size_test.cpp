#include <catch.hpp>

#include <ophidian/parser/Size.h>

TEST_CASE("Read example of size file", "[parser][size]") {
    ophidian::parser::SizeParser sizeParser;
    std::unique_ptr<ophidian::parser::Size> size(new ophidian::parser::Size());
    sizeParser.readFile("./input_files/example.size", size);

    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_9__u0"), ophidian::geometry::Point(1, 1)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_8__u0"), ophidian::geometry::Point(1, 2)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_7__u0"), ophidian::geometry::Point(1, 4)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_6__u0"), ophidian::geometry::Point(2, 1)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_5__u0"), ophidian::geometry::Point(2, 2)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_55__u0"), ophidian::geometry::Point(2, 4)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_54__u0"), ophidian::geometry::Point(4, 1)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_53__u0"), ophidian::geometry::Point(4, 2)));
    REQUIRE(boost::geometry::equals(size->cellSize("u2_uk_K_r9_reg_52__u0"), ophidian::geometry::Point(4, 4)));
}
