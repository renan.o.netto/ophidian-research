import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
import keras
import math
from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers import Dense, Input, Dropout
from sklearn.utils import shuffle
from sklearn.utils import class_weight
from sklearn.metrics import classification_report, confusion_matrix, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, MaxAbsScaler
from sklearn.model_selection import GridSearchCV
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.ensemble import RandomForestRegressor
from keras import regularizers

seed = 7
np.random.seed(seed)

number_of_bins = 20
header = ['n_cells_1h_m','n_cells_2h_m','n_cells_3h_m','n_cells_4h_m','n_cells_2w_m','n_cells_4w_m','n_cells_6w_m','n_cells_8w_m','n_cells_12w_m','n_cells_14w_m','n_cells_16w_m','n_cells_18w_m','n_cells_256w_m','n_cells_even_m','n_cells_odd_m', 'n_cells_1h_f','n_cells_2h_f','n_cells_3h_f','n_cells_4h_f','n_cells_2w_f','n_cells_4w_f','n_cells_6w_f','n_cells_8w_f','n_cells_12w_f','n_cells_14w_f','n_cells_16w_f','n_cells_18w_f','n_cells_256w_f','n_cells_even_f','n_cells_odd_f', 'partition_area','fixed_area','movable_area']
for size in range(4):
    header.append('area' + str(size))
for bin_index in range(1,number_of_bins+2):
    header.append('bin' + str(bin_index))
for bin_index in range(1,number_of_bins+2):
    header.append('overlap' + str(bin_index))
header.append('total_displacement')
header.append('avg_displacement')
header.append('max_displacement')
header.append('hpwl')
header.append('legalization_result')

iccad_2015_circuits = ['superblue18', 'superblue4', 'superblue10', 'superblue7', 'superblue1', 'superblue16', 'superblue3', 'superblue5']
iccad_2017_circuits = ['des_perf_b_md1', 'des_perf_b_md2', 'edit_dist_1_md1', 'edit_dist_a_md2', 'fft_2_md2', 'fft_a_md2', 'fft_a_md3', 'pci_bridge32_a_md1', 'des_perf_1', 'des_perf_a_md1', 'des_perf_a_md2', 'edit_dist_a_md3', 'pci_bridge32_a_md2', 'pci_bridge32_b_md1', 'pci_bridge32_b_md2', 'pci_bridge32_b_md3']

def read_data_for_all_circuits():
    circuits = iccad_2017_circuits
    frames = []
    for circuit in circuits:
        data = read_data_for_one_circuit(circuit)
        frames.append(data)

    result = pd.concat(frames)
    return result

def read_data_for_one_circuit(circuit):
    frames = []
    for i in range(1, 10):
        data = pd.read_csv('full_data_kdtree_abacus/data_' + circuit + '_' + str(i) + '.csv', sep=',', header=None, names=header)
        frames.append(data)
    data = pd.concat(frames)

    data['density'] = data['movable_area'] / (data['partition_area'] - data['fixed_area'])

    data['result'] = data.apply(lambda row : row.max_displacement > displacement_threshold or row.legalization_result == 0 or row.density <= 0, axis=1)

    data = data[desired_features]

    return data

def create_model():
    model = Sequential()

    model.add(Dense(10, activation='relu', input_dim=x_train.shape[1]))
    model.add(Dense(1, activation='relu'))

    model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    return model

def evaluate_model(circuit):
    model = create_model()

    model.fit(x_train, y_train, epochs=10, batch_size=100, validation_data=(x_test, y_test), verbose=1, class_weight=class_weights)

    model.save('histogram_' + circuit + '_model_' + str(displacement_threshold) + '.h5')

    evaluation = model.evaluate(x_test, y_test)

    print(evaluation)

    predictions = model.predict(x_test)
    for index, prediction in enumerate(predictions):
        if (prediction > 0.5):
            predictions[index] = 1
        else:
            predictions[index] = 0

    confusion = confusion_matrix(y_test.values, predictions)
    print(confusion)

    print(classification_report(y_test.values, predictions))


train_circuits = ['pci_bridge32_b_md3', 'des_perf_a_md1', 'des_perf_b_md2', 'des_perf_1', 'fft_2_md2', 'edit_dist_a_md2', 'fft_a_md3', 'fft_a_md2', 'edit_dist_a_md3', 'edit_dist_1_md1', 'des_perf_a_md2']
test_circuits = ['pci_bridge32_b_md2', 'pci_bridge32_a_md1', 'pci_bridge32_b_md1', 'pci_bridge32_a_md2', 'des_perf_b_md1']


desired_features = ['density']
for size in range(4):
    desired_features.append('area' + str(size))
for bin_index in range(1,number_of_bins+2):
    desired_features.append('bin' + str(bin_index))
for bin_index in range(1,number_of_bins+2):
    desired_features.append('overlap' + str(bin_index))
desired_features.append('result')

displacement_threshold = 20000

frames = []
for circuit in train_circuits:
    circuit_data = read_data_for_one_circuit(circuit)
    frames.append(circuit_data)
train_data = pd.concat(frames)

norm_header = ['density']
for size in range(4):
    norm_header.append('area' + str(size))
for bin_index in range(1,number_of_bins+2):
    norm_header.append('bin' + str(bin_index))
for bin_index in range(1,number_of_bins+2):
    norm_header.append('overlap' + str(bin_index))
norm_header.append('result')

frames = []
for circuit in test_circuits:
    circuit_data = read_data_for_one_circuit(circuit)
    frames.append(circuit_data)
test_data = pd.concat(frames)


full_data = pd.concat([train_data, test_data])
full_data.to_csv('full_data_kdtree_abacus/full_data_random_' + str(displacement_threshold) + '.csv')

train_data = shuffle(train_data)
test_data = shuffle(test_data)

x_train, y_train = train_data, train_data.pop('result')
x_test, y_test = test_data, test_data.pop('result')

x_scaler = MinMaxScaler().fit(x_train)
x_train = x_scaler.transform(x_train)
x_test = x_scaler.transform(x_test)

class_weights = class_weight.compute_class_weight('balanced',
                                                 np.unique(y_train),
                                                 y_train)
print('weights ' + str(class_weights))

evaluate_model('full')
